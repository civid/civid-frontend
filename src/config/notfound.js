import React from 'react';
import { Link } from 'react-router';

const NotFound = () => (
<section className="section">
<center><div className="title is-5" style={{marginBottom: "2rem"}}>404 - Page Not Found</div></center>
<center><Link to="/">Return to Home</Link></center>
</section>
);
export default NotFound;
