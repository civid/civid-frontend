
const MODELS = [

    // Accounts
    'USER',
    'USERS',
    'FOLLOWERS',

    // Articles
    'ARTICLES',
    'SOURCES',

    // Credits
    'INVITATIONS',
    'TRANSFERS',
    'WALLETS',

    // Posts
    'POST',
    'POSTS',

    // Private messages
    'PRIVATE_MESSAGES',

    // Replies
    'POST_REPLIES',

    // User roles
    'ADMINISTRATORS',
    'MODERATORS',

    // Votes
    'REPLY_VOTES',

    // Notifications
    'NOTIFICATIONS',

    // Trophies
    'TROPHIES',
    'AWARDED_TROPHIES',

];

const VIEWS = [

    // Authentication
    'LOGIN',
    'LOGOUT',
    'SIGNUP',
    'VERIFY',
    'SEND_VERIFY_EMAIL',
    'SEND_FORGOT_EMAIL',
    'SOCIAL_LOGIN',
    'REMOVE_FROM_MAILING',
    'PROFILE_PAGE_UNLOADED'

];

const addStatus = names => {
    return names.reduce((acc, name) => {
        acc[`${name}_PENDING`] = `${name}_PENDING`;
        acc[`${name}_SUCCESS`] = `${name}_SUCCESS`;
        acc[`${name}_ERROR`] = `${name}_ERROR`;
        return acc;
    }, {});
};

const setUnset = models => {
    let results = [];
    models.forEach(model => {
        results.push(`SET_${model}`);
        results.push(`UNSET_${model}`);
    });
    return results;
};

const addPagination = models => {
    return models.reduce((acc, model) => {
        const actionName = `UPDATE_${model}_PAGINATION`;
        acc[actionName] = actionName;
        return acc;
    }, {});
};

const addListing = models => {
    const actions = ['SET', 'APPEND'];
    return models.reduce((acc, model) => {
        for (const action of actions) {
            const actionName = `${action}_${model}_LIST`;
            acc[actionName] = actionName;
        }
        return acc;
    }, {});
};

const actionTypes = {
    ...addStatus(setUnset(MODELS)),
    ...addStatus(VIEWS),
    ...addPagination(['POSTS']),
    ...addListing(['POSTS']),
};

export default actionTypes;
