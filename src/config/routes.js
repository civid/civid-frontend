import React from "react";
import {IndexRoute, Route} from "react-router";
import Authenticate from '../containers/HOC/Authenticate';
import AboutRoutes from "../containers/About/routes";
import Home from "../containers/Home";
import AccountRoutes from "../containers/Account/routes";
import AuthenticationRoutes from "../containers/Authentication/routes";
import ContentRoutes from "../containers/ContentPolicy/routes";
import PostDetailRoutes from "../containers/PostDetail/routes";
import PrivacyRoutes from "../containers/Privacy/routes";
import ProfileRoutes from "../containers/Profile/routes";
import TermsRoutes from "../containers/Terms/routes";
import NotificationRoutes from "../containers/Notifications/routes";
import NotFound from './notfound.js';


export default (
    <Route path="/">
        <IndexRoute component={Authenticate(Home)}/>
        {AboutRoutes}
        {AccountRoutes}
        {AuthenticationRoutes}
        {ContentRoutes}
        {PrivacyRoutes}
        {ProfileRoutes}
        {TermsRoutes}
        {NotificationRoutes}
        <Route path="/posts/:postSlug" component={Authenticate(Home)}/>
        <Route path='*' exact={true} component={NotFound} />
    </Route>
);
