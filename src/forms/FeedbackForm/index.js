import axios from 'axios';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm, reset} from 'redux-form';
import PropTypes from 'prop-types';
import FormStatus from '../../components/FormStatus';
import {renderTextArea, renderInput} from '../../utils/redux-form-fields';
import settings from '../../config/settings';
import {tokenHeader} from '../../utils/requestHeaders';


const FORM_ID = 'FeedbackForm';

class FeedbackForm extends Component {

    state = {
        error: null,
        success: null,
    };


    formSubmit = data => {

        const {dispatch} = this.props;
        const submitData = {
            ...data
        };

        axios.post(`${settings.API_ROOT}/feedback`, submitData, tokenHeader())
            .then(() => {
                dispatch(reset(FORM_ID));
                this.setState({success: "Thank you for submitting your report!", error: null});
                this.props.closeModal();
            })
            .catch(error => {
                this.setState({error: error.response.data});
            });
    };

    render() {
        const {handleSubmit, success} = this.props;

        return (
            <form onSubmit={handleSubmit(this.formSubmit)}>
                <FormStatus formState={this.state} />
                {success && <div style={{color:"#25e200"}}>{success}</div>}
                <Field component={renderInput} className="input" name="user" placeholder="Enter your username (optional)" />
                <Field component={renderTextArea} className="textarea" name="message" placeholder="Enter your feedback here" minRows={3}/>
                <div className="buttons">
                    <button className="button is-blue" type="submit">Submit</button>
                </div>
            </form>
        );
    }

}

FeedbackForm = reduxForm({
    form: FORM_ID
})(FeedbackForm);

export default FeedbackForm;
