import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector, reset } from 'redux-form';
import RichTextMarkdown from '../../components/Editor';
import pick from 'lodash/pick';
import { createPostReply } from '../../actions/replies/post-reply/create';
import FormStatus from '../../components/FormStatus';
import PropTypes from 'prop-types';
import {renderInput, renderTextArea} from '../../utils/redux-form-fields';
import {browserHistory} from 'react-router';


const title_len = 160
const Input = ({ input: { onChange } }) => (

    <textarea onChange={onChange}

    />
)

class OpinionInlineForm extends Component {

    constructor() {
        super();
        this.state = {
            inputs: [],
            titleCount: title_len,
            visibleClaims: ['claim_0'],
            possibleClaims: ['claim_1', 'claim_2', 'claim_3'],
            error: null,
            success: null,
            processing: false,
            create: false,
            featured: false,
            isFocused: false,

        }
    }


    titleCount = (e) => {
        this.setState({ 'titleCount': title_len - e.target.value.length })
    };

    onFocus = () => {
        this.setState({ isFocused: true });
    };

    unFocus = () => {
        this.setState({ isFocused: false });
    };

    addClaim = (e) => {
        if (this.state.possibleClaims.length > 0) {
            const visible = [...this.state.visibleClaims, ...this.state.possibleClaims.slice(0, 1)]
            const possible = this.state.possibleClaims.slice(1)

            this.setState({
                'visibleClaims': visible,
                'possibleClaims': possible

            })
        }
    };

    removeClaim = (claimId) => {
        const claim = "claim_" + (claimId-1);
        var visArray = [...this.state.visibleClaims];
        var possArray = [...this.state.possibleClaims];

        if (this.state.visibleClaims.length > 1) {
            visArray.splice(claimId-1, 1)
            possArray.splice(0, 0, claim)

            this.setState({
                'visibleClaims': visArray,
                'possibleClaims': possArray

            })
        }
    }


    canBeSubmitted = () => {
        const { title, claim_0, claim_1, claim_2, claim_3 } = this.props;
        let canSubmit = true;
        if (!title || title.length == 0 || !claim_0) canSubmit = false;
        if (claim_0 && claim_0.length < 100) canSubmit = false;
        if (claim_1 && claim_1.length < 100) canSubmit = false;
        if (claim_2 && claim_2.length < 100) canSubmit = false;
        if (claim_3 && claim_3.length < 100) canSubmit = false;
        return canSubmit;
    };

    formSubmit = data => {
        this.setState(
            { error: null, success: null, processing: true }, () => this._formSubmitCore(data)
        );
    };

    _formSubmitCore = data => {
        let fields = ['title'];
        this.state.visibleClaims.map(c =>
            fields.push(c));
        let promise = this._createPost(pick(data, fields));

        promise.catch = (error) => {
            this.setState({
                error: error.response.data,
                processing: false,
            });
        }
    };

    _createPost = data => {
        const { activeUser, dispatch, postId } = this.props;
        return dispatch(createPostReply({
            ...data,
            post: postId,
            user: activeUser.id
        }))
            .then(() => {
                this.setState({error: null, success: "Counter-claim submitted!"});
                this.props.submitFunc();
                dispatch(reset('OpinionInlineForm'));
            })
            .catch(error => {
                this.setState({error: error.response.data, processing: false});
            });
    };

    renderRestOfForm() {
        const {isCounterFocused} = this.props;
        const { isFocused, visibleClaims } = this.state;
        const isEnabled = this.canBeSubmitted();
        if (isFocused || isCounterFocused) {
            return (
                    <div>
                    <p>Claims are required to be 100 characters minimum.</p>
                    {visibleClaims.map((claim_name, key) => (
                        <div className="editor" key={key}>
                            <div className="is-pulled-right" style={{margin:"1rem 1rem 0 0"}}>
                                {visibleClaims.length > 1 && <a onClick={() => this.removeClaim(key)}>remove</a>}
                            </div>
                            <Field name={claim_name}
                                component={RichTextMarkdown}
                                {...{
                                    placeholder: `Supporting claim # ${++key}`
                                }}
                            />
                        </div>
                    )
                    )}
                    <div>
                        <a className="button addClaim" onClick={this.addClaim}>
                            <span className="icon">
                                <i className="fas fa-plus"></i>
                            </span>
                            <p>Add claim</p>
                        </a>
                    </div>


                    <div className="buttons">
                        <button disabled={!isEnabled} className="button is-purple" type="submit">Submit Opinion</button>
                        <button className="button" onClick={this.unFocus} type="reset">Cancel</button>
                    </div>
                    </div>
            );
        }
    }

    render() {
        const { handleSubmit, closeModal, canReply, isCounterFocused } = this.props;
        const { processing, isFocused } = this.state;

        let holderText = "";
        (!isFocused ? holderText = "Think you can change their mind?" : holderText = "Main argument");

        return (
            <form className="OpinionInlineForm" onSubmit={handleSubmit(this.formSubmit)}>
                <input type="hidden" value="something"/>
                { (isFocused || isCounterFocused) && <div className="provide"><p>Enter your main argument, and provide at least one supporting claim.
                    <br/>
                    Keep in mind:</p>
                    <ul style={{fontFamily: "Open Sans"}}>
                        <li>- This is a discussion; justify your opinion, do not preach it. </li>
                    <li>- Make sure to address a point made by the host.</li>
                    <li>- Concise and well thought-out claims tend to gain more influence. </li>
                    </ul>
                    </div> }
                <FormStatus formState={this.state} />
                <Field
                    component={renderInput}
                    placeholder={holderText}
                    onChange={this.titleCount}
                    name="title"
                    onFocus={this.onFocus}
                    className="input title-input"
                    maxLength={title_len}
                    disabled={!canReply}
                    autoComplete="off"
                />

                { (isFocused || isCounterFocused) && <div className="is-pulled-right remaining">
                    {this.state.titleCount}
                </div> }

                {this.renderRestOfForm()}

                {
                    processing && <div className="processing-overlay">
                        <span>
                            <i className="fas fa-spinner fa-pulse fa-5x" />
                        </span>
                    </div>
                }
            </form>
        );
    }

}

const FORM_NAME = 'OpinionInlineForm';

OpinionInlineForm = reduxForm({
    form: FORM_NAME,
})(OpinionInlineForm);

const selector = formValueSelector(FORM_NAME);

export default connect(state => ({
    initialValues: {
        anonymous: true,
    },
    activeUser: state.activeUser,
    title: selector(state, 'title'),
    claim_0: selector(state, 'claim_0'),
    claim_1: selector(state, 'claim_1'),
    claim_2: selector(state, 'claim_2'),
    claim_3: selector(state, 'claim_3'),
    anonymous: selector(state, 'anonymous'),
}))(OpinionInlineForm);

OpinionInlineForm.propTypes = {
    closeModal: PropTypes.func,
    postId: PropTypes.number.isRequired,
    submitFunc: PropTypes.func.isRequired,
    isCounterFocused: PropTypes.bool,
}
