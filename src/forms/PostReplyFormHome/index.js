import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, reset } from 'redux-form';
import PropTypes from 'prop-types';
import { createPostReply } from '../../actions/replies/post-reply/create';
import FormStatus from '../../components/FormStatus';
import { renderInput, renderTextArea } from '../../utils/redux-form-fields';
import { getProfileImage } from '../../utils/user';
import { RichTextMarkdown } from "../../components/Editor"


class PostReplyForm extends Component {
    constructor() {
        super();
        this.state = {
            title: '',
            body: ''
        }
    }
    state = {
        error: null,
        success: null,
        isFocused: false
    };

    handleTitleChange = (evt) => {
        this.setState({ title: evt.target.value });
    };
    handleBodyChange = (evt) => {
        this.setState({ body: evt.target.value });
    };
    onFocus = () => {
        this.setState({ isFocused: true });
    };
    unFocus = () => {
        this.setState({ isFocused: false });
    };

    formSubmit = data => {
        const { activeUser, dispatch, postId } = this.props;
        dispatch(createPostReply({
            ...data,
            post: postId,
            user: activeUser.id
        }))
            .then(() => {
                dispatch(reset('PostReplyForm'));
                this.props.submitFunc();
                this.setState({ error: null, success: "Reply submitted!" });

            })
            .catch(error => {
                this.setState({ error: error.response.data });
            });
    };

    render() {
        const { activeUser, handleSubmit, users, canReply } = this.props;
        const { title, body, isFocused } = this.state;
        const isEnabled = title.length > 0 && body.length > 0;
        let holderText = "";
        (!isFocused ? holderText = "Think you can change their mind?" : holderText = "What's your main point summarized?");
        return (
            <article className="media">
                <figure className="media-left">
                    <img src={getProfileImage(activeUser.id, users)} className="user-img size-40" />
                </figure>
                <div className="media-content">
                    <form onSubmit={handleSubmit(this.formSubmit)}>
                        <input type="hidden" value="something" />
                        <FormStatus formState={this.state} />
                        <Field component={renderInput} disabled={!canReply} className="input" name="title" placeholder={holderText} onChange={this.handleTitleChange} onFocus={this.onFocus} autoComplete="off" />
                        {isFocused && <Field component={renderTextArea} className="textarea" name="body" placeholder="What's your argument?" onChange={this.handleBodyChange} />}
                        <div className="buttons">
                            {isFocused && <button disabled={!isEnabled} className="button is-blue" type="submit">Reply</button>}
                            {isFocused && <button className="button" onClick={this.unFocus}>Cancel</button>}

                        </div>
                    </form>
                </div>
            </article>
        );
    }

}

PostReplyForm.propTypes = {
    postId: PropTypes.number.isRequired,
    submitFunc: PropTypes.func.isRequired,
};

PostReplyForm = reduxForm({
    form: 'PostReplyForm'
})(PostReplyForm);

export default connect(state => ({
    activeUser: state.activeUser,
    users: state.users.data
}))(PostReplyForm);
