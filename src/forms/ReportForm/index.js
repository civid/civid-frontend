import axios from 'axios';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm, reset} from 'redux-form';
import PropTypes from 'prop-types';
import {createPostReply} from '../../actions/replies/post-reply/create';
import FormStatus from '../../components/FormStatus';
import {renderTextArea} from '../../utils/redux-form-fields';
import settings from '../../config/settings';
import {tokenHeader} from '../../utils/requestHeaders';


const FORM_ID = 'ReportForm';

class ReportForm extends Component {

    state = {
        error: null,
        success: null,
    };


    formSubmit = data => {

        const {activeUser, dispatch, post, postType} = this.props;
        const submitData = {
            ...data,
            user: activeUser.id,
            content_object: post,
            type_name: postType
        };

        axios.post(`${settings.API_ROOT}/reports`, submitData, tokenHeader())
            .then(() => {
                dispatch(reset(FORM_ID));
                this.setState({success: "Thank you for submitting your report!", error: null});
                this.props.closeModal();
            })
            .catch(error => {
                this.setState({error: error.response.data});
            });
    };

    render() {
        const {handleSubmit, success} = this.props;

        return (
            <form onSubmit={handleSubmit(this.formSubmit)}>
                <FormStatus formState={this.state} />
                {success && <div style={{color:"#25e200"}}>{success}</div>}
                <Field component={renderTextArea} className="textarea" name="message" placeholder="Enter a reason for for your report" minRows={3}/>
                <div className="buttons">
                    <button className="button is-blue" type="submit">Submit</button>
                </div>
            </form>
        );
    }

}

ReportForm.propTypes = {
    post: PropTypes.object,
    postType: PropTypes.string
};

ReportForm = reduxForm({
    form: FORM_ID
})(ReportForm);

export default connect(state => ({
    activeUser: state.activeUser
}))(ReportForm);
