import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reset, reduxForm } from 'redux-form';
import { login } from '../../actions/accounts/user/authentication';
import FormStatus from '../../components/FormStatus';
import { renderInput } from '../../utils/redux-form-fields';
import ForgotPasswordButton from '../../components/Forgot-pass-button'
import OrSeparator from '../../components/Or-separator'


class LoginForm extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            email: '',
            password: ''
        }
    }
    state = {
        error: null,
        success: null
    };

    handleUserNameChange = (evt) => {
        this.setState({ username: evt.target.value });
    };

    handleEmailChange = (evt) => {
        this.setState({ email: evt.target.value });
    };

    handlePasswordChange = (evt) => {
        this.setState({ password: evt.target.value });
    };

    canBeSubmitted() {
        const { email, password, username } = this.state;
        return (
            (username.length > 0 || email.length > 0) &&
            password.length > 0
        );
    }

    formSubmit = data => {
        const { dispatch } = this.props;

        if (!this.canBeSubmitted()) {
            evt.preventDefault();
            return;
        }
        this.setState({ error: null });
        dispatch(login(data))
            .then(() => {
                dispatch(reset('LoginForm'));
            })
            .catch(error => {
                this.setState({ error: error.message });
            });
    };

    render() {

        const { handleSubmit } = this.props;
        const isEnabled = this.canBeSubmitted();

        return (
            <form onSubmit={handleSubmit(this.formSubmit)}>
                <FormStatus formState={this.state} />
                <Field
                    component={renderInput}
                    className="input"
                    placeholder="Username or email"
                    name="username"
                    type="text"
                    onChange={this.handleUserNameChange}
                />
                <Field
                    component={renderInput}
                    className="input"
                    placeholder="Password"
                    name="password"
                    type="password"
                    onChange={this.handlePasswordChange}
                />
                <button disabled={!isEnabled} className="button is-blue is-link is-medium is-fullwidth" type="submit">Submit</button>

                <ForgotPasswordButton />
            </form>
        );
    }

}

LoginForm = reduxForm({
    form: 'LoginForm'
})(LoginForm);

export default connect((state) => ({
    activeUser: state.activeUser,
}))(LoginForm);
