import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import RichTextMarkdown from '../../components/Editor';
import pick from 'lodash/pick';
import { createPost } from '../../actions/posts/post/create';
import FormStatus from '../../components/FormStatus';
import {renderInput} from '../../utils/redux-form-fields';
import PropTypes from 'prop-types';
import {browserHistory} from 'react-router';
import './OpinionForm.scss';

const title_len = 160
const Input = ({ input: { onChange } }) => (

    <textarea onChange={onChange}
        placeholder="What is your controversial opinion? Make sure that is a statment, not a question."
        className="textarea"
        maxLength={title_len}
    />
)

class OpinionForm extends Component {

    constructor() {
        super();
        this.state = {
            error: null,
            success: null,
            processing: false,
            create: false,
            featured: false,
            inputs: [],
            titleCount: title_len,
            visibleClaims: ['claim_0'],
            possibleClaims: ['claim_1', 'claim_2', 'claim_3'],

        };
    }

    titleCount = (e) => {
        this.setState({ 'titleCount': title_len - e.target.value.length })
    }

    addClaim = (e) => {
        if (this.state.possibleClaims.length > 0) {
            const visible = [...this.state.visibleClaims, ...this.state.possibleClaims.slice(0, 1)]
            const possible = this.state.possibleClaims.slice(1)

            this.setState({
                'visibleClaims': visible,
                'possibleClaims': possible

            })
        }
    }

    removeClaim = (claimId) => {
        const claim = "claim_" + (claimId-1);
        var visArray = [...this.state.visibleClaims];
        var possArray = [...this.state.possibleClaims];

        if (this.state.visibleClaims.length > 1) {
            visArray.splice(claimId-1, 1)
            possArray.splice(0, 0, claim)

            this.setState({
                'visibleClaims': visArray,
                'possibleClaims': possArray

            })
        }
    }

    canBeSubmitted = () => {
        const { title, claim_0, claim_1, claim_2, claim_3 } = this.props;
        let canSubmit = true;
        if (!title || title.length == 0 || !claim_0) canSubmit = false;
        if (claim_0 && claim_0.length < 100) canSubmit = false;
        if (claim_1 && claim_1.length < 100) canSubmit = false;
        if (claim_2 && claim_2.length < 100) canSubmit = false;
        if (claim_3 && claim_3.length < 100) canSubmit = false;
        return canSubmit;
    };

    formSubmit = data => {
        this.setState(
            { error: null, success: null, processing: true }, () => this._formSubmitCore(data)
        );
    };

    _formSubmitCore(data) {
        let fields = ['title', 'anonymous'];
        this.state.visibleClaims.map(c =>
            fields.push(c));
        let promise = this._createPost(pick(data, fields));

        promise.catch = (error) => {
            console.log(error)
            this.setState({
                error: error.response.data,
                processing: false,
            });
        }
    }

    _createPost(data) {
        const { activeUser, dispatch } = this.props;
        return dispatch(createPost({
            ...data,
            anonymous: false,
            user: activeUser.id
        }))
            .then(entities => {
                this.props.closeModal();
            });
    }

    render() {
        const { handleSubmit, closeModal } = this.props;
        const { processing } = this.state;
        const isEnabled = this.canBeSubmitted();
        const visibleClaims = this.state.visibleClaims;

        return (
            <form className="OpinionForm" onSubmit={handleSubmit(this.formSubmit)}>
                <div className="provide"><p>Want to host a discussion on Civid? Submit your opinion to us!
                    <br/>
                    Keep in mind:</p>
                    <ul style={{fontFamily: "Open Sans"}}>
                    <li>- You are the one holding the opinion.</li>
                    <li>- You are open to having your opinion challenged.</li>
                    <li>- As a host on Civid, we expect you to be active throughout the discussion.</li>
                    <li>- You agree to follow Civid’s rules and policies.</li>
                    <li>- At the end of the discussion, award the contributor who made the most compelling point.</li>
                    <li>- <i>“The less people know, the more stubbornly they know it.”</i> Observe, reflect, respect.</li>
                    </ul></div>

                <FormStatus formState={this.state} />
                <Field
                    component={renderInput}
                    onChange={this.titleCount}
                    name="title"
                    onFocus={this.onFocus}
                    placeholder="Main argument"
                    className="input title-input"
                    maxLength={title_len}
                    autoComplete="off"
                />

                <div className="is-pulled-right remaining">
                    {this.state.titleCount}
                </div>
                <p>Claims are required to be 100 characters minimum.</p>
                {visibleClaims.map((claim_name, key) => (
                    <div className="editor" key={key}>
                        <div className="is-pulled-right" style={{margin:"1rem 1rem 0 0"}}>
                            {visibleClaims.length > 1 && <a onClick={() => this.removeClaim(key)}>remove</a>}
                        </div>
                        <Field name={claim_name}
                            component={RichTextMarkdown}
                            {...{
                                placeholder: `Supporting claim # ${++key}`
                            }}
                        />
                    </div>
                )
                )}
                <div>
                    <a className="button addClaim" onClick={this.addClaim}>
                        <span className="icon">
                            <i className="fas fa-plus"></i>
                        </span>
                        <p>Add claim</p>
                    </a>
                </div>


                <div className="buttons">
                    <button disabled={!isEnabled} className="button is-purple" type="submit">Submit Opinion</button>
                    <button className="button" onClick={closeModal} type="reset">Cancel</button>
                </div>

                {
                    processing && <div className="processing-overlay">
                        <span>
                            <i className="fas fa-spinner fa-pulse fa-5x" />
                        </span>
                    </div>
                }
            </form >
        );
    }

}

const FORM_NAME = 'OpinionForm';

OpinionForm = reduxForm({
    form: FORM_NAME,
})(OpinionForm);

const selector = formValueSelector(FORM_NAME);

export default connect(state => ({
    initialValues: {
        anonymous: false,
    },
    activeUser: state.activeUser,
    title: selector(state, 'title'),
    claim_0: selector(state, 'claim_0'),
    claim_1: selector(state, 'claim_1'),
    claim_2: selector(state, 'claim_2'),
    claim_3: selector(state, 'claim_3'),
    anonymous: selector(state, 'anonymous'),
}))(OpinionForm);

OpinionForm.propTypes = {
    closeModal: PropTypes.func,
}
