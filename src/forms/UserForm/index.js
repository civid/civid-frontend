import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { editUser } from '../../actions/accounts/user/edit';
import FormStatus from '../../components/FormStatus';
import { renderInput, renderTextArea } from '../../utils/redux-form-fields';


class UserForm extends Component {

    state = {
        error: null,
        success: null
    };

    formSubmit = data => {
        //??
        if (data.mailing_opt_in == "") {
            data.mailing_opt_in = false
        }
        const { activeUser, dispatch } = this.props;
        dispatch(editUser({
            ...activeUser,
            ...data
        }))
            .then(() => {
                this.setState({
                    error: null,
                    success: 'User information updated'
                });
            })
            .catch(error => {
                console.log(error.data);
                this.setState({ error: error.message });
            });
    };

    render() {
        const { handleSubmit } = this.props;
        return (
            <form onSubmit={handleSubmit(this.formSubmit)}>
                <FormStatus formState={this.state} />
                <p>Status</p>
                <Field component={renderInput} className="input" placeholder="Status" name="description" maxLength="60" />
                <p>Country/city of residence</p>
                <Field component={renderInput} className="input" placeholder="Country/city of residence" name="location" maxLength="50" />
                <p>Job position</p>
                <Field component={renderInput} className="input" placeholder="Job position" name="work" maxLength="50" />
                <p>Education</p>
                <Field component={renderInput} className="input" placeholder="Education" name="education" maxLength="50" />
                <p>Short bio</p>
                <Field component={renderTextArea} className="textarea" placeholder="Short bio" name="about_text" rows="5" />
                <div style={{marginBottom: "1rem"}}>
                    <label style={{marginRight:".5rem"}}>Receive Civid newsletter</label><Field component="input" type="checkbox" name="mailing_opt_in" />
                </div>
                <button className="button is-blue" type="submit">Submit</button>
            </form>
        );
    }

}

UserForm = reduxForm({
    form: 'UserForm'
})(UserForm);

export default connect(state => ({
    activeUser: state.activeUser,
    enableReinitialize: true,
    initialValues: state.activeUser
}))(UserForm);
