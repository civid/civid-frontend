import React, { Component } from 'react';
import BrowserHistory from 'react-router';
import { connect } from 'react-redux';
import { Field, reset, reduxForm } from 'redux-form';
import { signup } from '../../actions/accounts/user/authentication';
import FormStatus from '../../components/FormStatus';
import { renderInput } from '../../utils/redux-form-fields';


class SignUpForm extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            email: '',
            password: ''
        }
    }
    state = {
        error: null,
        success: null
    };

    handleUserNameChange = (evt) => {
        this.setState({ username: evt.target.value });
    };

    handleEmailChange = (evt) => {
        this.setState({ email: evt.target.value });
    };

    handlePasswordChange = (evt) => {
        this.setState({ password: evt.target.value });
    };

    canBeSubmitted() {
        const { email, password, username } = this.state;
        return (
            username.length > 0 &&
            email.length > 0 &&
            password.length > 0
        );
    }

    formSubmit = data => {
        const { dispatch, closeModal } = this.props;
        if (!this.canBeSubmitted()) {
            evt.preventDefault();
            return;
        }
        this.setState({ error: null });
        dispatch(signup(data))

            .then(() => {
                if (closeModal) {
                    this.props.closeModal();
                    window.location.reload();
                }
                else {
                    dispatch(reset('SignUpForm'));
                    window.location.reload();
                }
            })
            .catch(error => {
                this.setState({ error: error.response.data || error.response.data.message });
            });
    };

    render() {
        const { handleSubmit } = this.props;
        const isEnabled = this.canBeSubmitted();

        return (
            <form onSubmit={handleSubmit(this.formSubmit)}>
                <FormStatus formState={this.state} />

                <Field
                    component={renderInput}
                    className="input"
                    placeholder="Choose a username"
                    name="username"
                    type="text"
                    onChange={this.handleUserNameChange}
                />

                <Field
                    component={renderInput}
                    className="input"
                    placeholder="Email"
                    name="email"
                    type="email"
                    onChange={this.handleEmailChange}
                />

                <Field
                    component={renderInput}
                    className="input"
                    placeholder="Password"
                    name="password"
                    type="password"
                    onChange={this.handlePasswordChange}
                />
                <button disabled={!isEnabled} className="button is-link" type="submit">Sign Up</button>
            </form>
        );
    }

}

SignUpForm = reduxForm({
    form: 'SignUpForm'
})(SignUpForm);

export default connect((state, props) => ({
    activeUser: state.activeUser,
}))(SignUpForm);
