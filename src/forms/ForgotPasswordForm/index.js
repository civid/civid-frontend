import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, reset } from 'redux-form';
import { forgotpassword } from '../../actions/accounts/user/authentication';
import FormStatus from '../../components/FormStatus';
import { renderInput } from '../../utils/redux-form-fields';
import OrSeparator from '../../components/Or-separator'


class ForgotPassForm extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            email: ''
        }
    }
    state = {
        error: null,
        success: null,

    };

    handleUserNameChange = (evt) => {
        this.setState({ username: evt.target.value });
    };

    handleEmailChange = (evt) => {
        this.setState({ email: evt.target.value });
    };
    canBeSubmitted() {
        const { email, username } = this.state;
        return (
            (username.length > 0 || email.length > 0)
        );
    }
    formSubmit = data => {
        const { dispatch } = this.props;
        dispatch(forgotpassword(data))
            .then(() => {
                dispatch(reset('ForgotPassForm'));
                this.setState({
                    error: null,
                    success: 'Please check your email to reset password'
                });
            })
            .catch(error => {
                this.setState({
                    error: error.response.data || { Error: error.response.statusText },
                    success: null
                });
            });
    };

    render() {
        const { handleSubmit } = this.props;
        const isEnabled = this.canBeSubmitted()

        return (
            <form onSubmit={handleSubmit(this.formSubmit)}>
                <FormStatus formState={this.state} />
                <Field
                    component={renderInput}
                    className="input"
                    placeholder="Email"
                    name="email"
                    type="email"
                    onChange={this.handleEmailChange}
                />
                <button disabled={!isEnabled} className="button is-blue is-link is-medium is-fullwidth" type="submit">Submit</button>
            </form>
        );
    }

}

ForgotPassForm = reduxForm({
    form: 'ForgotPassForm'
})(ForgotPassForm);

export default connect(state => ({}))(ForgotPassForm);
