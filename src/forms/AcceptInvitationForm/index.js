import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Field, formValueSelector, reduxForm } from 'redux-form';

import { acceptInvitation } from '../../actions/accounts/user/accept-invitation';
import FormStatus from '../../components/FormStatus';
import { renderInput } from '../../utils/redux-form-fields';


class AcceptInvitationForm extends Component {

    constructor() {
        super();

        this.state = {
            error: null,
            success: null
        };
    }

    canBeSubmitted() {
        const { firstName, lastName, userName, email, password } = this.props;
        return (
            firstName && firstName.length > 0 &&
            lastName && lastName.length > 0 &&
            userName && userName.length > 0 &&
            email && email.length > 0 &&
            password && password.length > 0
        );
    }

    formSubmit = data => this.setState(
        { error: null, success: null },
        () => this._formSubmitCore(data),
    );

    _formSubmitCore(data) {
        const { dispatch } = this.props;
        dispatch(acceptInvitation(data))
            .then(() => {
                this.setState({ error: null });
                browserHistory.push('/home');
            })
            .catch(error => this.setState({ error: error.response.data }));
    }

    render() {
        const { handleSubmit } = this.props;
        const isEnabled = this.canBeSubmitted();
        return (
            <form onSubmit={handleSubmit(this.formSubmit)}>
                <FormStatus formState={this.state} />
                <Field
                    component={renderInput}
                    className="input"
                    name="first_name"
                    placeholder="First name"
                    type="text"
                />
                <Field
                    component={renderInput}
                    className="input"
                    name="last_name"
                    placeholder="Last name"
                    type="text"
                />
                <Field
                    component={renderInput}
                    className="input"
                    placeholder="User name"
                    name="user_name"
                    type="text"
                />
                <Field
                    component={renderInput}
                    className="input"
                    placeholder="Email address"
                    name="email"
                    type="email"
                />
                <Field
                    component={renderInput}
                    className="input"
                    placeholder="New Password"
                    name="password"
                    type="password"
                />
                <button disabled={!isEnabled} className="button is-blue" type="submit">Submit</button>
            </form>
        );
    }

}

const FORM_NAME = 'AcceptInvitationForm';

AcceptInvitationForm = reduxForm({
    form: FORM_NAME
})(AcceptInvitationForm);

const selector = formValueSelector(FORM_NAME);

export default connect(state => ({
    activeUser: state.activeUser,
    firstName: selector(state, 'first_name'),
    lastName: selector(state, 'last_name'),
    userName: selector(state, 'user_name'),
    email: selector(state, 'email'),
    password: selector(state, 'password'),
}))(AcceptInvitationForm);
