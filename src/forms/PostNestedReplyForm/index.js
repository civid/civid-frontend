import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm, reset} from 'redux-form';
import PropTypes from 'prop-types';
import {createPostReply} from '../../actions/replies/post-reply/create';
import FormStatus from '../../components/FormStatus';
import {renderTextArea} from '../../utils/redux-form-fields';


const FORM_ID = 'PostNestedReplyForm';

class PostNestedReplyForm extends Component {
    constructor() {
        super();
        this.state = {
            claim_0: ''
        }
    }
    state = {
        error: null,
        success: null,
        isFocused: true
    };

    unFocus = () => {
        this.setState({isFocused: false});
    };

    handleBodyChange = (evt) => {
        this.setState({ claim_0: evt.target.value });
    };

    formSubmit = data => {
        const {activeUser, dispatch, postId, parentReplyId, claimId} = this.props;
        dispatch(createPostReply({
            ...data,
            title: '',
            post: postId,
            parent: parentReplyId,
            user: activeUser.id,
            claimparent: claimId,
        }))
            .then(() => {
                dispatch(reset(FORM_ID));
                this.props.onSuccessfulSubmit();
            })
            .catch(error => {
                this.setState({error: error.response.data});
            });
    };

    render() {
        const {handleSubmit} = this.props;
        const {claim_0, isFocused} = this.state;
        const isEnabled = claim_0.length > 0;
        return (
            <form onSubmit={handleSubmit(this.formSubmit)}>
                <FormStatus formState={this.state} />
                <Field component={renderTextArea} className="textarea" name="claim_0" placeholder="Share your thoughts..." minRows={1} onChange={this.handleBodyChange}/>
                <div className="buttons">
                    <button disabled={!isEnabled} className="button is-small is-blue" type="submit">Reply</button>
                </div>
            </form>
        );
    }

}

PostNestedReplyForm.propTypes = {
    claimId: PropTypes.number.isRequired,
    postId: PropTypes.number.isRequired,
    parentReplyId: PropTypes.number.isRequired,
    onSuccessfulSubmit: PropTypes.func.isRequired,
};

PostNestedReplyForm = reduxForm({
    form: FORM_ID
})(PostNestedReplyForm);

export default connect(state => ({
    activeUser: state.activeUser
}))(PostNestedReplyForm);
