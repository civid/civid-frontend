import React, {Component} from 'react';
import {connect} from 'react-redux';
import {browserHistory} from 'react-router';
import {Field, reduxForm, formValueSelector} from 'redux-form';
import pick from 'lodash/pick';
import classnames from 'classnames';
import {createPost} from '../../actions/posts/post/create';
import {createArticleFromUrl} from '../../actions/articles/article/create';
import FormStatus from '../../components/FormStatus';
import {renderInput, renderTextArea} from '../../utils/redux-form-fields';
import DropDownSelect, {URL_ARTICLE_VALUE_STUB} from '../../utils/DropDownSelect';
import {modal} from '../../containers/Home/modal.js';
import './PostForm.scss';


class PostForm extends Component {

    constructor() {
        super();
        this.state = {
            error: null,
            success: null,
            processing: false,
            create: false,
            featured: false,
            inputs: []
        };
    }

    canBeSubmitted() {
        const {selectedArticle, title, body, articleUrl} = this.props;
        const articleSpecified = Number(selectedArticle) > 0 || (articleUrl && articleUrl.length > 0);
        return articleSpecified && title && title.length > 0 && body && body.length > 300;
    }

    formSubmit = data => {
        this.setState(
            {error: null, success: null, processing: true},
            () => this._formSubmitCore(data)
        );
    };

    renderInputs() {
        return this.state.inputs;
    }

    changeInput(evt, choice) {
        const {articles} = this.props;
        if (evt) {
            evt.preventDefault();
        }
        if (choice == "featured") {
            this.setState({ inputs: [<Field
                    component={DropDownSelect}
                    articles={articles}
                    label="Topic"
                    name="article"
                />], create: false, featured: true});
        }

        else if (choice == "create") {
            this.setState({ inputs: [<Field
                    component={renderInput}
                    className="input"
                    name="url"
                    placeholder="Submit a news article URL"
                />], create: true, featured: false});
        }


    }


    _formSubmitCore(data) {
        const {dispatch} = this.props;
        const {create} = this.state;
        let promise;
        if (create == true) {
            console.log("create topic");
            promise = dispatch(createArticleFromUrl({url: data.url}))
                .then(article => this._createPost({
                    ...pick(data, ['title', 'body']),
                    article: article.id,
                }));
        } else {
            promise = this._createPost(pick(data, ['title', 'body', 'article']));
        }
        promise.catch(error => this.setState({
            error: error.response.data,
            processing: false,
        }));
    }

    _createPost(data) {
        const {activeUser, dispatch} = this.props;
        return dispatch(createPost({
            ...data,
            user: activeUser.id
        }))
            .then(entities => {
                const post = Object.values(entities['POSTS'])[0];
                browserHistory.push(`/articles/${post.article}/posts/${post.id}`);
            });
    }

    render() {
        const {handleSubmit, articles, selectedArticle} = this.props;
        const {processing, featured, create} = this.state;
        const isEnabled = this.canBeSubmitted();

        return (
            <form className="PostForm" onSubmit={handleSubmit(this.formSubmit)}>
                <FormStatus formState={this.state}/>
                <div className="buttons">
                    <button className={classnames('button', {
          'is-blue': this.state.create,
        })} onClick={(evt) => this.changeInput(evt, "create")}>Create a new topic</button>
                    <button className={classnames('button', {
          'is-blue': this.state.featured,
        })} onClick={(evt) => this.changeInput(evt, "featured")}>Choose a featured topic</button>
                </div>


                {this.renderInputs()}
                <Field
                    component={renderInput}
                    className="input"
                    name="title"
                    placeholder="What's your main point? (120 char max)"
                />
                <Field
                    component={renderTextArea}
                    minlength="300"
                    className="textarea"
                    placeholder="What are your thoughts on this topic? (300 char min)"
                    name="body"
                    minRows={10}
                />
                <div className="buttons">
                    <button disabled={!isEnabled} className="button is-purple" type="submit">Post</button>
                    <button className="button" onClick={modal} type="reset">Cancel</button>
                </div>
                {processing && <div className="processing-overlay">
                    <span>
                        <i className="fas fa-spinner fa-pulse fa-5x" />
                    </span>
                </div>}
            </form>
        );
    }

}

const FORM_NAME = 'PostForm';

PostForm = reduxForm({
    form: FORM_NAME,
})(PostForm);

const selector = formValueSelector(FORM_NAME);

export default connect(state => ({
    activeUser: state.activeUser,
    articles: state.articles.data,
    posts: state.posts,
    selectedArticle: selector(state, 'article'),
    title: selector(state, 'title'),
    body: selector(state, 'body'),
    articleUrl: selector(state, 'url'),
}))(PostForm);
