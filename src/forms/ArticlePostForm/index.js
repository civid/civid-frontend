import React, {Component} from 'react';
import {connect} from 'react-redux';
import Dropzone from 'react-dropzone'
import {browserHistory} from 'react-router';
import {Field, reduxForm} from 'redux-form';
import {createPost} from '../../actions/posts/post/create';
import FormStatus from '../../components/FormStatus';
import {renderInput, renderTextArea} from '../../utils/redux-form-fields';
import DropDownSelect from '../../utils/DropDownSelect';
import {modal} from '../../containers/Home/modal.js';
import PropTypes from 'prop-types';
import './ArticlePostForm.scss';


class ArticlePostForm extends Component {
    constructor() {
        super();
        this.state = {
            title: '',
            body: ''
        }
    }
    state = {
        error: null,
        success: null
    };

    handleTitleChange = (evt) => {
        this.setState({ title: evt.target.value });
    };
    handleBodyChange = (evt) => {
        this.setState({ body: evt.target.value });
    };

    canBeSubmitted() {
        const {title, body} = this.state;
        return (
            title.length > 0 && body.length > 0
        );
    }

    formSubmit = data => {
        const {activeUser, articleId, dispatch} = this.props;
        if (!this.canBeSubmitted()) {
            evt.preventDefault();
            return;
        }
        dispatch(createPost({
            ...data,
            article: articleId,
            user: activeUser.id
        }))
            .then(entities => {
                const post = Object.values(entities['POSTS'])[0];
                browserHistory.push(`/articles/${post.article}/posts/${post.id}`);
            })
            .catch(error => {
                this.setState({error: error.response.data});
            });
    };

    onDrop = files => {
        this.setState({files});
    };


    render() {
        const {handleSubmit, articleId} = this.props;
        const {title, body} = this.state;
        const isEnabled = this.canBeSubmitted();
        return (
            <form className="ArticlePostForm" onSubmit={handleSubmit(this.formSubmit)}>
                <FormStatus formState={this.state}/>
                <Field component={renderInput} className="input" name="title" placeholder="Title" onChange={this.handleTitleChange}/>
                <Field component={renderTextArea} className="textarea" placeholder="Enter your argument here" name="body" rows="10" onChange={this.handleBodyChange}/>
                <div className="buttons">
                <button disabled={!isEnabled} className="button is-blue" type="submit">Submit</button>
                <button className="button" onClick={modal} type="reset">Cancel</button>
                </div>
            </form>
        );
    }

}

ArticlePostForm = reduxForm({
    form: 'ArticlePostForm'
})(ArticlePostForm);

ArticlePostForm.propTypes = {
    articleId: PropTypes.number.isRequired,
};

export default connect(state => ({
    activeUser: state.activeUser,
    posts: state.posts
}))(ArticlePostForm);
