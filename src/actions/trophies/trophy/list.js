import axios from 'axios';
import {normalize} from 'normalizr';

import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import {setNormalized} from '../../../utils/general';
import {TROPHY} from '../../../utils/normalize';
import {tokenHeader} from '../../../utils/requestHeaders';


export const getTrophiesList = () => async dispatch => {
    const MODEL = 'TROPHIES';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    try {
        const response = await axios.get(`${settings.API_ROOT}/trophies`, tokenHeader());
        const {entities} = normalize(response.data, [TROPHY]);
        setNormalized(dispatch, entities);
    } catch(error) {
        throw error;
    }
};
