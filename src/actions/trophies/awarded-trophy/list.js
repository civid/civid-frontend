import axios from 'axios';
import {normalize} from 'normalizr';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import {setNormalized} from '../../../utils/general';
import {AWARDED_TROPHY} from '../../../utils/normalize';
import {tokenHeader} from '../../../utils/requestHeaders';


export const getAwardedTrophiesList = (opts = {}) => async dispatch => {
    const MODEL = 'AWARDED_TROPHIES';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    try {
        const queryString = 'username' in opts ? `?username=${opts.username}` : '';
        const response = await axios.get(`${settings.API_ROOT}/trophies/awarded${queryString}`, tokenHeader());
        const {entities} = normalize(response.data, [AWARDED_TROPHY]);
        setNormalized(dispatch, entities);
    } catch(error) {
        throw error;
    }
};
