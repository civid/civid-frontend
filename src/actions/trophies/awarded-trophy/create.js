import axios from 'axios';
import {normalize} from 'normalizr';

import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import {tokenHeader} from '../../../utils/requestHeaders';
import {AWARDED_TROPHY} from "../../../utils/normalize";
import {setNormalized} from "../../../utils/general";


export const awardTrophyForPost = ({userId, postId, trophyId}) => async dispatch => {
    const MODEL = 'AWARDED_TROPHIES';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    try {
        const payload = { userId };
        const response = await axios.post(
            `${settings.API_ROOT}/trophies/${trophyId}/award_for_post/${postId}`,
            payload,
            tokenHeader()
        );
        const {entities} = normalize(response.data, AWARDED_TROPHY);
        setNormalized(dispatch, entities);
    } catch(error) {
        throw error;
    }
};
