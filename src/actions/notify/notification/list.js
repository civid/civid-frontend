import axios from 'axios';
import {normalize} from 'normalizr';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import {setNormalized} from '../../../utils/general';
import {NOTIFICATION} from '../../../utils/normalize';
import {tokenHeader} from '../../../utils/requestHeaders';


export const getNotificationList = (params = {}) => async dispatch => {
    const MODEL = 'NOTIFICATIONS';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    try {
        const response = await axios.get(`${settings.API_ROOT}/notifications`, tokenHeader());
        const {entities} = normalize(response.data, NOTIFICATION);
        setNormalized(dispatch, entities);
    } catch(error) {
        throw error;
    }
};
