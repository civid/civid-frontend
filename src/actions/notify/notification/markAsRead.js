import axios from 'axios';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import {tokenHeader} from '../../../utils/requestHeaders';


export const markNotificationsAsRead = opts => async dispatch => {
    const MODEL = 'NOTIFICATIONS';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    try {
        const payload = {
            ids: opts.ids,
        };
        await axios.post(`${settings.API_ROOT}/notifications/mark-as-read`, payload, tokenHeader());
        dispatch({
            type: actionTypes[`UNSET_${MODEL}_SUCCESS`],
            payload: opts.ids,
        });
    } catch(error) {
        throw error;
    }
};
