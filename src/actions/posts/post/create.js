import axios from 'axios';
import { normalize } from 'normalizr';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import { setNormalized } from '../../../utils/general';
import { POST } from '../../../utils/normalize';
import { tokenHeader } from '../../../utils/requestHeaders';


export const createPost = data => async dispatch => {
    const MODEL = 'POSTS';
    dispatch({ type: actionTypes[`SET_${MODEL}_PENDING`] });
    let formData = new FormData();
    formData.append('claim_0', data.claim_0 || '');
    formData.append('claim_1', data.claim_1 || '');
    formData.append('claim_2', data.claim_2 || '');
    formData.append('claim_3', data.claim_3 || '');
    formData.append('title', data.title || '');
    formData.append('anonymous', data.anonymous || false);

    try {
        const response = await axios.post(`${settings.API_ROOT}/posts`, formData, tokenHeader());
        const { entities } = normalize(response.data, POST);
        return entities;
    } catch (error) {
        throw error;
    }
};
