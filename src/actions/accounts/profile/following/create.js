import axios from 'axios';
import {normalize} from 'normalizr';
import actionTypes from '../../../../config/action-types';
import settings from '../../../../config/settings';
import {setNormalized} from '../../../../utils/general';
import {FOLLOWER} from '../../../../utils/normalize';
import {tokenHeader} from '../../../../utils/requestHeaders';


export const followUser = data => async dispatch => {
    const MODEL = 'FOLLOWERS';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    try {
        const response = await axios.post(`${settings.API_ROOT}/profiles/${data.id}/follow`, data, tokenHeader());
        const {entities} = normalize(response.data, FOLLOWER);
        setNormalized(dispatch, entities);
        return entities;
    } catch(error) {
        throw error;
    }
};
