import axios from 'axios';
import actionTypes from '../../../../config/action-types';
import settings from '../../../../config/settings';
import {tokenHeader} from '../../../../utils/requestHeaders';
import {browserHistory} from 'react-router';


export const unfollowUser = data => async dispatch => {
    const MODEL = 'FOLLOWERS';
    dispatch({type: actionTypes[`UNSET_${MODEL}_PENDING`]});
    try {
        await axios.delete(`${settings.API_ROOT}/profiles/${data.userId}/follow`, tokenHeader());
        dispatch({
            type: actionTypes[`UNSET_${MODEL}_SUCCESS`],
            payload: {id: data.followerId},
        });
    } catch(error) {
        throw error;
    }
};
