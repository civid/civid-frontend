import axios from 'axios';
import {normalize} from 'normalizr';
import actionTypes from '../../../../config/action-types';
import settings from '../../../../config/settings';
import {setNormalized} from '../../../../utils/general';
import {FOLLOWER} from '../../../../utils/normalize';
import {tokenHeader} from '../../../../utils/requestHeaders';


export const getFollowers = id => async (dispatch, getState) => {
    const MODEL = 'FOLLOWERS';
    dispatch({
        type: actionTypes[`UNSET_${MODEL}_SUCCESS`],
        payload: Object.values(getState().followers.data).map(f => f.id),
    });
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    try {
        const response = await axios.get(`${settings.API_ROOT}/profiles/${id}/followers`, tokenHeader());
        const {entities} = normalize(response.data, [FOLLOWER]);
        setNormalized(dispatch, entities);
    } catch(error) {
        throw error;
    }
};
