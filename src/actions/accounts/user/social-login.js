import axios from 'axios';
import { browserHistory } from 'react-router';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import { redirecToOrigin } from '../../../utils/redirec-to-origin'

export const socialLogin = (data, provider) => async dispatch => {

    dispatch({ type: actionTypes[`SOCIAL_LOGIN_PENDING`] });
    try {

        let parameters = {}

        if (provider == 'facebook') {
            parameters['access_token'] = data['accessToken']
        }
        if (provider == 'twitter') {
            parameters['access_token'] = data['accessToken']
        }
        if (provider == 'google-oauth2') {
            parameters['access_token'] = data['accessToken']
        }
        const response = await axios.post(`${settings.API_ROOT}/social-login/${provider}/`, parameters)
        localStorage.setItem('activeUser', JSON.stringify(response.data));
        dispatch({
            type: actionTypes[`SOCIAL_LOGIN_SUCCESS`],
            payload: response.data
        });
        window.location.reload();

    } catch (error) {
        throw error;
    }
};
