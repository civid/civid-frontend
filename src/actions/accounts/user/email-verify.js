import axios from 'axios';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';


export const emailVerify = data => async dispatch => {

    dispatch({ type: actionTypes[`VERIFY_PENDING`] });
    try {
        const response = await axios.post(`${settings.API_ROOT}/verify-email/${data}`);

        dispatch({
            type: actionTypes[`VERIFY_SUCCESS`],
            payload: response.data
        });

    } catch (error) {
        throw error;
    }
};