import axios from 'axios';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';


export const RemoveUserFromMailingList = data => async dispatch => {

    dispatch({ type: actionTypes[`REMOVE_FROM_MAILING_PENDING`] });
    try {
        const response = await axios.post(`${settings.API_ROOT}/remove-from-mailing/${data}`);

        dispatch({
            type: actionTypes[`REMOVE_FROM_MAILING_SUCCESS`],
            payload: response.data
        });

    } catch (error) {
        throw error;
    }
};