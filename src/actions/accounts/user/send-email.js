import axios from 'axios';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';


export const sendVerifyMail = data => async dispatch => {

    dispatch({ type: actionTypes[`SEND_VERIFY_EMAIL_PENDING`] });
    try {
        const response = await axios.post(`${settings.API_ROOT}/send-verify-email/${data}`);

        dispatch({
            type: actionTypes[`SEND_VERIFY_EMAIL_SUCCESS`],
            payload: response.data
        });

    } catch (error) {
        throw error;
    }
};