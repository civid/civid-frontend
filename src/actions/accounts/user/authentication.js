import axios from 'axios';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import { tokenHeader } from '../../../utils/requestHeaders';
import { redirecToOrigin } from '../../../utils/redirec-to-origin'


export const login = data => async dispatch => {
    dispatch({ type: actionTypes[`LOGIN_PENDING`] });
    try {
        const response = await axios.post(`${settings.API_ROOT}/login`, data);
        localStorage.setItem('activeUser', JSON.stringify(response.data));
        dispatch({
            type: actionTypes[`LOGIN_SUCCESS`],
            payload: response.data
        });
        redirecToOrigin('/')
    } catch (error) {
        throw new Error("Your email or password is incorrect. Please try again.");
    }
};

export const logout = () => dispatch => {
    localStorage.removeItem('activeUser');
    dispatch({
        type: actionTypes[`LOGOUT_SUCCESS`]
    });
};

export const updatePassword = data => async () => {
    try {
        const response = await axios.post(`${settings.API_ROOT}/update_password`, data, tokenHeader());
        return response.data;
    } catch (error) {
        throw error;
    }
};

export const signup = data => async dispatch => {

    dispatch({ type: actionTypes[`SIGNUP_PENDING`] });
    try {
        const response = await axios.post(`${settings.API_ROOT}/sign-up`, data);

        localStorage.setItem('activeUser', JSON.stringify(response.data));
        dispatch({
            type: actionTypes[`SIGNUP_SUCCESS`],
            payload: response.data
        });
        window.location.reload();
    } catch (error) {
        throw error;
    }
};

export const forgotpassword = data => async dispatch => {

    dispatch({ type: actionTypes[`SEND_FORGOT_EMAIL_PENDING`] });
    try {
        const response = await axios.post(`${settings.API_ROOT}/forgot-email`, data);
        dispatch({
            type: actionTypes[`SEND_FORGOT_EMAIL_SUCCESS`],
            payload: response.data
        });

    } catch (error) {
        throw error;
    }
};

