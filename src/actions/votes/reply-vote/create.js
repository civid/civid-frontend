import axios from 'axios';
import {normalize} from 'normalizr';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import {setNormalized} from '../../../utils/general';
import {REPLY_VOTE} from '../../../utils/normalize';
import {tokenHeader} from '../../../utils/requestHeaders';


export const createReplyVote = data => async dispatch => {
    const MODEL = 'REPLY_VOTES';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    try {
        const response = await axios.post(`${settings.API_ROOT}/votes`, data, tokenHeader());
        const {entities} = normalize(response.data, REPLY_VOTE);
        setNormalized(dispatch, entities);
    } catch(error) {
        dispatch({
            type: actionTypes[`SET_${MODEL}_ERROR`],
            payload: error
        });
    }
};
