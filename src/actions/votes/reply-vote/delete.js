import axios from 'axios';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import { tokenHeader } from '../../../utils/requestHeaders';


export const deleteReplyVote = replyVote => async dispatch => {
    const MODEL = 'REPLY_VOTES';
    dispatch({ type: actionTypes[`UNSET_${MODEL}_PENDING`] });
    try {
        await axios.delete(`${settings.API_ROOT}/votes/${replyVote.id}`, tokenHeader());
        dispatch({
            type: actionTypes[`UNSET_${MODEL}_SUCCESS`],
            payload: replyVote
        });
    } catch (error) {
        throw error;
    }
};
