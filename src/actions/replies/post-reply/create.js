import axios from 'axios';
import {normalize} from 'normalizr';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import {setNormalized} from '../../../utils/general';
import {POST_REPLY} from '../../../utils/normalize';
import {tokenHeader} from '../../../utils/requestHeaders';


export const createPostReply = data => async (dispatch, getState) => {
    const MODEL = 'POST_REPLIES';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    try {
        const response = await axios.post(`${settings.API_ROOT}/post_replies`, data, tokenHeader());
        const {entities} = normalize(response.data, POST_REPLY);
        setNormalized(dispatch, entities);

        const postReplies = getState().postReplies.data;
        const updatedParents = Object.values(entities[MODEL])
            .filter(r => !!r.parent)
            .map(r => {
                const parentReply = postReplies[r.parent];
                const parentReplies = parentReply.replies ? parentReply.replies : [];
                return Object.assign({}, parentReply, { replies: [...parentReplies, r.id]});
            })
            .reduce((acc, r) => {
                acc[r.id] = r;
                return acc;
            }, {});
        dispatch({type: actionTypes[`SET_${MODEL}_SUCCESS`], payload: updatedParents});

        return entities;
    } catch(error) {
        throw error;
    }
};
