import axios from 'axios';
import {normalize} from 'normalizr';

import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import {setNormalized} from '../../../utils/general';
import {ARTICLE} from '../../../utils/normalize';
import {tokenHeader} from '../../../utils/requestHeaders';


const MODEL = 'ARTICLES';

export const createArticle = data => async dispatch => {
    const MODEL = 'ARTICLES';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    let formData = new FormData();
    formData.append('title', data.title || '');
    formData.append('url', data.url || '');
    formData.append('date', data.date || '');
    formData.append('pub', data.pub || '');
    if(data.image) formData.append('image', data.image);
    try {
        const response = await axios.post(`${settings.API_ROOT}/articles`, formData, tokenHeader());
        const {entities} = normalize(response.data, ARTICLE);
        setNormalized(dispatch, entities);
        return entities;
    } catch(error) {
        throw error;
    }
};

export const createArticleFromUrl = data => async dispatch => {
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    const formData = {
        url: data.url,
    };
    try {
        const response = await axios.post(`${settings.API_ROOT}/articles/submit_url`, formData, tokenHeader());
        const {entities} = normalize(response.data, ARTICLE);
        setNormalized(dispatch, entities);
        return Object.values(entities[MODEL])[0];
    } catch(error) {
        throw error;
    }
};
