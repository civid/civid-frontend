import axios from 'axios';
import {normalize} from 'normalizr';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import {setNormalized, stringify} from '../../../utils/general';
import {ARTICLE} from '../../../utils/normalize';
import {tokenHeader} from '../../../utils/requestHeaders';


export const getArticleList = (params = {}) => async dispatch => {
    const MODEL = 'ARTICLES';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    try {
        const response = await axios.get(`${settings.API_ROOT}/articles${stringify(params)}`, tokenHeader());
        const {entities} = normalize(response.data, [ARTICLE]);
        setNormalized(dispatch, entities);
    } catch(error) {
        throw error;
    }
};
