import axios from 'axios';
import {normalize} from 'normalizr';
import actionTypes from '../../../config/action-types';
import settings from '../../../config/settings';
import {setNormalized} from '../../../utils/general';
import {SOURCE} from '../../../utils/normalize';
import {tokenHeader} from '../../../utils/requestHeaders';


export const createSource = data => async dispatch => {
    const MODEL = 'SOURCES';
    dispatch({type: actionTypes[`SET_${MODEL}_PENDING`]});
    let formData = new FormData();
    formData.append('title', data.title || '');
    formData.append('url', data.url || '');
    formData.append('date', data.date || '');
    formData.append('pub', data.pub || '');
    formData.append('article', data.article);
    if(data.image) formData.append('image', data.image);
    try {
        const response = await axios.post(`${settings.API_ROOT}/sources`, formData, tokenHeader());
        const {entities} = normalize(response.data, SOURCE);
        setNormalized(dispatch, entities);
        return entities;
    } catch(error) {
        throw error;
    }
};
