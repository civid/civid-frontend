import actionTypes from '../../config/action-types';

export default function (state = null, action) {
    switch (action.type) {
        case actionTypes[`REMOVE_FROM_MAILING_SUCCESS`]:
            return action.payload;
    }
    return state;
}





