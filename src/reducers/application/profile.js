
import actionTypes from '../../config/action-types';

export default (state = {}, action) => {
    switch (action.type) {
        case actionTypes[`PROFILE_PAGE_UNLOADED`]:
            return {};
    default:
        return state;
    }
};

