export default function createListReducer(modelName, INITIAL_STATE = []) {
    return (state = INITIAL_STATE, action) => {
        switch(action.type) {

            case `SET_${modelName}_LIST`:
                return action.payload;

            case `APPEND_${modelName}_LIST`:
                return [...state, ...action.payload];
        }
        return state;
    };
}
