export default function createPaginationReducer(modelName, INITIAL_STATE = {}) {
    return (state = INITIAL_STATE, action) => {
        switch(action.type) {

            case `UPDATE_${modelName}_PAGINATION`:
                return {
                    ...state,
                    ...action.payload,
                };

        }
        return state;
    };
}
