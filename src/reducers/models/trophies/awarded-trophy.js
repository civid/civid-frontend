import {combineReducers} from 'redux';
import modelReducer from '../../higher-order-reducers/model';


const MODEL = 'AWARDED_TROPHIES';

export default combineReducers({
    data: modelReducer(MODEL)
});
