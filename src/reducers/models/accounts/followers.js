import {combineReducers} from 'redux';
import modelReducer from '../../higher-order-reducers/model';


const MODEL = 'FOLLOWERS';

export default combineReducers({
    data: modelReducer(MODEL)
});
