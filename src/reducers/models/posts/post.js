import { combineReducers } from 'redux';
import modelReducer from '../../higher-order-reducers/model';
import paginationReducer from '../../higher-order-reducers/pagination';
import listReducer from '../../higher-order-reducers/list';


const MODEL = 'POSTS';

export default combineReducers({
    data: modelReducer(MODEL),
    pagination: paginationReducer(MODEL),
    list: listReducer(MODEL),
});
