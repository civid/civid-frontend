import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import actionTypes from '../config/action-types';

// Application
import activeUser from './application/active-user';

// Models
import administrators from './models/user-roles/administrator';
import articles from './models/articles/article';
import sources from './models/sources/source';
import invitations from './models/credits/invitation';
import moderators from './models/user-roles/moderator';
import postReplies from './models/replies/post-reply';
import replyVotes from './models/votes/reply-vote';
import posts from './models/posts/post';
import privateMessages from './models/private-messages/private-message';
import users from './models/accounts/users';
import followers from './models/accounts/followers';
import notifications from './models/notify/notification';
import trophies from './models/trophies/trophy';
import awardedTrophies from './models/trophies/awarded-trophy';

const appReducer = combineReducers({
    form: formReducer,

    // Application
    activeUser,

    // Models
    administrators,
    articles,
    sources,
    invitations,
    followers,
    moderators,
    postReplies,
    replyVotes,
    posts,
    privateMessages,
    users,
    notifications,
    trophies,
    awardedTrophies,
});

const rootReducer = (state, action) => {
    if (action.type === actionTypes[`LOGOUT_SUCCESS`]) state = undefined;
    return appReducer(state, action);
};

export default rootReducer
