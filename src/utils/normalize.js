import { schema } from 'normalizr';


export const USER = new schema.Entity('USERS');

export const FOLLOWER = new schema.Entity('FOLLOWERS', {
    user: USER,
});

export const INVITATION = new schema.Entity('INVITATIONS', {
    receiver: USER,
    sender: USER,
});

export const TRANSFER = new schema.Entity('TRANSFERS', {
    receiver: USER,
    sender: USER,
});

export const WALLET = new schema.Entity('WALLETS', {
    user: USER,
});

export const ADMINISTRATOR = new schema.Entity('ADMINISTRATORS', {
    user: USER,
});

export const ARTICLE = new schema.Entity('ARTICLES', {
    user: USER,
});

export const SOURCE = new schema.Entity('SOURCES', {
    article: ARTICLE,
});

export const MODERATOR = new schema.Entity('MODERATORS', {
    sponsor: USER,
    user: USER,
});

export const REPLY_VOTE = new schema.Entity('REPLY_VOTES');

export const POST_REPLY = new schema.Entity('POST_REPLIES', {
    user: USER,
});

const replies = new schema.Array(POST_REPLY);
POST_REPLY.define({ replies });

export const POST_STATUSES = {
    NEW: 'new',
    AWARD_PENDING: 'award_pending',
    AWARD_EXPIRED: 'award_expired',
    CLOSED: 'closed',
};

export const POST = new schema.Entity('POSTS', {
    user: USER,
    direct_replies: [POST_REPLY],
    article: ARTICLE,
});


export const PRIVATE_MESSAGE = new schema.Entity('PRIVATE_MESSAGES', {
    receiver: USER,
    sender: USER,
});

export const TROPHY = new schema.Entity('TROPHIES');

export const NOTIFICATION_TYPES = {
    REPLY_TO_COMMENT: 'REPLY_TO_COMMENT',
    RESPONSE_TO_OPINION: 'RESPONSE_TO_OPINION',
    NEW_FOLLOWER: 'NEW_FOLLOWER',
    RECEIVED_TROPHY: 'RECEIVED_TROPHY',
    TROPHY_REMINDER: 'TROPHY_REMINDER',
};

export const REPLY_NOTIFICATION = new schema.Entity('NOTIFICATIONS', {
    actor: USER,
    recipient: USER,
    target: ARTICLE,
    action_object: POST_REPLY,
});

export const FOLLOWER_NOTIFICATION = new schema.Entity('NOTIFICATIONS', {
    actor: USER,
    recipient: USER,
});

export const TROPHY_NOTIFICATION = new schema.Entity('NOTIFICATIONS', {
    actor: USER,
    recipient: USER,
    action_object: TROPHY,
});

export const POST_TROPHY_REMINDER_NOTIFICATION = new schema.Entity('NOTIFICATIONS', {
    actor: USER,
    recipient: USER,
    target: POST,
});

export const NOTIFICATION = new schema.Array({
    [NOTIFICATION_TYPES.REPLY_TO_COMMENT]: REPLY_NOTIFICATION,
    [NOTIFICATION_TYPES.RESPONSE_TO_OPINION]: REPLY_NOTIFICATION,
    [NOTIFICATION_TYPES.NEW_FOLLOWER]: FOLLOWER_NOTIFICATION,
    [NOTIFICATION_TYPES.RECEIVED_TROPHY]: TROPHY_NOTIFICATION,
    [NOTIFICATION_TYPES.TROPHY_REMINDER]: POST_TROPHY_REMINDER_NOTIFICATION,
}, 'verb');

export const AWARDED_TROPHY_CONTEXT_TYPES = {
    POST: 'post',
};

const AWARDED_TROPHY_CONTEXT = new schema.Union({
    [AWARDED_TROPHY_CONTEXT_TYPES.POST]: POST,
}, () => AWARDED_TROPHY_CONTEXT_TYPES.POST);

export const AWARDED_TROPHY = new schema.Entity('AWARDED_TROPHIES', {
    user: USER,
    trophy: TROPHY,
    context: AWARDED_TROPHY_CONTEXT,
});
