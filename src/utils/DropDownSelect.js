import React, {Component} from 'react';
import {connect} from 'react-redux';


export const URL_ARTICLE_VALUE_STUB = 'url';

class DropDownSelect extends Component {

    renderSelectOptions = (articles) => (
        <option key={articles.id} value={articles.id}>{articles.title}</option>
    );

    render() {
        const {articles, input, meta: {touched, error} } = this.props;
        const featuredArticles = Object.values(articles).filter(a => !!a.featured);
        return (
            <div className="form-group select">
                <select {...input}>
                    <option value="0" disabled>Choose a topic</option>
                    {Object.values(featuredArticles).map(this.renderSelectOptions)}
                </select>
                {touched && error && <span className="has-text-danger">{error}</span>}
            </div>
        );
    }
}

{/**export const renderDropdown = ({input, label, type, meta: {touched, error}}) =>
    (
     <div className="form-group">
        <label>{label}:</label>
        <select className="form-control">
            <option value="sdfsdf">sdfsdf</option>
        </select>
        {touched && error && <span className="text-danger">{error}</span>}
    </div>
); **/}

export default connect(state => ({
    articles: state.articles.data
}))(DropDownSelect);
