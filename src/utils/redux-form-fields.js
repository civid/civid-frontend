import React from 'react';
import Textarea from 'react-textarea-autosize';
import './forms.scss';

export const renderInput = ({ input, type, className, placeholder, autoComplete, maxLength, meta: { touched, error }, disabled }) => (
    <div className="form-group">
        <input {...input} className={className} disabled={disabled} placeholder={placeholder} maxLength={maxLength} type={type} autoComplete={autoComplete} />
        {touched && error && <span className="has-text-danger">{error}</span>}
    </div>
);

export const renderTextArea = ({ input, type, rows, minRows, maxRows, className, placeholder, meta: { touched, error } }) => (
    <div className="form-group">
        <Textarea {...input} className={className} minRows={minRows} maxRows={maxRows} rows={minRows} placeholder={placeholder} />
        {touched && error && <span className="has-text-danger">{error}</span>}
    </div>
);
