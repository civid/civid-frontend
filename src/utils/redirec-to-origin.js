import { browserHistory } from 'react-router';

export const redirecToOrigin = (destin) => {
    // if coming from a page that needs autentication
    // redirect to path , otherwise to destin
    const path = localStorage.getItem('paths')
    localStorage.removeItem('paths')
    if (path === null) {
        browserHistory.push(destin);
    } else {
        browserHistory.push(path)
    }
}
