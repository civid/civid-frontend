import settings from '../config/settings';
import pickBy from 'lodash/pickBy';
import qs from 'querystring';
import logo from '../assets/images/logo.png';


export function setNormalized(dispatch, data) {
    Object.keys(data).forEach(key => {
        dispatch({
            type: `SET_${key}_SUCCESS`,
            payload: data[key]
        });
    });
}

export function stringify(params = {}) {
    const results = qs.stringify(pickBy(params));
    if (!Object.values(results).length) return '';
    return '?' + results;
}

export function getArticleImage(article) {
    let image = [article.image, article.image_url, logo].find(x => !!x);
    if (image !== null && !image.startsWith("http")) {
        image = settings.API_ROOT + image
    }
    return image
}


