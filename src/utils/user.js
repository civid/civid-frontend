import settings from '../config/settings';

export const getUsername = (userId, users) => {
    if (userId !== undefined && users != undefined) {
        const { username } = users[userId];
        return `${username}`;
    }
}

export const getProfileImage = (userId, users) => {
    let image = null;
    if (userId !== undefined && users[userId] != undefined) {
        image = users[userId].profile.image;
    };
    if (image !== null && !image.startsWith("http")) {
        image = settings.API_ROOT + image
    }
    return image ? image : 'https://i.imgur.com/uuykYlB.png';
};
