import '@babel/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, Router } from "react-router";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import promise from 'redux-promise';
import thunk from 'redux-thunk';
import routes from "./config/routes";
import rootReducer from './reducers/index';
import './assets/scss/main.scss';
import './style.css';

//todo remove composeWithDevtools in production

const store = createStore(
    rootReducer,
    applyMiddleware(thunk, promise, createLogger())
);

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory} routes={routes} />
    </Provider>,
    document.getElementById('root')
);
