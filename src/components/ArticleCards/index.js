import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';
import moment from 'moment';
import { getArticleImage } from '../../utils/general';
import './ArticleCards.scss';


class ArticleCards extends Component {

    renderTimeAgo() {
        const { article } = this.props;
        return (
            <div className="level-item has-text-centered detail-text">
                <TimeAgo date={article.date} minPeriod="60" />
            </div>
        );
    }

    renderContent() {
        const { article, articles } = this.props;
        let opinions = "opinions";
        if (article.opinions_count === 1) opinions = "opinion";
        const now = moment();
        const postDate = moment(article.date);
        const daysAgo = now.diff(postDate, 'days');

        return (
            <div className="content has-text-centered">

                <div className="card-title">
                    {daysAgo < 2 && <span className="tag is-danger breaking-tag"> BREAKING </span>}
                    {article.title}
                </div>
                <nav className="level">
                    {this.renderTimeAgo()}
                    <div className="level-item has-text-centered detail-text">
                        <span className="icon">
                            <i className="far fa-comment-alt">
                            </i>
                        </span>
                        {article.opinions_count} {opinions}
                    </div>
                </nav>
            </div>
        );
    }

    renderThumbnail() {
        const { article } = this.props;
        const articleImage = {
            backgroundImage: `url(${getArticleImage(article)})`
        };

        return (
            <figure className="image is-2by1 article-image" style={articleImage}>
            </figure>
        );
    }


    render() {
        const { article } = this.props;
        return (
            <div className="column is-12-mobile is-4-tablet is-3-widescreen is-3-desktop">
                <Link className="card" to={{
                    pathname: `/articles/${article.id}`,
                    state: { article: article }
                }}>
                    <div className="article-box link-box box-style">
                        <div className="card-image">
                            {this.renderThumbnail()}
                        </div>
                        <div className="card-content">
                            {this.renderContent()}
                        </div>
                    </div>
                </Link>
            </div>
        );
    }

}

ArticleCards.propTypes = {
    article: PropTypes.object.isRequired,
};

export default connect(state => ({
    articles: state.articles.data,
}))(ArticleCards);
