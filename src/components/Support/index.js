import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './support.scss';
import { createReplyVote } from '../../actions/votes/reply-vote/create';
import { editReplyVote } from '../../actions/votes/reply-vote/edit';
import { deleteReplyVote } from '../../actions/votes/reply-vote/delete';

class Support extends Component {

    state = {
        b1: false,
        b2: false,
        b3: false,
        b4: false,
        b5: false,
    }

    componentDidMount() {
        this.initState();
    }

    initState = () => {
        const vote = this.usersVote();
        let v_k;
        if (vote) {
            const v_k = "b" + vote.value;
            this.setState({[v_k]: true});
        }
    }

    handleClick = e => console.log('button clicked for' + e.target.value);

    isEmpty(obj) {
        return !obj || Object.keys(obj).length === 0;
    }
    usersVote() {
        const {activeUser, replyVotes, post} = this.props;
        if (!activeUser || !post) return null;
        var replyVote;
        if (this.isEmpty(replyVotes) && post.votes.length==0) return null;
        else if (this.isEmpty(replyVotes)) replyVote = post.votes;
        else if (replyVotes) replyVote = replyVotes;

        const vote = Object.values(replyVote)
            .filter(r => r.user.id === activeUser.id && r.object_id === post.id)
        if(vote.length) return vote[0];
        return null;
    }


    onSubmit = (vote) => {
        const {activeUser, dispatch, post} = this.props;
        const v_k = "b" + vote;
        let type = '';
        if (post.post == null) type = 'P';
        else type = 'R';
        if(!activeUser) return null;
        const currVote = this.usersVote();
        if(currVote === null) {
            dispatch(createReplyVote({
                value: vote,
                recipient: post.user,
                content_object: post,
                type_name: type
            }));
            this.setState({[v_k]: true});
            this.props.setSupport(vote);
        }
        else if (currVote.value != vote) {
            const ov_k = "b" + currVote.value;
            dispatch(editReplyVote({
                ...currVote,
                value: vote
                }));

            this.setState({[ov_k]: false});
            this.setState({[v_k]: true});
            this.props.setSupport(vote);
        }
        else {
            const ov_k = "b" + currVote.value;
            dispatch(deleteReplyVote(this.usersVote()));
            this.setState({[ov_k]: false});
            this.props.setSupport(0);
        }
        this.props.click();
    };

    render() {
        const { handleSubmit } = this.props;
        const { b1, b2, b3, b4, b5} = this.state;
        if (this.props.visible) {
            return (
                <div className="user agree">

                    <button className={classnames('button vote button-one', {'is-selected': this.state.b1})} onClick={handleSubmit(values =>
                      this.onSubmit(1))}>1</button>
                    <button className={classnames('button vote button-two', {'is-selected': this.state.b2})} onClick={handleSubmit(values =>
                      this.onSubmit(2))}>2</button>
                    <button className={classnames('button vote button-three', {'is-selected': this.state.b3})} onClick={handleSubmit(values =>
                      this.onSubmit(3))}>3</button>
                    <button className={classnames('button vote button-four', {'is-selected': this.state.b4})} onClick={handleSubmit(values =>
                      this.onSubmit(4))}>4</button>
                    <button className={classnames('button vote button-five', {'is-selected': this.state.b5})} onClick={handleSubmit(values =>
                      this.onSubmit(5))}>5</button>

                </div>
            )
        } else {
            return null;
        }
    }
}

Support = reduxForm({
    form: 'Support',
})(Support);

Support.propTypes = {
    visible: PropTypes.bool,
    post: PropTypes.object,
    setSupport: PropTypes.func,
};

export default connect(state => ({
    activeUser: state.activeUser,
    replyVotes: state.replyVotes.data,
}))(Support);
