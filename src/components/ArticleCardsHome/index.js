import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import PropTypes from 'prop-types';

import ResponsivePicture from '../../components/Picture'
import Clock from '../../components/Clock';

class ArticleCards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isClockVisible: false,
        };
    }

    renderContent() {

        const { article } = this.props;

        return (
            <div className="columns">
                <div className="column">

                    <h1 className="is-size-3 has-text-weight-bold">{article.title}</h1>
                    <div className="user">
                        <span className="user-text">Todays host</span>
                        <span className="user-text"><img className="user-icon" src="http://localhost:8000/media/user.png" /></span>
                        <span className="user-text" >User name</span>
                    </div>
                    {<Clock visible={this.state.isClockVisible} />}
                </div>

                <div className="column">
                    <ResponsivePicture article={article} />
                </div>

            </div>
        );
    }


    render() {
        const { article } = this.props;
        return (
            <div>
                <Link className="card" to={{ pathname: `/articles/${article.id}`, state: { article: article } }}>
                    <div className="columns">
                        {this.renderContent()}
                    </div>
                </Link>
            </div>

        );
    }

}

ArticleCards.propTypes = {
    article: PropTypes.object.isRequired,
};

export default connect(state => ({
    articles: state.articles.data,
}))(ArticleCards);
