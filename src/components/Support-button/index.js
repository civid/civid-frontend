import React, { Component } from 'react';
import PropTypes from 'prop-types';
import "./support-button.scss"

class SupportButton extends Component {

    render() {
        return (

            <a className="button is-white action-button" onClick={this.props.click} id="tutorial2a">
                <span className="icon">
                    <i className="fas fa-thumbs-up" style={{fontSize: "1rem", marginTop: "-.3rem", color: "#4a4a4a" }}></i>
                </span>
                <span className="action-text">Support</span>
            </a>

        )
    }
}

SupportButton.propTypes = {
    click: PropTypes.func
};
export default SupportButton;
