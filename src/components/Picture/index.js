import React, { Component } from 'react';
import PropTypes from 'prop-types';
import settings from '../../config/settings';

class ResponsivePicture extends Component {

    tag = (image, size, extension, key, len) => {
        if (len !== key) {
            return (<source srcSet={`${settings.API_ROOT}${image}-${size}w.${extension}`} key={key} media={`(max-width: ${size}px)`} />)
        } else {
            return (<img srcSet={`${settings.API_ROOT}${image}-${size}w.${extension}`} key={key} />)
        }
    }

    render() {

        const { post } = this.props

        const [image, extension] = post.image ? post.image.split(".") : 'https://i.imgur.com/uuykYlB.png'
        const tempImage = post.image ? post.image : 'https://i.imgur.com/uuykYlB.png'
        const len = post.images_sizes.length - 1

        return (
            <img className="opinion-img" src={tempImage} />
        )
    }
}

ResponsivePicture.propTypes = {
    post: PropTypes.object
};

export default ResponsivePicture;

