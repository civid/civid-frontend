import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import './claim.scss';


const Claim = ({claim, onExpand}) => (
    <div className="claim-box" onClick={onExpand}>
        <div className="claim-content">
            {claim}
        </div>
    </div>
);

Claim.propTypes = {
    claim: PropTypes.string.isRequired,
    onExpand: PropTypes.func.isRequired
};

export default Claim;
