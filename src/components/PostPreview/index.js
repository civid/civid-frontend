import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';
import {getProfileImage} from '../../utils/user';
import './PostPreview.scss';

class PostPreview extends Component {


    getPostImage() {
        const {post} = this.props;
        return post.image ? post.image : 'http://i.imgur.com/1HHJKXC.png';
    }

    renderReplyCount() {
        const {post, post: {post_reply_count}} = this.props;
        var replies = "responses";
        if (post_reply_count === 1) replies = "response";

        return (
            <div className="level-item detail-text">
                <span className="icon">
                    <i className="fas fa-comment-alt">
                    </i>
                </span>
                {post_reply_count} {replies}
            </div>
        );
    }


    renderThumbnail() {
        const postImage = {
            backgroundImage: `url(${this.getPostImage()})`
        };

        return (
            <figure className="image is-2by1 article-image" style={postImage}>
            </figure>
        );
    }

    getVoteTally() {
        const {post} = this.props;
        const replyVotes = post.votes;
        if (!replyVotes) return 0;
        return Object.values(replyVotes)
            .reduce((acc, replyVote) => acc + replyVote.value, 0);
    }

    renderContent() {
        const { post, users } = this.props;
        return (
            <div className="content media-content">

                <div className="card-title">
                    {post.title}
                </div>
                    <div className="influence-details">
                        {this.getVoteTally()} influence
                    </div>

                <nav className="level is-mobile is-marginless">
                    <div className="level-left influence-details">
                        <div className="user level-item">
                            <img className="user-img size-30" src={getProfileImage(post.user, users)} />
                        </div>
                        <div className="level-item">
                            <div className="post-name">{post.username}
                            </div>
                        </div>

                        <div className="level-item time-ago">
                            <TimeAgo date={post.pub_date} minPeriod="60" />
                        </div>
                    </div>
                </nav>
            </div>
        );
    }

    render() {
        const {activeUser, children, post} = this.props;

        return (
            <div className="column is-12-mobile is-4-tablet is-4-widescreen is-4-desktop">
          <Link className="card" to={`/posts/${post.slug}`}>
                    <div className="article-box link-box box-style">
                        <div className="card-image">
                            {this.renderThumbnail()}
                        </div>
                        <div className="card-content">
                            {this.renderContent()}
                        </div>
                    </div>
                </Link>
            </div>
        );
    }

}

PostPreview.propTypes = {
    post: PropTypes.object.isRequired,
};

export default connect(state => ({
    activeUser: state.activeUser,
    postReplies: state.postReplies.data,
    users: state.users.data
}))(PostPreview);
