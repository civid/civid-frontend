import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import PropTypes from 'prop-types';
import './SourceList.scss';


class SourceList extends Component {



    render() {
        const {source} = this.props;

        return (
            <li className="pub-list">
                <article className="media centered-content">
                    <figure className="media-left">
                        <img className="user-img" src={source.pub.image} height="25" width="25"/>
                    </figure>
                    <div className="media-content pub-name">
                        {source.pub.name}
                    </div>
                </article>
                <a className="source-title" href={source.url} target="_blank">{source.title}</a>
            </li>
        );
    }
}

export default connect((state, props) => ({
}))(SourceList);
