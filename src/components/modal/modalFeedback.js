import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FeedbackForm from '../../forms/FeedbackForm';

class ModalFeedbackForm extends Component {

    render() {
        const { visible, modalClose} = this.props;

        if (visible) {
            return (
                <div className="modal is-active">
                    <div className="modal-bg" />
                    <div className="modal-card post-width">
                        <header className="modal-card-head">
                            <p className="modal-card-title">Feedback</p>
                            <button className="delete" onClick={modalClose} aria-label="close" />
                        </header>
                        <section className="modal-card-body">
                            <FeedbackForm closeModal={this.props.modalClose} />
                        </section>
                    </div>
                </div>
            )
        } else {
            return (null)
        }
    }
}

ModalFeedbackForm.propTypes = {
    modalClose: PropTypes.func,
    visible: PropTypes.bool,
};

export default ModalFeedbackForm;


