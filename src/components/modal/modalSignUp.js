import React, { Component } from 'react';
import {Link} from 'react-router';
import PropTypes from 'prop-types';
import settings from '../../config/settings';
import SignUpForm from '../../forms/SignUpForm';

class ModalSignUpForm extends Component {

    render() {
        const { visible, modalClose } = this.props;

        if (visible) {
            return (
                <div className="modal is-active">
                    <div className="modal-bg" />
                    <div className="modal-card post-width">
                        <header className="modal-card-head">
                            <p className="modal-card-title">Sign Up</p>
                            <button className="delete" onClick={modalClose} aria-label="close" />
                        </header>
                        <section className="modal-card-body">
                        <div className="signup-content">
                        <div className="title is-4">Join our community</div>
                        <div className="subtitle is-6 has-text-grey">Take part in a global conversation dedicated to learning more about each other.</div>

                        <SignUpForm closeModal={this.props.modalClose} />
                        <p style={{paddingTop:"1rem"}}>Already have an account? <Link to={`/login`}>Sign In</Link></p>
                        <p className="privacy-agree">By signing up, you agree to our <Link to={`/terms`}>Terms</Link> and that you have read our <Link to={`/privacy`}>Privacy Policy</Link> and <Link to={`/content-policy`}>Content Policy</Link>.
                        </p>
                    </div>

                        </section>
                    </div>
                </div>
            )
        } else {
            return (null)
        }
    }
}

ModalSignUpForm.propTypes = {
    modalClose: PropTypes.func,
    visible: PropTypes.bool
};

export default ModalSignUpForm;


