import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReportForm from '../../forms/ReportForm';

class ModalReportForm extends Component {

    render() {
        const { visible, modalClose, post } = this.props;
        let postType = "R";
        if (post.post == null) postType = "P";
        let title = "";
        if (postType == "R") title = "Counter-Claim";
        else title = "Claim";

        if (visible) {
            return (
                <div className="modal is-active">
                    <div className="modal-bg" />
                    <div className="modal-card post-width">
                        <header className="modal-card-head">
                            <p className="modal-card-title">Report {title}</p>
                            <button className="delete" onClick={modalClose} aria-label="close" />
                        </header>
                        <section className="modal-card-body">
                            <ReportForm post={post} postType={postType} closeModal={this.props.modalClose} />
                        </section>
                    </div>
                </div>
            )
        } else {
            return (null)
        }
    }
}

ModalReportForm.propTypes = {
    modalClose: PropTypes.func,
    visible: PropTypes.bool,
    post: PropTypes.object
};

export default ModalReportForm;


