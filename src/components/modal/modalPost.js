import React, { Component } from 'react';
import PropTypes from 'prop-types';
import settings from '../../config/settings';
import OpinionForm from '../../forms/OpinionForm';

class ModalPostForm extends Component {

    render() {
        const { visible, modalClose } = this.props;

        if (visible) {
            return (
                <div className="modal is-active">
                    <div className="modal-bg" />
                    <div className="modal-card post-width">
                        <header className="modal-card-head">
                            <p className="modal-card-title">Submit your opinion to Civid</p>
                            <button className="delete" onClick={modalClose} aria-label="close" />
                        </header>
                        <section className="modal-card-body">
                            <OpinionForm closeModal={this.props.modalClose} />
                        </section>
                    </div>
                </div>
            )
        } else {
            return (null)
        }
    }
}

ModalPostForm.propTypes = {
    modalClose: PropTypes.func,
    visible: PropTypes.bool
};

export default ModalPostForm;


