import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReportForm from '../../forms/ReportForm';

class ModalDelete extends Component {

    render() {
        const { visible, modalClose} = this.props;

        if (visible) {
            return (
                <div className="modal is-active">
                    <div className="modal-bg" />
                    <div className="modal-card post-width">
                        <header className="modal-card-head">
                            <p className="modal-card-title">Delete Post</p>
                            <button className="delete" onClick={modalClose} aria-label="close" />
                        </header>
                        <section className="modal-card-body">
                            Are you sure?
                            <div className="buttons">
                            <button className="button is-danger" onClick={() => {this.props.modalClose(); this.props.willDelete();}}>Yes</button>
                            <button className="button" onClick={modalClose}>No</button>
                            </div>
                        </section>
                    </div>
                </div>
            )
        } else {
            return (null)
        }
    }
}

ModalDelete.propTypes = {
    modalClose: PropTypes.func,
    visible: PropTypes.bool,
    willDelete: PropTypes.func,
};

export default ModalDelete;


