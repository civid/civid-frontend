import React, { Component } from 'react';
import "./rules-discussion.scss"

class RulesOfDiscussion extends Component {

    render() {
        return (
            <div className="card opinion">
                <header className="card-header header-size center-title">
                    <p className="title is-6">Rules of Discussion</p>
                </header>

                <div className="card-content">
                    <div className="content">
                        <ol className="margin-list">
                            <li>
                                <p>Do not use hostile or offensive language.</p>
                            </li>
                            <li>
                                <p>Direct responses mist challenge the initiator's arguments in some way.</p>
                            </li>

                            <li>
                                <p>Responses must contribute meanningfully to the discussion.</p>
                            </li>
                            <li>
                                <p>Do not directly accuse the initiator. If you have suspicions that they are not playing fair, pkease message us.</p>
                            </li>
                            <li>
                                <p>A single trophy is awarded for the replay that best changes your mind, if any. Do not award sarcastic joking, or otherwise non-serious trophies.</p>
                            </li>
                        </ol>


                    </div>

                </div>
            </div>
        )
    }
}

export default RulesOfDiscussion;
