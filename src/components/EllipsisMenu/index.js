import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link, browserHistory} from 'react-router';
import classnames from 'classnames';
import ModalReportForm from '../../components/modal/modalReport';
import ModalDelete from '../../components/modal/modalDelete';


class EllipsisMenu extends Component {

  state = {
    open: false,
    isReportModalVisible: false,
    isDeleteModalVisible: false,
  }

  componentDidMount() {
    document.addEventListener('click', this.close);
  }


  componentWillUnmount() {
    document.removeEventListener('click', this.close);
    }

  reportModalOpenClose = (e) => {
    const {activeUser} = this.props;
    if (!activeUser) this.props.SignUpModalOpenClose();
    else this.setState({ 'isReportModalVisible': !this.state.isReportModalVisible });
  }
  deleteModalOpenClose = (e) => {
      this.setState({isDeleteModalVisible: !this.state.isDeleteModalVisible})
  }

  close = (evt) => {
    if (this.props.hoverable || (evt && evt.path.find(node => node === this.htmlElement))) {
      return;
    }
    this.setState({ open: false });
  }

  toggle = (evt) => {
    if (this.props.hoverable) {
      return;
    }
    if (evt) {
      evt.preventDefault();
    }
    this.setState({ open: !this.state.open });
  }

  select = value => () => {
    if (this.props.onChange) {
      this.props.onChange(value);
    }
    this.close();
  }

    render() {
      const {activeUser, canDelete, willDelete, post, className} = this.props;
      return (
        <div ref={(node) => { this.htmlElement = node; }}
        className={classnames('dropdown', className, {
          'is-active': this.state.open,
        })}>
        <div className="dropdown is-right level-item">
        <div className="dropdown-trigger" role="presentation" onClick={this.toggle}>
            <button className="button is-ellipsis" aria-haspopup="true" aria-controls="dropdown-menu">
                <span className="icon">
                    <i className="fas fa-ellipsis-h fa-lg" aria-hidden="true"></i>
                </span>
            </button>
        </div>
        <div className="dropdown-menu" id="dropdown-menu" role="menu">
            <div className="dropdown-content">
                {canDelete && <Link onClick={this.deleteModalOpenClose} className="dropdown-item">
                    Delete
                </Link>}
                <Link onClick={this.reportModalOpenClose} className="dropdown-item">
                  Report
                </Link>
                <ModalReportForm visible={this.state.isReportModalVisible} modalClose={this.reportModalOpenClose}
                        post={post} />
                <ModalDelete visible={this.state.isDeleteModalVisible}
                  modalClose={this.deleteModalOpenClose}
                  willDelete={willDelete} />
            </div>
        </div>
    </div>
    </div>);
  }
}

EllipsisMenu.defaultProps = {
    canDelete: false,
};

EllipsisMenu.propTypes = {
    canDelete: PropTypes.bool,
    willDelete: PropTypes.func,
    post: PropTypes.object
};

export default EllipsisMenu;
