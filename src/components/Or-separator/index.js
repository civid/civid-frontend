import React, { Component } from "react"

const style = {
    marginBottom: "14px",
}

class OrSeparator extends Component {

    render() {
        return (
            <div className="center is-size-5 has-text-black" style={style}>
                or
            </div>
        )
    }
}


export default OrSeparator;