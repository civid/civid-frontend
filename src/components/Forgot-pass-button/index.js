import React, { Component } from 'react';
import { Link } from 'react-router';

const style = {
    marginTop: "10px",
    marginBottom: "10px"
}

class ForgotPasswordButton extends Component {

    render() {
        return (
            <Link style={style}
                to="forgot-password"
                className="button is-medium is-fullwidth">Forgot password ?

            </Link>
        )
    }
}

export default ForgotPasswordButton