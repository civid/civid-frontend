import React, { Component } from 'react';
import RichTextEditor from 'react-rte';
import './editor.scss';

const editorStyle = {
    height: "150px",
    fontFamily: "Open Sans",
    fontSize: ".9rem"
}

class RichTextMarkdown extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.input.value === '' ?
                RichTextEditor.createEmptyValue() :
                RichTextEditor.createValueFromString(this.props.input.value, 'html')
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.input.value !== this.state.value.toString('html')) {
            this.setState({
                value: nextProps.input.value ?
                    RichTextEditor.createValueFromString(nextProps.input.value, 'html') :
                    RichTextEditor.createEmptyValue()
            });
        }
    }

    onChange(value) {

        const isTextChanged = this.state.value.toString('html') != value.toString('html');
        this.setState({ value }, e => isTextChanged && this.props.input.onChange(value.toString('html')));

    };

    render() {
        const { placeholder } = this.props;

        const toolbarConfig = {
            // display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
            display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS', 'HISTORY_BUTTONS'],

            INLINE_STYLE_BUTTONS: [
                { label: 'Bold', style: 'BOLD', className: 'custom-css-class' },
                { label: 'Italic', style: 'ITALIC' },
                { label: 'Underline', style: 'UNDERLINE' }
            ],
            // not working
            // BLOCK_TYPE_DROPDOWN: [
            //     { label: 'Normal', style: 'unstyled' },
            //     { label: 'Heading Large', style: 'header-one' },
            //     { label: 'Heading Medium', style: 'header-two' },
            //     { label: 'Heading Small', style: 'header-three' }
            // ],
            BLOCK_TYPE_BUTTONS: [
                { label: 'UL', style: 'unordered-list-item' },
                { label: 'OL', style: 'ordered-list-item' },
                { label: 'Blockquote', style: 'blockquote' }
            ]
        };
        return (
            <RichTextEditor
                value={this.state.value}
                onChange={this.onChange.bind(this)}
                toolbarConfig={toolbarConfig}
                editorStyle={editorStyle}
                placeholder={placeholder}

            />
        );
    }
}

export default RichTextMarkdown;
