import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import InfiniteScroll from "react-infinite-scroll-component";
import includes from "lodash/includes";
import isEqual from "lodash/isEqual";
import pick from "lodash/pick";

import {deletePost} from "../../actions/posts/post/delete";
import PostListItem from "./postListItem";
import {getNextPostsPage, getPostList} from "../../actions/posts/post/list";
import {POST_LIST_SORT_TYPES, POST_LIST_FILTER_TYPES} from './postListOptions';
import {browserHistory} from 'react-router';

class PostList extends Component {

    componentDidMount() {
        PostList._fetchPosts(this.props);
    }

    componentWillReceiveProps(nextProps) {
        const fetchPropNames = ['filter', 'filterValue', 'sort'];
        const currentFetchProps = pick(this.props, fetchPropNames);
        const nextFetchProps = pick(nextProps, fetchPropNames);
        if (!isEqual(currentFetchProps, nextFetchProps))
            PostList._fetchPosts(nextProps);
    }

    onFetchNextPage = () => this.props.dispatch(getNextPostsPage());

    onDeletePost = id => {
        try {
            this.props.dispatch(deletePost({id}))
            .then(browserHistory.push('/home'));
        }
        catch(error) {
            throw error;
        }
    }

    static _fetchPosts(props) {
        const {dispatch, filter, sort, filterValue} = props;
        const params = {};
        if (includes(Object.values(POST_LIST_SORT_TYPES), sort))
            params.sort = sort;
        if (includes(Object.values(POST_LIST_FILTER_TYPES), filter) && !!filterValue) {
            params[filter] = filterValue;
        }
        dispatch(getPostList(params));
    }

    render() {
        const {
            posts,
            nextPostsPagePresent,
            articles,
            activeUser,
            postReplies,
            users,
            renderHeader,
            responseLabel,
            responsesLabel,
            filter,
        } = this.props;
        if (posts.length === 0) {
            let message = "No discussions have been created yet!";
            if (filter === "user") message = "This user hasn't created any discussions yet!";
            else if (filter === "following") message = "You're not following anyone yet! Follow someone to see their activity.";
            return (<div className="empty-section">
                {message}
                </div>);
        }

        const postConfigProps = {renderHeader, responseLabel, responsesLabel};
        const postLookupProps = {articles, activeUser, postReplies, users};

        return (<InfiniteScroll
            dataLength={posts.length}
            next={this.onFetchNextPage}
            hasMore={nextPostsPagePresent}
            loader={<p className="has-text-centered">
                <i className="fas fa-spinner fa-pulse fa-3x" />
            </p>}
            endMessage={<h6 className="subtitle is-6 has-text-centered end-feed">End of feed.</h6>}
            style={{overflow: 'visible'}}>
            {posts.map(post => <PostListItem
                key={post.id}
                post={post}
                {...postLookupProps}
                {...postConfigProps}
                onDeletePost={this.onDeletePost}
            />)}
        </InfiniteScroll>);
    }
}

PostList.propTypes = {
    posts: PropTypes.array.isRequired,
    nextPostsPagePresent: PropTypes.bool.isRequired,
    articles: PropTypes.object.isRequired,
    activeUser: PropTypes.object,
    postReplies: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired,
    filter: PropTypes.string,
    filterValue: PropTypes.string,
    sort: PropTypes.string,
    renderHeader: PropTypes.bool,
    responseLabel: PropTypes.string,
    responsesLabel: PropTypes.string,
};

export default connect(state => ({
    posts: state.posts.list,
    nextPostsPagePresent: !!state.posts.pagination.nextPageUrl,
    articles: state.articles.data,
    activeUser: state.activeUser,
    postReplies: state.postReplies.data,
    users: state.users.data,
}))(PostList);
