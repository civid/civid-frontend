export const POST_LIST_SORT_TYPES = {
    COMMENTS: 'comments',
};

export const POST_LIST_FILTER_TYPES = {
    USER: 'user',
    ARTICLE: 'article',
    POST_AGE: 'post_age_days',
    FOLLOWING: 'following',
};
