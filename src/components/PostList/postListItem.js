import React, {Component} from 'react';
import {Link} from 'react-router';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';
import Truncate from 'react-truncate';
import {getArticleImage} from '../../utils/general';
import EllipsisMenu from '../EllipsisMenu';
import './PostListItem.scss';


class PostListItem extends Component {

    setDelete = () => this.props.onDeletePost(this.props.post.id);

    renderReplyCount() {
        const {post, post: {post_reply_count}, responseLabel, responsesLabel} = this.props;
        const label = post_reply_count === 1 ? responseLabel : responsesLabel;

        return (
            <Link className="level-item detail-text" to={`/articles/${post.article}/posts/${post.id}`}>
                <span className="icon">
                    <i className="fas fa-comment-alt">
                    </i>
                </span>
                {post_reply_count} {label}
            </Link>
        );
    }

    renderTimeAgo() {
        const {post} = this.props;
        return (
            <div className="level-item detail-text">
                <TimeAgo date={post.created_date} minPeriod="60"/>
            </div>
        );
    }


    renderPostHeader() {
        const {articles, post} = this.props;

        if (!post || Object.values(articles).filter(article => article.id === post.article).length === 0) return null;

        return (
            <div className="preview-header">
                <article className="media centered-content">
                    <figure className="media-left">
                        <img className="change-ratio" src={getArticleImage(articles[post.article])}/>
                    </figure>
                    <div className="media-content">
                        <div className="detail-text">
                            {articles[post.article].title}
                        </div>
                    </div>
                </article>
            </div>
        );
    }

    render() {
        const {activeUser, post, users, renderHeader} = this.props;
        const canDeletePost = activeUser && activeUser.id === post.user;
        return (
            <div className="post-box box-style">
                <div className="post-content">
                {renderHeader && this.renderPostHeader()}
                    <Link to={`/articles/${post.article}/posts/${post.id}`} className="title post-title is-marginless">
                        {post.title}
                    </Link>


                    <div className="post-short body-text">
                    <Truncate lines={3} ellipsis={<span>... <Link to={`/articles/${post.article}/posts/${post.id}`}>(more)</Link></span>}>
                        {post.body}
                    </Truncate>
                    </div>
                </div>
                <div className="post-bottom">
                    <div className="post-details">
                        <nav className="level is-mobile">
                            <div className="level-left">
                                <Link
                                    to={`/articles/${post.article}/posts/${post.id}`}
                                    className="level-item button is-blue is-small view-btn">
                                    View
                                </Link>

                                {this.renderTimeAgo()}

                                <div className="level-item">
                                    {' · '}
                                </div>

                                <div className="level-item">
                                    {this.renderReplyCount()}
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        );
    }

}

PostListItem.defaultProps = {
    renderHeader: true,
    responseLabel: 'response',
    responsesLabel: 'responses',
};

PostListItem.propTypes = {
    post: PropTypes.object.isRequired,
    articles: PropTypes.object.isRequired,
    activeUser: PropTypes.object,
    postReplies: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired,
    renderHeader: PropTypes.bool,
    responseLabel: PropTypes.string,
    responsesLabel: PropTypes.string,
    onDeletePost: PropTypes.func.isRequired,
};

export default PostListItem;
