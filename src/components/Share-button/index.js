import React, { Component } from 'react';

class ShareButton extends Component {

    render() {
        return (

            <a className="button is-white is-large">
                <span className="icon">
                    <i className="fas fa-share-alt" style={{fontSize: "1.2rem", marginTop: "-.3rem", color: "#4a4a4a" }}></i>
                </span>
                <span className="action-text">Share</span>
            </a>
        )
    }
}

export default ShareButton;
