import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './opinion-card.scss'


class OpinionCard extends Component {

    render() {
        const { activeUser, click } = this.props

        return (
            <div className="content-box box-style">
                <header className="card-header header-color header-size">
                    &nbsp;
                </header>

                <div className="card-content">
                    <div className="content">
                        <p className="title is-6">Welcome to Civid</p>
                        <p>Civid hosts users with controversial opinions who are open to discuss and engage with different perspectives. Want to be featured on Civid? Submit your opinion to us, and we may select you to host the discussion.</p>
                    </div>
                    <a className="button support-button opinion-button" onClick={click}>
                        <span className="support">Submit Opinion</span>
                    </a>
                </div>
            </div>
        )
    }
}

OpinionCard.propTypes = {
    click: PropTypes.func
}

export default OpinionCard;
