import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './clains-counter.scss';

class ClaimsCounter extends Component {

    render() {
        return (
            <span>
                <span className="icon claims">
                    <i className="far fa-comment-alt"></i>
                </span>
                <span className="claim">{this.props.counter} counter-claims</span>
            </span>
        )

    }
}

ClaimsCounter.propTypes = {
    counter: PropTypes.number
};

export default ClaimsCounter;