import React, { Component } from "react";
import PropTypes from "prop-types";

class AccordionSection extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Object).isRequired,
    isOpen: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  };

  onClick = () => {
    this.props.onClick(this.props.label);
  };

  render() {
    const {
      onClick,
      props: { isOpen, label }
    } = this;
    let cname = "claim-content";
    let style = {};
    if (!isOpen) style = {cursor: "pointer", textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap", width: "95%"};
    return (
      <div className={cname} onClick={onClick}>
            {!isOpen && <div style={style}>{label}</div>}
          <div style={{ position: "absolute", right:"1rem", top:"35%"}}>
            {!isOpen && <span><i className="fas fa-chevron-up" style={{ fontSize: "1.4rem", color: "#3081f9", fontWeight: "800" }}></i></span>}
            {isOpen && <span><i className="fas fa-chevron-down" style={{ fontSize: "1.4rem", color: "#3081f9", fontWeight: "800" }}></i></span>}
          </div>
            {isOpen && this.props.children}
      </div>
    );
  }
}

export default AccordionSection;
