import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import classnames from 'classnames';
import {
  FacebookShareButton,
  TwitterShareButton,
  FacebookIcon,
  TwitterIcon
} from 'react-share';


class ShareMenu extends Component {

  state = {
    open: false,
  }

  componentDidMount() {
    document.addEventListener('click', this.close);
  }


  componentWillUnmount() {
    document.removeEventListener('click', this.close);
    }


  close = (evt) => {
    if (this.props.hoverable || (evt && evt.path.find(node => node === this.htmlElement))) {
      return;
    }
    this.setState({ open: false });
  }

  toggle = (evt) => {
    if (this.props.hoverable) {
      return;
    }
    if (evt) {
      evt.preventDefault();
    }
    this.setState({ open: !this.state.open });
  }

  select = value => () => {
    if (this.props.onChange) {
      this.props.onChange(value);
    }
    this.close();
  }

    render() {
      const {canDelete, willDelete, className} = this.props;
      const shareUrl = 'https://civid.com';
      const title = 'Civid';

      return (
        <div ref={(node) => { this.htmlElement = node; }}
        className={classnames('dropdown', className, {
          'is-active': this.state.open,
        })}>
        <div className="dropdown is-right level-item">
        <div className="dropdown-trigger" role="presentation" onClick={this.toggle}>
            <button className="button is-ellipsis" aria-haspopup="true" aria-controls="dropdown-menu">
                <span className="icon">
                    <i className="fas fa-share-alt" style={{fontSize: "1.2rem", marginTop: "-.3rem", color: "#4a4a4a" }}></i>
                </span>
                <span className="action-text">Share</span>
            </button>
        </div>
        <div className="dropdown-menu" id="dropdown-menu" role="menu">
            <div className="dropdown-content">
              <div>
                <FacebookShareButton
                  url={shareUrl}
                  quote={title}>
                  <FacebookIcon
                    size={25} />
                </FacebookShareButton>
              </div>
              <div>
                <TwitterShareButton
                  url={shareUrl}
                  quote={title}>
                  <TwitterIcon
                    size={25} />
                </TwitterShareButton>
              </div>
            </div>
        </div>
    </div>
    </div>);
  }
}

ShareMenu.defaultProps = {
    canDelete: false,
};

ShareMenu.propTypes = {
    canDelete: PropTypes.bool,
    willDelete: PropTypes.func,
};

export default ShareMenu;
