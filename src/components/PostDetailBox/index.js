import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';
import {deletePost} from '../../actions/posts/post/delete';
import {getFullName, getProfileImage} from '../../utils/user';
import {getArticle} from '../../actions/articles/article/get';
import {getArticleImage} from '../../utils/general';
import EllipsisMenu from '../EllipsisMenu';
import UserInfo from '../UserInfo';
import ReadMore from '../ReadMore';
import './PostDetailBox.scss';


class PostDetailBox extends Component {
    componentDidMount() {
        const {dispatch, post} = this.props;
        dispatch(getArticle(post.article));
    }

    getPostImage() {
        const {post} = this.props;
        return post.image ? post.image : 'http://i.imgur.com/1HHJKXC.png';
    }

    handleDelete = () => {
        const {dispatch, post} = this.props;
        dispatch(deletePost(post));
    };

    renderDelete() {
        const {activeUser, post} = this.props;
        if(!activeUser || activeUser.id !== post.user) return null;
        return (
            <a href="" className="dropdown-item">
                <span>
                    <a className="dropdown-link" onClick={this.handleDelete}>Delete</a>
                </span>
            </a>
        );
    }

    renderReplyCount() {
        const {post, post: {post_reply_count}} = this.props;
        var replies = "responses";
        if (post_reply_count === 1) replies = "response";

        return (
            <div className="level-item detail-text">
                <span className="icon">
                    <i className="fas fa-comment-alt">
                    </i>
                </span>
                {post_reply_count} {replies}
            </div>
        );
    }

    renderViews() {
        const {post} = this.props;
        return (
            <div className="level-item detail-text">
                <span className="icon">
                    <i className="fas fa-eye"></i>
                </span>
                0 views
            </div>
        );
    }

    renderTimeAgo() {
        const {post} = this.props;
        return (
            <div className="time-ago detail-text level-item">
                <TimeAgo date={post.created_date} minPeriod="60"/>
            </div>
        );
    }

    renderThumbnail() {
        const {post} = this.props;
        return (
            <div className="thumbnail-container">
                <Link to={`/profile/${post.user}/posts/${post.id}`}>
                    <img className="thumbnail" src={this.getPostImage()}/>
                </Link>
            </div>
        );
    }

    render() {
        const {activeUser, children, post, users} = this.props;

        return (
                <div className="postdetail-section">
                    <div className="column is-9">
                    <div className="post-title">
                        <div className="cmm">Change my mind: </div>
                        <div className="title is-3">{post.title}</div>
                    </div>
                <p className="post-body has-text-white">
                <ReadMore lines={3} className="readmore-white">
                    {post.body}
                </ReadMore>
                </p>
                <UserInfo userId={post.user} users={users} userClass="user-name has-text-white"/>
                </div>
            </div>
        );
    }

}

PostDetailBox.propTypes = {
    post: PropTypes.object.isRequired,
};

export default connect(state => ({
    articles: state.articles.data,
    activeUser: state.activeUser,
    postReplies: state.postReplies.data,
    users: state.users.data
}))(PostDetailBox);
