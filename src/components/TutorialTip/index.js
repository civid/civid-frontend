import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import classnames from 'classnames';
import './TutorialTip.scss';


class TutorialTip extends Component {

  state = {
    open: true,
    bg: { }
  }

  componentDidMount() {
    document.addEventListener('click', this.close);
  }


  componentWillUnmount() {
    document.removeEventListener('click', this.close);
    }

  close = (evt) => {
    const {className, nextTip} = this.props;
    if (this.props.hoverable || (evt && evt.path.find(node => node === this.htmlElement))) {
      return;
    }
    this.setState({ open: false, bg: {} });
    nextTip(className);
  }

  toggle = (evt) => {
    if (this.props.hoverable) {
      return;
    }
    if (evt) {
      evt.preventDefault();
    }
    this.setState({ open: !this.state.open });
  }

    render() {
      const {className, text} = this.props;

      return (
        <div ref={(node) => { this.htmlElement = node; }}
        className={classnames('dropdown',className, {
          'is-active': this.state.open,
        })} style={{display:"block"}}>
        <div className="dropdown is-up" style={{display:"block"}}>
        <div className="dropdown-menu tut-menu" id="dropdown-tut1" role="menu">
            <div className="dropdown-content">
              <div className="dropdown-before"/>
              <div className="dropdown-item">
                  <p className="tooltip">{text}<Link onClick={() => this.props.nextTip(className)}><span><i className="fas fa-chevron-right" style={{ fontSize: "1.4rem", color: "#3081f9", fontWeight: "800", paddingLeft: "1rem" }}></i></span></Link></p>
              </div>
              <div className="dropdown-after"/>
            </div>
        </div>
    </div>
    </div>);
  }
}


TutorialTip.propTypes = {
  text: PropTypes.string,
};

export default TutorialTip;
