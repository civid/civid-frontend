import React, { Component } from 'react';


class Footer extends Component {

    render() {
        return (
            <section className="footer">
                <div className="container">
                    <div className="content">
                        <p>© 2019 Civid, inc.</p>
                    </div>
                </div>
            </section>
        )
    }
}

export default Footer;
