import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import logo from '../../assets/images/iconlogo.png';
import './landing.scss'

class LandingNavigation extends Component {
    constructor() {
        super();

        this.state = {
            isActive: false,
        }
    }

    toggleActive = () => {
        const { isActive } = this.state;
        this.setState({ isActive: !isActive });
    };

    renderUnauthenticatedMenu() {
        const { activeUser } = this.props;
        const { isActive } = this.state;
        if (activeUser) return null;
        return (
            <div id="navMenu" className={`navbar-menu ${isActive && 'is-active'}`}>
                <div className="navbar-end button-wrapper">

                    <Link className="navbar-item" to={`/about`}>
                        About Us
                  </Link>

                    <Link className="navbar-item" to="/login">
                        Sign In
                    </Link>

                    <Link className="button is-purple navbar-item log-sign" to={`/sign-up`}>
                        Sign Up
                    </Link>
                </div>
            </div>
        );
    }

    renderUserControls() {
        const { activeUser, users } = this.props;
        const { isActive } = this.state;
        if (!activeUser || !users[activeUser.id]) return null;

        return (
            <div id="navMenu" className={`navbar-menu ${isActive && 'is-active'}`}>
                <div className="navbar-end button-wrapper">

                    <Link className="navbar-item" to={`/about`}>
                        About Us
                  </Link>

                    <Link className="button is-purple navbar-item" to={`/`}>
                        Go to Site
                    </Link>
                </div>
            </div>
        );
    }

    render() {
        const { isActive } = this.state;
        return (
            <header className="landing-header">
                <div className="container">
                    <nav className="navbar">
                        <div className="navbar-brand">
                            <Link className="navbar-item" to={`/`}>
                                <img src={logo} alt="Civid Logo" height="34" width="127" />
                            </Link>
                            {/* todo remove this */}
                            {/* <div role="button" className={`navbar-burger ${isActive && 'is-active'}`} data-target="navMenu" aria-label="menu" ara-expanded="false" onClick={this.toggleActive}> */}
                            <div role="button" className={`navbar-burger ${isActive && 'is-active'}`} data-target="navMenu" aria-label="menu" onClick={this.toggleActive}>

                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>

                        {this.renderUnauthenticatedMenu()}
                        {this.renderUserControls()}

                    </nav>
                </div>
            </header>
        );
    }
}

export default connect(state => ({
    activeUser: state.activeUser,
    users: state.users.data
}))(LandingNavigation);
