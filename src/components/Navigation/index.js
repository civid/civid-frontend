import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router';
import { logout } from '../../actions/accounts/user/authentication';
import logo from '../../assets/images/iconlogo.png';
import { getProfileImage } from '../../utils/user.js';
import { getNotificationList } from '../../actions/notify/notification/list';


const NOTIFICATIONS_UPDATE_INTERVAL = 30 * 1000; // Seconds * msec per second

class Navigation extends Component {
    constructor() {
        super();

        this.state = {
            isActive: false,
        }
    }

    componentDidMount() {
        const { dispatch } = this.props;
        this.refreshNotifications();
    }

    componentWillUnmount() {
        if (this._updateTimeoutId) clearTimeout(this._updateTimeoutId);
    }

    refreshNotifications = () => {
        const { dispatch, activeUser } = this.props;
        if (activeUser) dispatch(getNotificationList());
        this._updateTimeoutId = setTimeout(this.refreshNotifications, NOTIFICATIONS_UPDATE_INTERVAL);
    };

    logoutUser = () => {
        const { dispatch } = this.props;
        dispatch(logout());
        browserHistory.push('/');
    };

    toggleActive = () => {
        const { isActive } = this.state;
        this.setState({ isActive: !isActive });
    };

    renderUnauthenticatedMenu() {
        const { activeUser } = this.props;
        const { isActive } = this.state;
        if (activeUser) return null;
        return (
            <div id="navMenu" className={`navbar-menu ${isActive && 'is-active'}`}>
                <div className="navbar-end button-wrapper">
                    <Link className="navbar-item" to="/login">
                        Sign In
                    </Link>

                    <Link className="button is-purple navbar-item log-sign" to={`/sign-up`}>
                        Sign Up
                    </Link>

                </div>
            </div>
        );
    }

    renderUserControls() {
        const { activeUser, notifications, users } = this.props;
        const { isActive } = this.state;
        if (!activeUser || !users[activeUser.id]) return null;
        const notificationsCount = Object.keys(notifications).length;

        return (
            <div id="navMenu" className={`navbar-menu ${isActive && 'is-active'}`}>
                <div className="navbar-end">
                    <Link className="navbar-item" to={`/`}>
                        Home
                    </Link>
                    <Link className="navbar-item" to={'/notifications'}>
                        Notifications
                    </Link>
                    {notificationsCount > 0 && <span className="notif-badge">{notificationsCount}</span>}
                    <div className="navbar-item has-dropdown is-hoverable">
                        <a className="navbar-link">
                            <img src={getProfileImage(activeUser.id, users)} className="navbar-profile"/>
                        </a>

                        <div className="navbar-dropdown is-right">
                            <Link className="navbar-item" to={`/profile/${activeUser.username}`}>
                                Profile
                            </Link>

                            <Link className="navbar-item" to="/account/basic-information">
                                Settings
                            </Link>
                            <div className="navbar-item navbar-footer">
                                <br />
                                <ul className="footer-links">
                                    <li>
                                        <Link to={`/about`}>About</Link>
                                        <span> · </span>
                                        <Link to={`/terms`}>Terms</Link>
                                        <span> · </span>
                                    </li>
                                    <li>
                                        <Link to={`/privacy`}>Privacy</Link>
                                        <span> · </span>
                                        <Link to={`/content-policy`}>Content Policy</Link>
                                        <span> · </span>
                                    </li>
                                    <li>
                                        <a href="" onClick={this.logoutUser}>Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { activeUser } = this.props;
        const { isActive } = this.state;

        return (
            <header className="app-header">
                <div className="navbar" role="navigation" aria-label="dropdown navigation">
                    <nav className="container">
                        <div className="navbar-brand">
                            <Link className="navbar-item" to={`/`}>
                                <img src={logo} alt="Civid Logo" height="34" width="127" />
                            </Link>
                            {/* too remove this */}
                            {/* <div role="button" className={`navbar-burger ${isActive && 'is-active'}`} data-target="navMenu" aria-label="menu" ara-expanded="false" onClick={this.toggleActive}> */}
                            <div role="button" className={`navbar-burger ${isActive && 'is-active'}`} data-target="navMenu" aria-label="menu" onClick={this.toggleActive}>

                                <span aria-hidden="true"></span>
                                <span aria-hidden="true"></span>
                                <span aria-hidden="true"></span>
                            </div>
                        </div>

                        {this.renderUnauthenticatedMenu()}
                        {this.renderUserControls()}
                    </nav>
                </div>
            </header>
        );
    }

}

export default connect(state => ({
    activeUser: state.activeUser,
    notifications: state.notifications.data,
    users: state.users.data
}))(Navigation);
