import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clock from "../../assets/images/clock.png"
import './clock.scss';


class Clock extends Component {

    render() {
        if (this.props.visible) {
            return (
                <div className="has-background-white">
                    <span className="user-text"><p>Time left to</p>
                        <p>Change mind:</p>
                    </span>
                    <span className="user-text"><img src={clock}></img></span>
                    <span className="user-text"><p className="clock-text">0</p></span>
                    <span className="user-text"><p className="clock-text">0</p></span>
                    <span className="separator">:</span>
                    <span className="user-text"><p className="clock-text">0</p></span>
                    <span className="user-text"><p className="clock-text">0</p></span>
                    <span className="separator">:</span>
                    <span className="user-text"><p className="clock-text">0</p></span>
                    <span className="user-text"><p className="clock-text">0</p></span>
                </div>
            )
        } else {
            return (null)
        }
    }
}

Clock.propTypes = {
    visible: PropTypes.bool
};

export default Clock;
