import React, { Component } from 'react';
import { connect } from 'react-redux';

import ClaimsCounter from '../../components/Claims-counter';
import ShareButton from '../../components/Share-button';
import SupportButton from '../../components/Support-button';
import { getProfileImage } from '../../utils/user';
import './post.scss'

class PostReplies extends Component {


    anonymous = (postData, activeUser, users) => {
        if (postData.anonymous) {
            return (
                <div className="host">
                    <span> Anonymous </span>
                </div>
            )
        } else {
            return (
                <div className="host">
                    <span> Todays host </span>
                    <img src={getProfileImage(activeUser.id, users)} className="user-img" height="50" width="50" />
                    <span>{activeUser.user_name}</span>
                </div>
            )
        }
    }
    getPostData = (data) => {
        if (data !== undefined && data.hasOwnProperty('data')) {
            const key = Object.keys(data.data)
            return data.data[key]
        }
        return
    }

    claim = (postData, claim, key) => {
        if (postData[claim] !== "") {

            return (
                <li className="panel" key={key}>
                    <div className="panel-heading" dangerouslySetInnerHTML={{ __html: postData[claim] }} />
                </li>
            )
        }

    }
    renderClaims = (postData) => {
        const claims = ['claim_0', 'claim_1', 'claim_2', 'claim_3'];

        return (
            <ol>
                {claims.map((claim, key) => (this.claim(postData, claim, key)))}
            </ol>
        )
    }

    renderSupport = (postData, activeUser) => {
        const claimsCounter = 3
        if (postData.user === activeUser.id) {
            return (
                <div>
                    <div className="colunm">
                        <p>You are the owner of this post</p>
                    </div>
                    <div className="column">

                        {<ShareButton />}

                        {<ClaimsCounter counter={claimsCounter} />}

                    </div>
                </div>
            )

        } else {
            return (
                <div className="column">
                    {<SupportButton click={this.supportOnClick} />}

                    {<ShareButton />}

                    {<ClaimsCounter counter={claimsCounter} />}

                </div>

            )
        }
    }

    render() {
        const { activeUser, post, users } = this.props;
        const postData = this.getPostData(post)

        if (postData) {

            return (
                <div className="postdetail-section">
                    <div className="columns">
                        <div className="title-post column" dangerouslySetInnerHTML={{ __html: postData.title }} />
                        <div className="column">
                            <Picture post={postData} />
                        </div>

                    </div>

                    {this.anonymous(postData, activeUser, users)}

                    <div className="suporting">
                        Suporting claims
                    </div>

                    <div className="column claims-column">
                        {this.renderClaims(postData)}
                    </div>

                    <div className="influence">
                        <Influence influenceNumber={activeUser.influence} />
                    </div>

                    {this.renderSupport(postData, activeUser)}

                    <div className="colunm">
                        {<Support visible={this.state.isSupportVisible} />}
                    </div>
                </div>
            )
        } else {
            return (null)
        };
    }

}


export default connect(state => ({
    post: state.posts,
    activeUser: state.activeUser,
    users: state.users.data
}))(Post);
