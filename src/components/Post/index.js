import React, { Component } from 'react';
import {Link, browserHistory} from 'react-router';
import { connect } from 'react-redux';
import striptags from "striptags";
import {
  FacebookShareButton,
  TwitterShareButton,
  FacebookIcon,
  TwitterIcon
} from 'react-share';
import classnames from 'classnames';
import settings from '../../config/settings';
import Support from '../../components/Support';
import Picture from '../Picture';
import Influence from '../Influence';
import {getPost} from '../../actions/posts/post/get';
import {awardTrophyForPost} from '../../actions/trophies/awarded-trophy/create';
import {editPostReply} from '../../actions/replies/post-reply/edit';
import OpinionInlineForm from '../../forms/OpinionInlineForm';
import ClaimsCounter from '../../components/Claims-counter';
import EllipsisMenu from '../../components/EllipsisMenu';
import TutorialTip from '../../components/TutorialTip';
import SupportButton from '../../components/Support-button';
import Anonymous from '../Anonymous'
import ChangeUserMind from '../../components/Change-user-mind';
import Claim from '../Claim/claim';
import ClaimPreview from '../Claim/claimPreview';
import Countdown from '../../components/Countdown';
import Accordion from '../Accordion/accordion';
import {POST_STATUSES} from '../../utils/normalize';
import PostReplyForm from '../../forms/PostReplyForm';
import PostRepliesList from '../../containers/PostDetail/postRepliesList';
import {findAwardedTrophiesForPost} from '../../containers/PostDetail/utils';
import {modalOpenClose, dimBg, addZindex} from '../../components/Tutorial/modal';
import './post.scss'

const SAPERE_AUDE_TROPHY_TITLE = 'Sapere Aude';
class Post extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSupportVisible: false,
            postReplytoDisplayFormFor: null,
            isExpanded: false,
            hasNotPosted: true,
            isCounterFocused: false,
            support: 0,
            step: "0",
        };
        this.myRef = React.createRef();
        this.myRef2 = React.createRef();
        this.myRef3 = React.createRef();
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    startTutorial = () => {
        this.setState({step: "1"});
        window.scrollTo({top: this.myRef.current.offsetTop, behavior: 'smooth'});
        dimBg();
        addZindex('tutorial1');
    }
    componentDidUpdate(prevProps) {
        const results = this.props.tutResults;
        if (results !== prevProps.tutResults) {
            this.startTutorial();
        }
    }

    nextTip = (tip) => {
        const {tutResults} = this.props;
        addZindex(tip);
        if (tip=="tutorial1") {
            if (tutResults == true) {
                this.setState({step: "2a"});
                addZindex('tutorial2a');
            }
            else {
                this.setState({step: "2b"});
                addZindex('tutorial2b');
            }

            setTimeout(window.scrollTo({top: this.myRef2.current.offsetTop, behavior: 'smooth'}),100);
        }
        else if (tip=="tutorial2a" || tip=="tutorial2b") {
            if (tutResults == true)
                this.setState({step: "3a"});
            else this.setState({step: "3b"});
            addZindex('tutorial3');

            setTimeout(window.scrollTo({top: this.myRef3.current.offsetTop, behavior: 'smooth'}),100);
        }
    }

    closeTip = (tip) => {
        this.setState({step: "0"});
        addZindex('tutorial3');
        setTimeout(window.scrollTo({top: 0, behavior: 'smooth'}),100);
        dimBg();
    }

    onReplyToPostClick = id => this.setState({postReplyToDisplayFormFor: id});

    onNestedReplySubmit = () => this.setState({postReplyToDisplayFormFor: null});

    onReplySubmit = () => this.setState({hasNotPosted: false});

    setDelete = (postReply) => {
        const {dispatch} = this.props;
            dispatch(editPostReply({
                ...postReply,
                title: '[deleted]',
                claim_0: '[deleted]',
                claim_1: '',
                claim_2: '',
                claim_3: ''
        }));
    };

    onAwardTrophy = userId => {
        const {post, trophies, dispatch} = this.props;
        const trophy = Object.values(trophies).find(t => t.title === SAPERE_AUDE_TROPHY_TITLE);
        dispatch(awardTrophyForPost({
            userId,
            postId: post.id,
            trophyId: trophy.id,
        }));
    };

    onTrophyAwardDeadlineReached = () => {
        const {dispatch, post} = this.props;
        dispatch(getPost(post.slug));
    };

    supportOnClick = (e) => {
        const {post, activeUser} = this.props;
        if (!activeUser) this.props.SignUpModalOpenClose();
        else if (post.user.id != activeUser.id)
            this.setState({ 'isSupportVisible': !this.state.isSupportVisible });
    }

    setSupport = (num) => {
        this.setState({support: num})
    }

    onExpand = () => this.setState({ isExpanded: true });

    renderClaim = (claim, index) => {
        const numberedClaim = claim.slice(0,3) + (index+1) + ". " + claim.slice(3);
        let strippedTags = (index+1) + ". " + striptags(claim).replace(/&nbsp;/gi,'');
        return (
            <div className="claim-box" key={index} label={strippedTags}>
                <div key={index} className="claim-contentopen"dangerouslySetInnerHTML={{ __html: numberedClaim }} />
            </div>
        )
    }

    renderClaims = () => {
        const {post} = this.props;
        const claim_id = ['claim_0', 'claim_1', 'claim_2', 'claim_3'];
        let claims = [];
        claim_id.map(claim => {
            if (post[claim] != "") {
                claims.push(post[claim]);
            }
        });

        return (
            <Accordion allowMultipleOpen>
                {claims.map((claim, index) => (this.renderClaim(claim, index)))}
            </Accordion>
        )
    }

    focusCounter = () => {
        const {activeUser, post} = this.props;
        const {isCounterFocused} = this.state;
        if (!activeUser) this.props.SignUpModalOpenClose();
        else if (post.user != activeUser.id) this.setState({isCounterFocused: !isCounterFocused});
        this.renderReplyForm(post);
    }

    renderReplyForm() {
        const {activeUser, post} = this.props;
        const {hasNotPosted, isCounterFocused} = this.state;
        let canReply = post.status !== POST_STATUSES.CLOSED;
        if (!activeUser) return null;
        if (post.user.id == activeUser.id) canReply = false;
        return (
            <div className="change-mind">
                {hasNotPosted ? <OpinionInlineForm
                    postId={post.id}
                    canReply={canReply}
                    submitFunc={this.onReplySubmit}
                    isCounterFocused={isCounterFocused} />
                    : <p>Counter-claim submitted!</p>}
            </div>
        );
    }

    getVoteTally(post) {
        const replyVotes = post.votes;
        if (!replyVotes) return 0;
        return Object.values(replyVotes)
            .reduce((acc, replyVote) => acc + replyVote.value, 0);
    }

   renderReplySection() {
        const {post, postReplies, activeUser, replyVotes, awardedTrophies, trophies, tutResults, users} = this.props;
        const {postReplyToDisplayFormFor, step} = this.state;
        const postReplyList = Object.values(postReplies).filter(postReply => !postReply.parent && postReply.post == post.id).sort((a, b) => {
                if (this.getVoteTally(a) == this.getVoteTally(b)) return 0;
                return this.getVoteTally(a) < this.getVoteTally(b) ? -1 : 1;
        });

        if (postReplyList.length === 0)
            return (
                <div className="empty-section" ref={this.myRef3} id="tutorial3">
                {!activeUser && step == "3a" && <TutorialTip nextTip={this.closeTip} results={tutResults} text={"This is the discussion, where users challenge the host’s opinion. Help the host create a meaningful discussion by debating and engaging with the challengers. "} className="tutorial3a"/>}
                 {!activeUser && step == "3b" && <TutorialTip nextTip={this.closeTip} results={tutResults} text={"This is the discussion, where users challenge the host’s opinion. Stand up for your fellow opinion holders, and engage with those who disagree."} className="tutorial3b"/>}

                    <p className="title is-4 is-spaced">
                        No Responses Yet
                    </p>
                    <p className="subtitle is-6">
                        Be the first to change their mind!
                    </p>
                </div>
            );

        const trophiesForThisPost = findAwardedTrophiesForPost(awardedTrophies, post);
        let canAwardTrophy = false;
        if (activeUser) { canAwardTrophy = activeUser.id === post.user && post.status === POST_STATUSES.AWARD_PENDING
            && trophiesForThisPost.length === 0;}
        const canReply = post.status !== POST_STATUSES.CLOSED;
        const userReceivedAward = trophiesForThisPost.length > 0 ? trophiesForThisPost[0].user : null;
        return (
            <div className="replies-content" ref={this.myRef3} id="tutorial3">
                 {!activeUser && step == "3a" && <TutorialTip nextTip={this.closeTip} results={tutResults} text={"This is the discussion, where users challenge the host’s opinion. Help the host create a meaningful discussion by debating and engaging with the challengers. "} className="tutorial3a"/>}
                 {!activeUser && step == "3b" && <TutorialTip nextTip={this.closeTip} results={tutResults} text={"This is the discussion, where users challenge the host’s opinion. Stand up for your fellow opinion holders, and engage with those who disagree."} className="tutorial3b"/>}
                <PostRepliesList
                  postsToDisplay={postReplyList}
                  postReplies={postReplies}
                  replyVotes={replyVotes}
                  postToDisplayFormFor={postReplyToDisplayFormFor}
                  onReplyToPostClick={this.onReplyToPostClick}
                  onSuccessfulReplySubmit={this.onNestedReplySubmit}
                  activeUser={activeUser}
                  renderPreviewsOnly
                  nestingLevel={0}
                  setDelete={this.setDelete}
                  onAwardTrophy={this.onAwardTrophy}
                  canAwardTrophy={canAwardTrophy}
                  userReceivedAward={userReceivedAward}
                  canReply={canReply}
                  trophies={trophies}
                  users={users}
                  claimUser={post.user}
                  SignUpModalOpenClose={this.props.SignUpModalOpenClose}
                />
            </div>
        );
    }

    render() {
        const { activeUser, post, postReplies, tutResults, users } = this.props;
        const { isExpanded, isSupportVisible, support, step } = this.state;
        const postReplyList = Object.values(postReplies).filter(postReply => !postReply.parent);
        const shareUrl = window.location.host + "/posts/" + post.id;
        const title = post.title;
        const iconStyle = {fill:"#ffffff"};
        var date = new Date(post.pub_date);
        if (date.getDay() == 5)
            date.setDate(date.getDate() + 3);
        else
            date.setDate(date.getDate() + 2);

        if (post) {
            return (
                <div>
                <div className="tip-bg" />
                <div className="content-box box-style column-content" id="tutorial1">
                    <div className="columns post-header">
                        <div className="column" ref={this.myRef}>
                            {!activeUser &&  step == "1" && <TutorialTip nextTip={this.nextTip} results={tutResults} text={"This is the host’s opinion - today’s discussion will revolve around it."} className="tutorial1"/>}
                            <div className="title-post">
                                {post.title}
                            </div>
                            <Anonymous post={post} users={users} />
                            <Countdown date={date.toString()} />

                        </div>
                        <div className="column">
                            <Picture post={post} />
                        </div>
                    </div>
                    <hr />

                    <div className="supporting">
                        Supporting Claims
                    </div>


                    <div className="claim-box body-text">
                    {this.renderClaims(post)}
                    </div>

                    <div className="claim-stats">
                        <nav className="level is-mobile">
                            <Influence post={post} postReplies={postReplies} support={support} users={users}/>
                        </nav>
                    </div>

                    <div className="action-group">
                    <nav className={classnames(
                                        'level', {
                                        'is-mobile': !isSupportVisible
                    })}>
                        <div className="level-item" ref={this.myRef2}>
                            {!activeUser && step == "2a" && <TutorialTip nextTip={this.nextTip} results={tutResults} text={"Strengthen their stance by supporting it, and raising its level of influence in the discussion."} className="tutorial2a"/>}
                            {!isSupportVisible && <SupportButton click={this.supportOnClick} />}
                            {isSupportVisible && <Support visible={isSupportVisible} click={this.supportOnClick} post={post} setSupport={this.setSupport} />}
                        </div>
                        <div className="level-item">
                        {!activeUser &&  step == "2b" && <TutorialTip nextTip={this.nextTip} results={tutResults} text={"The host will select one user who provided the most compelling argument in the discussion. Submit your opinion and take on the challenge!"} className="tutorial2b"/>}

                         <Link className="button is-white action-button level-item" onClick={this.focusCounter} id="tutorial2b">
                                <span className="icon">
                                    <i className="fas fa-comment" style={{fontSize: "1rem", marginTop: "-.3rem", color: "#3081f9" }}></i>
                                </span>
                                <span className="action-text">Respond</span>
                                </Link>
                        </div>

                        <div className="level-item">
                        <div className="share-button">
                            <FacebookShareButton
                              url={shareUrl}
                              quote={title}>
                              <FacebookIcon
                                size={35}
                                iconBgStyle={iconStyle}
                                logoFillColor="#95989A"
                                />
                            </FacebookShareButton>
                          </div>
                          <div className="share-button">
                            <TwitterShareButton
                              url={shareUrl}
                              title={title}>
                              <TwitterIcon
                                size={35}
                                iconBgStyle={iconStyle}
                                logoFillColor="#95989A"
                                />
                            </TwitterShareButton>
                          </div>

                            <EllipsisMenu post={post} SignUpModalOpenClose={this.props.SignUpModalOpenClose} activeUser={activeUser}/>
                        </div>
                    </nav>
                    </div>
                    {this.renderReplyForm()}
                </div>

                <div className="is-size-5" style={{ fontWeight:"600", paddingTop: "4rem", paddingBottom: "1rem" }}><span style={{borderBottom: "5px solid #3081f9", paddingBottom: ".4rem" }}>{post.direct_replies.length}
                    {post.direct_replies.length == 1 ? ` Response` : ` Responses`}<hr style={{ margin: ".5rem 0", backgroundColor: "#95989a" }}/></span>

                </div>

                    {this.renderReplySection()}
                </div>
            )
        } else {
            return (null)
        };
    }

}


export default connect(state => ({
    postReplies: state.postReplies.data,
    replyVotes: state.replyVotes.data,
    activeUser: state.activeUser,
    trophies: state.trophies.data,
    awardedTrophies: state.awardedTrophies.data,
    users: state.users.data,
}))(Post);
