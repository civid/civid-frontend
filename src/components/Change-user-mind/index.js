import React, { Component } from 'react';
import PropTypes from 'prop-types';
import avatar from "../../assets/images/avatar.png"
import './change-user-mind.scss'
import { PostReplyForm } from '../../forms/PostReplyFormHome'

class ChangeUserMind extends Component {
    constructor(props) {
        super(props)
        this.state = {
            replyForm: false
        }
    }
    render() {
        return (
            <article className="change-mind media centered-content">
                        <figure className="media-left">
                            <img src={avatar} className="user-img" height="25" width="25"/>
                        </figure>
                        <div className="media-content">
                            <input className="input is-medium" value="Think you can change their mind?" readOnly/>
                        </div>
                    </article>

        )
    }
}

export default ChangeUserMind;
