import React, { Component } from 'react';
import Link from 'react-router';
import PropTypes from 'prop-types';
import influence from "../../assets/images/influence.png"
import moment from 'moment';
import {getProfileImage} from '../../utils/user';
import './influence.scss';

class Influence extends Component {

    getVoteTally() {
        const {post} = this.props;
        const replyVotes = post.votes;
        if (!replyVotes) return 0;
        return Object.values(replyVotes)
            .reduce((acc, replyVote) => acc + replyVote.value, 0);
    }

    renderRecentVotes() {
        const { post, users } = this.props;
        const votes = post.votes.sort((a, b) => moment(b.created_date) - moment(a.created_date)).slice(0,5);
        {if (votes.length > 0) return (
            <span className="recent-prof">
             {votes.map((vote) => {
                return (
                <img key={vote.user.id} className="user-icon" src={getProfileImage(vote.user.id, users)} height="25" width="25" />)
            })}
            </span>)
        }
    }

    renderRecentClaims() {
        const { post, postReplies, users } = this.props;
        const claims = Object.values(postReplies).filter(postReply => postReply.post === post.id && !postReply.parent).sort((a, b) => moment(b.created_date) - moment(a.created_date)).slice(0,5);

        {if (claims.length > 0) return (
            <span className="recent-prof" key={post.id}>
             {claims.map((claim) => {
                return (
                <img key={claim.user} className="user-icon" src={getProfileImage(claim.user, users)} height="25" width="25" />)
            })}
            </span>)
        }
    }

    render() {
        const { post, postReplies, support } = this.props;
        if (!post) return null;
        const votes = this.getVoteTally() + support;
        let style = {fontWeight:"600"};
        if (support != 0) style = {fontWeight:"600",color:"#3081f9"};

        return (
            <div className="level-left">
                <div className="level-item">
                    <span className="subtitle is-6" style={style}>{votes} </span> <span className="influence-text">  influence</span>

                    {this.renderRecentVotes()}
                </div>


                {post.direct_replies != null &&
                    <div className="level-item">
                        <span className="subtitle is-6" style={{ fontWeight:"600", paddingLeft: "2rem" }}>{post.direct_replies.length} </span>
                        <span className="influence-text">
                        {post.direct_replies.length == 1 ? `response` : `responses`}
                        </span>
                        {this.renderRecentClaims()}
                    </div>}
            </div>
        );
    }
}

Influence.propTypes = {
    post: PropTypes.object,
    postReplies: PropTypes.object,
    support: PropTypes.number,
    users: PropTypes.object.isRequired
};

export default Influence;
