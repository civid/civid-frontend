// deprecate remove

import React, { Component } from 'react';

class FacebookLogButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            message: ""
        };
    }

    componentDidMount() {
        const { fb } = this.props;
        fb.Event.subscribe('auth.logout', this.onLogout);

        fb.Event.subscribe('auth.statusChange', this.onStatusChange);
    }

    onStatusChange = (response) => {
        const { fb } = this.props;
        if (response.id !== undefined) {
            fb.api('/me?fields=name,email', function (response) {
                self.setState({
                    message: "Welcome " + response.name
                });
            })
        }
    }

    onLogout = (response) => {
        this.setState({
            message: ""
        });
    }


    render() {
        return (
            <div>
                <div
                    className="fb-login-button"
                    data-max-rows="1"
                    data-size="xlarge"
                    data-show-faces="false"
                    data-auto-logout-link="true"
                >
                </div>
                <div>{this.state.message}</div>
            </div>
        );
    }
};


export default FacebookLogButton