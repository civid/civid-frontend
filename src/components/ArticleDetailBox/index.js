import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';
import moment from 'moment';
import {getArticleImage} from '../../utils/general';
import EllipsisMenu from '../EllipsisMenu';
import './ArticleDetailBox.scss';


class ArticleDetailBox extends Component {


    renderOpinionCount() {
        const {article} = this.props;
        let opinions = "opinions";
        if (article.opinions_count === 1) opinions = "opinion";
        return (
            <div className="num-opinions">
                <nav className="level is-mobile">
                    <div className="level-item opinions-item">
                        <TimeAgo date={article.date} minPeriod="60"/>
                    </div>
                    <div className="opinions-item">
                        {' · '}
                    </div>

                    <div className="level-item opinions-item">
                        <span className="icon">
                            <i className="far fa-comment-alt">
                            </i>
                        </span>
                        {article.opinions_count} {opinions}
                    </div>
                    <div className="level-item opinions-item">
                        <EllipsisMenu />
                    </div>
                </nav>
            </div>
        );
    }

    render() {
        const {article, articles} = this.props;
        const now = moment();
        const postDate = moment(article.date);
        const daysAgo = now.diff(postDate, 'days');
        return (
            <div className="ArticleDetailBox">
                <div className="article-box box-style">
                    <div className="columns">
                        <div className="column is-3">
                            <img src={getArticleImage(article)} className="article-img"/>
                        </div>
                        <div className="column is-9">
                            {daysAgo < 2 && <div className="article-details">
                                <span className="tag is-danger breaking-tag"> BREAKING </span>
                            </div>}
                            <h4 className="title is-4 article-title">{article.title}</h4>

                            <div className="post-short body-text">
                                {article.summary}
                            </div>
                            {this.renderOpinionCount()}
                        </div>

                    </div>
                </div>
            </div>
        );
    }

}

ArticleDetailBox.propTypes = {
    article: PropTypes.object.isRequired,
};

export default connect(state => ({
    articles: state.articles.data,
    activeUser: state.activeUser,
    postReplies: state.postReplies.data,
    users: state.users.data
}))(ArticleDetailBox);
