import React, { Component } from 'react';
import { getProfileImage } from '../../utils/user';
import PropTypes from 'prop-types';
import './anonymous.scss'
import {Link} from 'react-router';


class Anonymous extends Component {

    render() {
        const { post, users } = this.props;
        if (post.anonymous) {
            return (
                <div className="host">
                    <span> Anonymous </span>
                </div>
            )
        } else {
            return (
                <article className="media centered-content">
                    <figure className="media-left">
                        <Link className="user-name" to={`/profile/${post.username}`}>
                            <img src={getProfileImage(post.user, users)} className="user-img size-40" />
                        </Link>
                    </figure>
                    <div className="media-content">
                        <Link className="user-name" to={`/profile/${post.username}`}>
                            {post.username}
                        </Link>
                    </div>
                </article>
            )
        }
    }
}

Anonymous.propTypes = {
    post: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired
};

export default Anonymous;
