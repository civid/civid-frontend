import React, { Component } from 'react';
import { connect } from 'react-redux';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { GoogleLogin } from 'react-google-login';
import TwitterLogin from 'react-twitter-auth';
import { socialLogin } from '../../actions/accounts/user/social-login';
import {GoogleIcon, FacebookIcon} from 'react-share';


const style = {
    marginTop: "20px",
    color: "#fff",
}

const googleStyle = {
    display: 'inline-block',
    color: '#fff',
    marginTop: "1rem",

}

class SocialLogin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            message: ''
        }
    }

    responseFacebook = (response) => {
        const { dispatch } = this.props;
        dispatch(socialLogin(response, 'facebook'))

    }
    responseTwitter = (response) => {
        const { dispatch } = this.props;
        dispatch(socialLogin(response, 'twitter'))

    }
    responseGoogle = (response) => {
        const { dispatch } = this.props;
        dispatch(socialLogin(response, 'google-oauth2'))
//        .then(() => {
 //           browserHistory.push('/home');
 //       };
    }

    responseBadGoogle = (response) => {
        console.log(response);
    }

    render() {
        return (
            <div className="content-box">
                <FacebookLogin
                    appId="726367227716909"
                    autoLoad={false}
                    fields="name,email,picture"
                    callback={this.responseFacebook}
                    render={renderProps => (
                        <button
                            style={style}
                            className="facebook-button button is-medium is-fullwidth spacer"
                            onClick={renderProps.onClick}>Continue with Facebook</button>
                    )}
                />

                <GoogleLogin
                render={renderProps => (
                    <button
                        style={googleStyle}
                        className="google-button button is-medium is-fullwidth spacer"
                        onClick={renderProps.onClick}>Continue with Google</button>
                )}
                    clientId="89515965122-atteoonlchkkb0a30ukpml00ffr5patc.apps.googleusercontent.com"
                    onSuccess={this.responseGoogle}
                    onFailure={this.responseBadGoogle}
                />

                {/*<TwitterLogin loginUrl="http://localhost:8000/social-login/twitter/"
                   onFailure={this.responseBadTwitter}
                   onSuccess={this.resonseTwitter}
                   requestTokenUrl="http://localhost:8000/social-login/twitter/"/>*/}

                </div>

        );
    }

}


export default connect(state => ({
    activeUser: state.activeUser
}))(SocialLogin);
