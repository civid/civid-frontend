import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';

class stepThree extends Component {

    render() {
        return (
            <div className="modal is-active">
                <div className="modal-bg"/>
                <div className="modal-card">
                    <header className="modal-card-head" style={{display: "block"}}>
                    </header>
                    <section className="modal-card-body">
                        <div className="post-content has-text-centered">

                            <div className="tut-line">Today’s host has posted his opinion with a willingness to change their mind and hear the other side. Will you simply support the discussion leaders or take on the challenge of presenting the host with the bigger picture?</div>

                            <div className="buttons" style={{display: "block"}}>
                                <button className="button" onClick={() => this.props.nextStep('three', true)}>Support</button>
                                <button className="button" onClick={() => this.props.nextStep('three', false)}>Challenge</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}

export default stepThree;
