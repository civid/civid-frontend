export function modalOpenClose(classname) {
    let tip = document.querySelector("."+classname);
    tip.classList.toggle('is-active');
}

export function dimBg() {
    let bg = document.querySelector(".tip-bg");
    bg.classList.toggle('is-visible');
}

export function addZindex(classname) {
    let zindex = document.getElementById(classname);
    zindex.classList.toggle('zindex');
}
