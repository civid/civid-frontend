import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';

class StepTwo extends Component {

    render() {
        return (
            <div className="modal is-active">
                <div className="modal-bg"/>
                <div className="modal-card">
                    <header className="modal-card-head" style={{display: "block"}}>
                    </header>
                    <section className="modal-card-body">
                        <div className="post-content has-text-centered">

                            <div className="tut-line">Today’s host has posted their opinion with a willingness to change their mind and participate in discussion. Will you simply help the host gain a stronger stance, or will you actively defend their opinion?</div>

                            <div className="buttons" style={{display: "block"}}>
                                <button className="button" onClick={() => this.props.nextStep('two', true)}>Help</button>
                                <button className="button" onClick={() => this.props.nextStep('two', false)}>Actively Defend</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}

export default StepTwo;
