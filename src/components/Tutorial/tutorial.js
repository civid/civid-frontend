import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router";
import {modalOpenClose} from './modal';
import './Tutorial.scss';

const TUTORIAL_SHOWN_KEY = 'tutorialAlreadyShown';

class Tutorial extends Component {

    modalClose = (input) => {
        this.props.setResults(input);
        modalOpenClose("modal");
    }

    static tutorialAlreadyShown() {
        return !!localStorage.getItem(TUTORIAL_SHOWN_KEY);
    }

    componentWillUnmount() {
        localStorage.setItem(TUTORIAL_SHOWN_KEY, true.toString());
    }


    render() {
        const {title, user, userimg, postimg} = this.props;
        const img = postimg ? postimg : 'https://i.imgur.com/uuykYlB.png'
        return (
            <div className="modal is-active">
                <div className="tutorial-bg"/>
                <div className="modal-card" style={{width:"600px"}}>
                    <header className="modal-card-head">
                        <p className="modal-card-title has-text-centered" style={{fontSize:"1rem"}}>Welcome to Civid!</p>
                    <button className="delete" onClick={() => modalOpenClose("modal")} aria-label="close" />
                    </header>
                    <section className="modal-card-body">
                        <div className="post-content">
                            <div className="columns">
                                <div className="column">
                                    <div className="tut-line subtitle is-4" style={{marginBottom:"1rem"}}><strong>"{title}"</strong></div>

                                    – <img src={userimg} className="user-img" style={{ margin: "0 .7rem", height:"25px", width:"25px" }}/>

                                        {user}

                                </div>

                                <div className="column">
                                    <img style={{maxHeight:"150px"}} src={img} />
                                </div>
                            </div>
                                <div className="has-text-centered">
                                    <div className="tut-line subtitle is-5"><strong>Do you agree with this opinion?</strong></div>

                                    <div className="buttons" style={{display: "block"}}>
                                        <button className="button" onClick={() => this.modalClose(true)}>Agree</button>
                                        <button className="button" onClick={() => this.modalClose(false)}>Disagree</button>
                                    </div>
                                </div>
                        </div>
                    </section>
                </div>
            </div>
        );

    }
}

Tutorial.propTypes = {
    title: PropTypes.string,
    user: PropTypes.string,
    userimg: PropTypes.string,
    postimg: PropTypes.string
}

export default Tutorial;
