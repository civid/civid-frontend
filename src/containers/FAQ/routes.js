import React from 'react';
import {Route} from 'react-router';
import Authenticate from '../HOC/Authenticate';
import AuthenticationRequired from '../HOC/AuthenticationRequired';
import FAQ from "./";



export default (
    <Route path="/faq" component={Authenticate(FAQ)}/>
);
