import React, {Component} from 'react';
import logo from '../../assets/images/iconlogo.png';
import Navigation from '../../components/Navigation';
import './FAQ.scss';

class FAQ extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {

    return (
      <div className="Faq">
        <div className="app-container">
          <Navigation />


          <section className="section">
            <div className="container">
              <h4 className="title is-4 faq-title">FAQ</h4>

              <ul className="faq-links">
                <li>
                  <a className="faq-link" href="#what-is-civid">
                    What is Civid?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#what-is-a-discussion">
                    What is a discussion?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#how-do-i-start-a-discussion">
                    How do I start a discussion?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#who-do-i-award-the-sapere-aude-to">
                    Who do I award the "Sapere Aude" to?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#how-do-i-post-in-an-existing-discussion">
                    How do I post in an existing discussion?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#how-do-i-win-a-discussion">
                    How do I win a discussion?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#does-every-discussion-need-to-relate-to-a-news-article">
                    Does every discussion need to relate to a news article?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#what-does-it-mean-to-follow-someone-at-civid">
                    What does it mean to follow someone at Civid?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#what-does-winning-a-sapere-aude-mean">
                    What does winning a "Sapere Aude" mean?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#what-are-influence-points">
                    What are influence points?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#how-do-i-earn-influence-points">
                    How do I earn influence points?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#what-do-i-do-with-influence-points">
                    What do I do with influence points?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#how-do-i-report-someone">
                    How do I report someone?
                  </a>
                </li>
                <li>
                  <a className="faq-link" href="#how-are-accounts-suspended">
                    How are accounts suspended?
                  </a>
                </li>
              </ul>

              <section className="faq-section">
              <div className="faq-item" id="what-is-civid">
                <strong>
                What is Civid?
                </strong>
                <p>
               Civid is a community for people who want to help others attain a better understanding of the world, by challenging their current understanding about current events.
                Civid’s community helps others grasp a deeper understanding of events shaping the world through the different perspectives of our global community.
                </p>
              </div>
              <div className="faq-item" id="what-is-a-discussion">
                <strong>
                  What is a discussion?
                </strong>
                <p>
               Discussions are Civid’s current main feature, and exist to help individuals attain a clearer understanding of the ‘big picture’.
              The discussion revolves around a user’s current stance, understanding or opinion of a recent news, while other users are invited to challenge his position, point our information that he has missed, and sometimes even help him change his mind about a certain issue.
              At the end of 24 hours, the discussion creator will have a 24-hour window to award the “Sapere Aude” trophy to the user who helped him understand the issue better, shed light of news facts, or change his mind about his stance.
              We believe that we are not intelligent for knowing everything without questioning, but rather because we question everything we know.
                </p>
              </div>
              <div className="faq-item"id="how-do-i-start-a-discussion">
                <strong>
                  How do I start a discussion?
                </strong>
                <p>
                Initiate a discussion on the top of the home feed (PICTURE)
                Post your opinion or understanding of a certain issue, be open to listening and hearing out the other side, be faq-item with the fact that you might be wrong, and gain knowledge through others wisdom, experience and perspectives.
                </p>
              </div>
              <div className="faq-item" id="who-do-i-award-the-sapere-aude-to">
                <strong>
                  Who do I award the "Sapere Aude" to?
                </strong>
                <p>
                The "Sapere Aude" is awarded by the discussion initiator to the user who either changed your mind, helped you gain a new perspective, helped you better understand an issue or even a user that taught you something new.
The title can be awarded after 24 hours from the initiation of the discussion, and can only be awarded once to single user. Choose wisely!
                </p>
              </div>
              <div className="faq-item" id="how-do-i-post-in-an-existing-discussion">
                <strong>
                  How do I post in an existing discussion?
                </strong>
                <p>
                Anyone can post in any existing discussion. All you need to do is scroll through Home feed, find a discussion that sparks your interest and open it by clicking on it. You will be redirected to the discussion and simply click on ‘post in discussion’ to help the initiator see the issue from a new perspective.
                </p>
              </div>
              <div className="faq-item" id="how-do-i-win-a-discussion">
                <strong>
                  How do I win a discussion?
                </strong>
                <p>
                A discussion is over once a discussion initiator awards the ‘Sapere Aude’ trophy. The trophy is awarded to the user who made the most compelling counter argument, helped the initiator see a new side to his opinion, or changed his mind.
                </p>
              </div>
              <div className="faq-item" id="does-every-discussion-need-to-relate-to-a-news-article">
                <strong>
                  Does every discussion need to relate to a news article?
                </strong>
                <p>
                Yes. At Civid, we only discuss the latest and hottest topics, and help other gain a better understanding of them.
                </p>
              </div>
              <div className="faq-item" id="what-does-it-mean-to-follow-someone-at-civid">
                <strong>
                  What does it mean to follow someone at Civid?
                </strong>
                <p>
                Following a writer on Civid means that you are interested in seeing his future work in your home feed.
In the window on the left side of the home feed, you have an option to filter the discussion based on three categories. Choosing the ‘following’ filter will mean that only people whom you follow will appear in your feed.
                </p>
              </div>
              <div className="faq-item" id="what-does-winning-a-sapere-aude-mean">
                <strong>
                  What does winning a "Sapere Aude" mean?
                </strong>
                <p>
                It means that a discussion initiator decided that your post helped him better understand an issue, see a relevant and different perspective, or changed his mind completely about a certain topic.
The ‘Sapere Aude’ will both grant you the title, and twenty-five influence points.
                </p>
              </div>
              <div className="faq-item" id="what-are-influence-points">
                <strong>
                  What are influence points?
                </strong>
                <p>
                Influence points determine how influential a writer is within the Civid community. Users can receive Influence points by gaining up- votes, creating relevant discussions, and winning a ‘Sapere Aude’ trophy.
Influence points are used to differentiate between writers who participate and influence the discussion, as opposed to users who read, learn and gain a new perspective from existing discussions.
                </p>
              </div>
              <div className="faq-item" id="how-do-i-earn-influence-points">
                <strong>
                  How do I earn influence points?
                </strong>
                <p>
                Influence points can be earned in several different ways:
                1. Up- votes: one up- vote is equal to one influence point.
                2. Creating a discussion: By challenging your understanding, and awarding a ‘Sapere Aude’ to the writer who helped you better understand an issue, you are awarded 15 influence points.
                3. Winning a ‘Sapere Aude’: The title will translate into 25 influence points.
                </p>
              </div>
              <div className="faq-item" id="what-do-i-do-with-influence-points">
                <strong>
                  What do I do with influence points?
                </strong>
                <p>
                Influence points have two major roles in the Civid community:
                1. Users who gain the most influence points every month will be presented on the ‘Influencers of the Month’ board on the home feed, for all to see. This is dedicated to the Civid members who have impacted, influenced and lead the discussions.
                2. At Civid, we award writers who help fight misinformation and help users gain a better understanding of world events, and so, users who add meaningful faq-item, value and insight to a conversation, will be rewarded for their relevance, leadership and humanity.
                </p>
              </div>
              <div className="faq-item" id="how-do-i-report-someone">
                <strong>
                  How do I report someone?
                </strong>
                <p>
                Reports can be filed for any post that breaks the Civid policy. The report link can located in the ellipses menu to the right of every post. Alternatively, contact us directly through email or Slack.
                </p>
              </div>
              <div className="faq-item"  id="how-are-accounts-suspended">
                <strong>
                  How are accounts suspended?
                </strong>
                <p>
                Accounts on Civid are suspended when users break the Civid policy several times. Users who will use sexual, racist, offensive language will receive a warning. After several warnings, based on the users reported activity, the user's account will be suspended.

                </p>
              </div>
              </section>
            </div>
          </section>


    <section className="footer">
      <div className="container">
        <div className="content">
          <p>
            © 2018 Civid, inc.
          </p>
        </div>
      </div>
    </section>
  </div>
  </div>
        );
    }

}

export default FAQ;
