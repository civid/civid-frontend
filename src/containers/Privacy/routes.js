import React from 'react';
import {Route} from 'react-router';
import Authenticate from '../HOC/Authenticate';
import AuthenticationRequired from '../HOC/AuthenticationRequired';
import Privacy from "./";



export default (
    <Route path="/privacy" component={Authenticate(Privacy)}/>
);
