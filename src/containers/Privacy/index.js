import React, {Component} from 'react';
import {Link} from 'react-router';
import logo from '../../assets/images/iconlogo.png';
import Navigation from '../../components/Navigation';
import './privacy.scss';

class Privacy extends Component {

  render() {

    return (
      <div className="Privacy">
        <div className="app-container">
          <Navigation />


          <section className="section">
            <div className="container body-font">
              <h4 className="title is-4">Privacy Policy</h4>
                <ol>
                <div className="content">
                    <li><h6 className="subtitle is-6">
                    Who We Are:
                    </h6>
                    <p>
                      <strong>Civid</strong> (hereafter “Civid”, “we”, “our” or “us”) is a social community platform that promotes discussion on topical world events and encourages the exchange of knowledge, experience and opinions. Our aim is to build an informed and cooperative intellectual community where members are willing to question as well and expand their perspectives and understanding of issues and events shaping our world. To facilitate this goal, we will, from time to time, have to collect general and personal information necessary to build and operate our website and facilitate the discussions. We have therefore created this Privacy Policy to explain how and why we “process” (which in effect includes, collection, use, retention, transmission, sharing and disclosure) of your information in connection with the use of this website and its blogs, forums, subdomains or mobile application platform (collectively the “Civid Platform” or “Platform”). We process your information in accordance with applicable data protection laws. By using this Platform, you are deemed to agree to our <Link to={`/terms`}>Terms of Use</Link>, this Policy and our use of Cookies. If you do not agree with this Policy and the way we deal with personal information in connection with the use of the Civid Platform, please do not use the Platform.
                    </p>
                    <p>
                      <strong>Civid</strong> is a company/partnership/sole proprietorship registered in Delaware.
                    </p>
                    <p>
                      Our register address is at 400W 113th street, 10025 and our Company/registration number is 35- 2614560.  Civid decides the ways and purposes of collecting information in relation to this Platform and we therefore act as Data Controller when handling your information. Civid’s Data Protection officer is Austin McGuire and can be reached at <a href="mailto:austin.mcguire@civid.com">Austin.McGuire@civid.com</a>.
                    </p>
                  </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">Information Collection:</h6>
                  <p>
                    Civid collects certain information necessary to operate our platform. This information may be provide by you or obtained by from third parties or using other data gathering technologies.
                  </p>
                  <p>
                    <strong><i>Information you provide: </i></strong>
                      To use the Civid Platform and be able to start and participate in discussions we will require you to register and create a user account with us. To create your account we may collect the following information:
                  </p><ul>
                    <li>Name or username</li>
                    <li>Email address</li>
                    <li>Country or city of residence</li>
                  </ul>

                  <p>
                    We may also provide you with the option to voluntarily supply additional profile information including:
                    </p><ul>
                      <li>Photo</li>
                      <li>Certification</li>
                      <li>Occupation</li>
                    </ul>
                  <p>
                    <strong><i>Information on website usage: </i></strong>
                      When you use the Civid Platform, we automatically receive certain information which includes:
                  </p><ul>
                    <li>Information about the device used to access the Platform (i.e., type and operating system of the device)</li>
                    <li>The internet address of your internet service provider</li>
                    <li>The pages viewed on the Platform</li>
                    <li>Date and time of access.</li>
                  </ul>
                  <p>
                    <strong><i>Information on interests and favorites: </i></strong>
                      We may collect information about your interests and favorites including news, topics, or articles you have saved in your profile under your user account.
                  </p>

                </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">Cookies and similar technologies:</h6>
                  <p>
                    Our Platform uses cookies so as to provide users with a custom and useful experience. A cookie is a minute text file downloaded to a user’s device by a web server when a user visits a site. This file contains information about the site of origin and does not contain any malicious or invasive content. We use cookies to assist in authenticating users and remember user actions on the site. Cookies help keep you logged in while you navigate the site and interact with several discussion topics.  We also use cookie to remember your preferences and provide a personalized feed of trending and live news basing on your interests.  Cookie also help to remember your browser for future visits including any language or display preferences you have selected for the site.
                  </p>

                  <p>
                    <strong><i>Google Analytics website tracking:</i></strong> Civid Platform uses Google Analytics, a third party web analytics tool developed by Google, to gain insight into how visitors use the website. Google Analytics may use third patty scripts and cookies for the purposes of identifying users and session visits, measure website loading speed as well traffic sources and navigation on the site including for example which search engines are used to locate content on the Civid Platform. Data collected by analytics cookies in connection with the use of the Platform (including your IP address) is transmitted to and stored in Google servers in the United States of America. We use this information to evaluate website statistics and to help improve our website’s navigation and structure. Google may also share this information with third parties if required according to law if any third parties process information on Google’s behalf. To protect the integrity of your information, we will use Google Analytics’ IP-address anonymization functionality during the information collection process.  If you do not want your visit to Civid Platform to be tracked by Google Analytics statistics, you can use the Google Analytics opt-out browser add-on. Civid does not merge personal data with analytics or cookie data about site visitors’ online activities.
                  </p>
                </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">How we use information:</h6>
                  <p>
                    We use information we collect about you for the following main reasons:
                    </p><ul>
                      <li>To provide access and use of our services (including improvement and personalization).</li>
                      <li>To ensure that our Platform and services is used in accordance with our Terms of Use, and to comply with the laws.</li>
                      <li>To maintain security and integrity of our Platform</li>
                      <li>Communication with our members which include both administrative communication (such as feedback or member support services) and marketing communication directed to member in accordance with member consent or our Terms of Use.</li>
                      <li>To market our services and deliver ads on our website or other advertising networks.</li>
                    </ul>
                </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">Legal basis for processing of information:</h6>
                  <p>
                  In compliance of the GDPR, we use the following lawful justifications when processing your personal data:
                    </p><ul>
                      <li>Performance of a contract between Civid and you</li>
                      <li>To provide our services, products or information in accordance with our Terms of Use.</li>
                      <li>To comply with any legal obligation</li>
                      <li>Pursuant to your consent for processing of information for a particular purpose</li>
                      <li>As part of our legitimate interest to develop and improve our Platform as well as send marketing communication that may be reasonably expected by on the basis of your membership at Civid Platform.</li>
                    </ul>
                </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">Sharing and disclosure:</h6>
                  <p>
                    Civid does not share or disclose information about you unless you consent or there is a legitimate reason to do. It may in certain circumstances be necessary to share your information about you with third parties in our legitimate interest or with other parties which provide services on our behalf so that we can deliver services you have requested. In cases where we share personal information about you we ensure that these third parties comply with our data protection requirements described in this Policy. Our third party service providers shall not have the right to use the information they receive from us for any other reason than what is provided in this Policy. We may share information with third parties we have contracted to provide customer support, marketing, web analytics as well as parties we have contracted to maintain and protect our Platform or services and who may need access to personal information so as to provide such support services. In other cases we may have to share or disclose your information when is it necessary for:

                      </p><ul>
                        <li>Comply with the law or judicial process or share information to law enforcement</li>
                        <li>Protect our members</li>
                        <li>Manage and provide security of our Platform</li>
                        <li>Protect privacy  and proprietary rights of others </li>
                        <li>To enforce our Terms of Use.</li>
                      </ul>
                </li>
                </div>

                <div className="content">
                  <li>
                  <h6 className="subtitle is-6">Information retention:</h6>
                  <p>
                    Civid may retain your information if it necessary for provision of services and fulfil your requests, in accordance with the law, or as otherwise necessary for compliance of any legal obligation. Unless retention is required for the foregoing reasons, we will delete all data which is no longer need in relation to the purposes for which it was obtained or if the processing was based on consent and that consent has been withdrawn or where processing has been lawfully objected to. Civid has also adopted a strong Data Retention Policy to fulfil our relevant data protection obligations.
                  </p>
                </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">Information security:</h6>
                  <p>
                    Civid is aware of the ubiquitous risks and threats to security of personal information including in particular the threat and risk of accidental loss and unauthorised access, acquisition, use, alteration, disclosure or destruction of personal information. To protect personal information against these threats and risks, Civid uses Secure Socket Layer (SSL) encryption to ensure integrity of personal data. As explained in this Policy, we will undertake anonymization and pseudomysation of online identifiers as well as control how employees access personal data.
                  </p>
                </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">Individual rights:</h6>
                  <p>
                    Civid believes it is important for you to know your rights in respect of personal information we hold about you. In accordance with the GDPR, EU residents have the following rights:
                      </p><ul>
                        <li>Right to access information we hold about you and obtain free copy of such information free of charge</li>
                        <li>Right to withdraw consent where collection and use of information is based on consent</li>
                        <li>Right to request correction of inaccurate or incomplete data</li>
                        <li>Right to request of erasure of personal information under certain circumstances</li>
                        <li>Right to restrict processing of your personal information including for example where you think we do not have lawful justification to process it.</li>
                        <li>Right to data portability</li>
                        <li>Right to object to direct marketing</li>
                      </ul>
                </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">Children’s Online Privacy Protection:</h6>
                  <p>
                    Civid does not target its services to children under the age of 13. Civid understands however the broad application of the children’s online privacy protection obligations under US privacy law as well as the EU GDPR. Civid does not collect information from children under the age of 13 on purpose. Persons under the age of 13 must not provide personal information to us or use the Civid Platform without guidance of a parent or guardian.
                  </p>
                </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">Your California Privacy Rights</h6>
                  <p>
                    California privacy law require website operators conducting business in California to provide California residents with either information on third parties with whom Civid has revealed personal information for marketing purposes with regard to the use of the site or, where a website has a privacy policy, the choice to opt-out of processing of personal information for third party marketing purposes. As long as Civid provides a privacy policy, we are not obligated to disclose third party marketers with whom we share information but we will provide the mechanism to opt-out/opt-in for use of information for marketing purposes. If you do not agree to disclosure of your information to third parties for their marketing purposes, please notify us by email at <a href="mailto:privacy@civid.com">privacy@civid.com</a> and we will stop doing so.
                  </p>
                </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">Privacy Policy Changes:</h6>
                  <p>
                    If Civid decides to change its privacy policy, Civid will notify you of those changes on our Platform or through member’s email addresses so that our members are made aware of how and why Civid collects information and in which circumstances, if any, it may share or disclose such information.
                  </p>
                </li>
                </div>

                <div className="content">
                <li>
                  <h6 className="subtitle is-6">Contact Information:</h6>
                  <p>
                    If you have any questions about this policy or the way we deal with your personal information including your rights to access, correct and update your information, please contact via email at <a href="mailto:ben.kolber@civid.com">ben.kolber@civid.com</a>.
                  </p>
                </li>
                </div>
              </ol>
            </div>
          </section>


    <section className="footer">
      <div className="container">
        <div className="content">
          <p>
            © 2018 Civid, inc.
          </p>
        </div>
      </div>
    </section>
  </div>
  </div>
        );
    }

}

export default Privacy;
