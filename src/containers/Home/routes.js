import React from 'react';
import {Route} from 'react-router';
import Authenticate from '../HOC/Authenticate';
import AuthenticationRequired from '../HOC/AuthenticationRequired';
import Home from "./";



export default (
    <Route path="/home" component={Authenticate(Home)}/>
);
