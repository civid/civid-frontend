import React from "react";
import PropTypes from 'prop-types';
import {Link} from "react-router";

import { getFullName, getProfileImage } from "../../utils/user";


const Leaderboard = ({users}) => {
    const sortedUsers = Object.values(users).sort((a, b) => {
        if (a.influence === b.influence) return 0;
        // Sort order reversed intentionally
        return a.influence < b.influence ? 1 : -1;
    }).slice(0, 5);

    return (<div className="leaderboard content-box box-style">
        <div className="box-header">
            <div>
                <h6 className="title is-6">Influence Leaders
                    <span className="icon faq-icon">
                        <Link className="is-grey" to={`/faq#what-do-i-do-with-influence-points`}>
                            <i className="fas fa-question-circle"/>
                        </Link>
                    </span>
                </h6>
            </div>
        </div>
        <div className="box-bottom">
            <ul className="feed-list">
                {sortedUsers.map((usr, index) => <li key={usr.id}>
                    <span className="order">
                        <img className="user-img is-25" width="25" height="25" src={getProfileImage(usr.id, users)} />
                    </span>
                    <Link className="user-name" to={`/profile/${usr.id}`}>{getFullName(usr.id, users)}</Link>
                    <span className="influence">{usr.influence}</span>
                </li>)}
            </ul>
        </div>
    </div>);
};

Leaderboard.propTypes = {
    users: PropTypes.object.isRequired,
};

export default Leaderboard;
