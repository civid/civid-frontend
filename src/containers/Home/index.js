import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link, browserHistory} from 'react-router';
import Tutorial from '../../components/Tutorial/tutorial';
import { getPostFromQueue } from '../../actions/posts/post/get-post-from-queue'
import Navigation from '../../components/Navigation';
import OpinionCard from '../../components/Opinion-card';
import ModalPostForm from '../../components/modal/modalPost';
import ModalFeedbackForm from '../../components/modal/modalFeedback';
import ModalSignUpForm from '../../components/modal/modalSignUp';
import Post from '../../components/Post/index'
import { getNewestPost } from '../../actions/posts/post/get-newest';
import { getPost } from '../../actions/posts/post/get';
import Anonymous from '../../components/Anonymous';
import PostPreview from '../../components/PostPreview';
import { getProfileImage, getUsername} from '../../utils/user';
import {getTrophiesList} from '../../actions/trophies/trophy/list';
import {getAwardedTrophiesList} from '../../actions/trophies/awarded-trophy/list';
import {getPreviousPosts} from '../../actions/posts/post/get-previous';
import {AWARDED_TROPHY_CONTEXT_TYPES} from '../../utils/normalize';
import Landing from '../../containers/Landing';
import './Home.scss';


let timeoutVar;
class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isClockVisible: false,
            isOpinionModalVisible: false,
            isFeedbackModalVisible: false,
            isSignUpModalVisible: false,
            results: null,
            postId: null,
            delayTutorial: true,
        };
    }

    componentDidMount() {
        const { dispatch, params: {postSlug} } = this.props;
        if (!postSlug)
            dispatch(getNewestPost());
        else
            dispatch(getPost(postSlug));

        dispatch(getTrophiesList());
        dispatch(getAwardedTrophiesList());
        dispatch(getPreviousPosts());
        timeoutVar = setTimeout(() => {this.setState({delayTutorial: false})}, 12000);
    }
    componentWillUnmount() {
        clearTimeout(timeoutVar);
    }

    componentDidUpdate(prevProps) {
        const {posts} = this.props;
            if ((prevProps.params.postSlug !== this.props.params.postSlug) || (prevProps.posts !== posts)) {
                window.scrollTo(0,0);
                const post = Object.values(posts).find(p => p.slug == this.props.params.postSlug);
                if (post)
                    this.setState({postId: post.id});
            }
    }

    setResults = (input) => {
        this.setState({ results: input });
    }

    feedbackModalOpenClose = (e) => {
        this.setState({ 'isFeedbackModalVisible': !this.state.isFeedbackModalVisible })
    }
    SignUpModalOpenClose = (e) => {
        this.setState({ 'isSignUpModalVisible': !this.state.isSignUpModalVisible })
    }
    opinionModalOpenClose = (e) => {
        const {activeUser} = this.props;
        if (!activeUser) this.SignUpModalOpenClose();
        else this.setState({ 'isOpinionModalVisible': !this.state.isOpinionModalVisible })
    }

    getPostData = (data) => {
        if (data !== undefined && data.hasOwnProperty('data')) {
            const key = Object.keys(data.data)
            return data.data[key]
        }
        return
    }

    renderPreviousWinners(postList) {
        const {awardedTrophies, posts, users} = this.props;
        const trophiesList = Object.values(awardedTrophies);
        const titles = Object.values(postList).slice(1,4);
        const len = Math.max(0, trophiesList.length-3);
        const winners = trophiesList.filter(
        at => at.context && at.context.schema === AWARDED_TROPHY_CONTEXT_TYPES.POST).slice(len, trophiesList.length).reverse();
        if (titles.length == 0 || winners.length == 0) return null;

        let x = new Object();
        for (let i = 0; i < Math.min(3, titles.length, winners.length); i++) {
            x[i] = {title: titles[i].title,
                    user: winners[i].user}
        }
        return (
           <div className="content-box box-style is-hidden-mobile">
                <div className="box-header">
                    <div>
                    <h6 className="title is-6">Host's Choice Award
                    </h6>
                    </div>
                </div>
                <div className="winners-bottom">

                    {Object.values(x).map(winner => <div style={{marginBottom: "1rem"}} key={winner.title}>
                        <div className="title is-6" style={{marginBottom: "1rem"}}>{winner.title}</div>
                        <Link to={`/profile/${getUsername(winner.user, users)}`}>
                            <img src={getProfileImage(winner.user, users)} className="user-img size-30" style={{ margin: "0rem 1rem" }}/>
                        <span className="post-name" style={{color:"#4a4a4a"}}>{getUsername(winner.user, users)}</span>
                        </Link>
                        </div>)}
                </div>
            </div>
        );
    }

    renderMostPopular(postList) {
        const {awardedTrophies, posts, users} = this.props;
        const trophiesList = Object.values(awardedTrophies);
        const titles = Object.values(postList);

        return (
           <div className="content-box box-style is-hidden-mobile">
                <div className="box-header">
                    <div>
                    <h6 className="title is-6">Top Influencers
                    </h6>
                    </div>
                </div>
                <div className="winners-bottom">

                    <div style={{marginBottom: "1rem"}}>
                        <div className="title is-6" style={{marginBottom: "1rem"}}>The blockchain is like pokemon go, a meaningless hype</div>
                        <Link to={`/profile/Avelzion`}>
                            <img src="https://civid-media.s3.amazonaws.com/media/f3c35a5.jpg" className="user-img size-30" style={{ margin: "0rem 1rem" }}/>
                        <span className="post-name" style={{color:"#4a4a4a"}}>Avelzion</span>
                        </Link>
                    </div>
                    <div style={{marginBottom: "1rem"}}>
                        <div className="title is-6" style={{marginBottom: "1rem"}}>Before voting, an individual must pass an “I’m not crazy” test.</div>
                        <Link to={`/profile/simbas_son`}>
                            <img src="https://civid-media.s3.amazonaws.com/media/Screen_Shot_2019-02-03_at_20.41.29.png" className="user-img size-30" style={{ margin: "0rem 1rem" }}/>
                        <span className="post-name" style={{color:"#4a4a4a"}}>simbas_son</span>
                        </Link>
                    </div>
                    <div style={{marginBottom: "1rem"}}>
                        <div className="title is-6" style={{marginBottom: "1rem"}}>There are only two genders.</div>
                        <Link to={`/profile/Virtuoso`}>
                            <img src="https://civid-media.s3.amazonaws.com/media/260px-Johann_Sebastian_Bach.jpg" className="user-img size-30" style={{ margin: "0rem 1rem" }}/>
                        <span className="post-name" style={{color:"#4a4a4a"}}>Virtuoso</span>
                        </Link>
                    </div>

                </div>
            </div>
        );
    }

    renderRules() {
        return (
            <div className="content-box box-style is-hidden-mobile">
                <div className="box-header">
                    <div>
                    <h6 className="title is-6">Rules of Discussion
                    </h6>
                    </div>
                </div>
                    <div className="box-bottom">
                        <ul className="rules-list">
                            <li>1. Do not use hostile or offensive language.</li>
                            <li>2. Direct responses must challenge the initiator's argument in some way.</li>
                            <li>3. Responses must contribute meaningfully to the discussion. </li>
                            <li>4. Do not directly accuse the initiator. If you have suspicions that they are not playing fair, please message us. </li>
                            <li>5. A single trophy is awarded for the reply that best changes your mind, if any. Do not award sarcastic, joking, or otherwise non-serious trophies. </li>
                        </ul>
                    </div>
            </div>
        );
    }

    renderFeedback() {
        return (
            <div className="content-box box-style">
                <div className="box-header">
                <div>
                    <h6 className="title is-6">We Want to Hear From You!
                    </h6>
                    </div>
                </div>
                <div className="card-content has-text-centered">
                    <div className="home-button"><p> Got feedback on how to improve Civid beta? Send us a line here and we'll use it to make Civid a better place. </p></div>
                    <button className="button is-blue" onClick={this.feedbackModalOpenClose}>Give Feedback</button>

                    <ModalFeedbackForm visible={this.state.isFeedbackModalVisible} modalClose={this.feedbackModalOpenClose} />
                </div>
            </div>
        );
    }

    renderPreviousDiscussions(postList) {
        if (!postList) return null;
        return (
            <div className="columns">
                {postList.slice(1, 4).map(post => <PostPreview
                    key={post.slug}
                    post={post}
                />)}
            </div>
        );
    }

    render() {
        const{ activeUser } = this.props;
        if (!activeUser) return(<Landing />);
        else {
        const { posts, users, params:{postSlug}} = this.props;
        const { results, postId, delayTutorial } = this.state;
        const postList = Object.values(posts).sort((a, b) => {
            return new Date(b.pub_date) - new Date(a.pub_date)});
        let post = new Object();
        if (!postSlug)
            post = postList[0];
        else post = posts[postId];
        if (!post) return null;

        return (
            <div className="home">
                <div className="app-container">

                    <Navigation />

                    {!activeUser && !Tutorial.tutorialAlreadyShown() && !delayTutorial
                        && <Tutorial
                             title={post.title}
                             user={post.username}
                             userimg={getProfileImage(post.user, users)}
                             postimg={post.image}
                             setResults={this.setResults} />}

                    <section className="feed-banner has-background-white">
                        <h3 className="title has-text-centered control">Think you can change their mind?</h3>
                        <h5 className="subtitle is-5 has-text-centered">
                        <nav className="level">
                            <p className="level-item centered-content user-name">
                                Today's Host:
                                <Link className="user-name" to={`/profile/${post.username}`}>
                                    <img src={getProfileImage(post.user, users)} className="user-img size-40" style={{ margin: "0 1rem" }}/>
                                </Link>

                                <Link to={`/profile/${post.username}`} style={{ color: "#4A4A4A" }}>
                                    {post.username}
                                </Link>
                            </p>
                        </nav>
                        </h5>
                    </section>

                    <div className="container">
                        <div className="columns main-content margin-top-2">
                            <div className="column is-9">

                                <Post tutResults={results} post={post} SignUpModalOpenClose={this.SignUpModalOpenClose} />

                                <div className="is-size-5" style={{ fontWeight:"600", paddingTop: "2rem", paddingBottom: "1rem" }}><span style={{borderBottom: "5px solid #3081f9", paddingBottom: ".4rem" }}>Recent Discussions<hr style={{ margin: ".5rem 0", backgroundColor: "#95989a" }}/></span>
                                </div>
                                {this.renderPreviousDiscussions(postList)}
                            </div>
                            <div className="column is-3">
                                <OpinionCard click={this.opinionModalOpenClose} activeUser={activeUser} />


                                {this.renderPreviousWinners(postList)}
                                {this.renderMostPopular(postList)}
                                {this.renderRules()}
                                {this.renderFeedback()}

                            </div>

                            <ModalPostForm visible={this.state.isOpinionModalVisible} modalClose={this.opinionModalOpenClose} />
                            <ModalSignUpForm visible={this.state.isSignUpModalVisible} modalClose={this.SignUpModalOpenClose} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    }
}

export default connect(state => ({
    posts: state.posts.data,
    activeUser: state.activeUser,
    trophies: state.trophies.data,
    awardedTrophies: state.awardedTrophies.data,
    users: state.users.data
}))(Home);
