import React, {Component} from 'react';
import {connect} from 'react-redux';
import Dropzone from 'react-dropzone';
import {Link} from 'react-router';
import classNames from 'classnames';

import {getUser} from '../../actions/accounts/user/get';
import {editProfile} from '../../actions/accounts/profile/edit';
import {getFollowers} from '../../actions/accounts/profile/followers/get';
import {followUser} from '../../actions/accounts/profile/following/create';
import {unfollowUser} from '../../actions/accounts/profile/following/delete';
import {getAwardedTrophiesList} from '../../actions/trophies/awarded-trophy/list';
import PostList from '../../components/PostList';
import {POST_LIST_FILTER_TYPES} from '../../components/PostList/postListOptions';
import Navigation from '../../components/Navigation';
import {getFullName, getProfileImage} from '../../utils/user';
import actionTypes from '../../config/action-types';
import './Profile.scss';


/*
const mapDispatchToProps = dispatch => ({
    onUnload: () => {
        dispatch({ type: actionTypes[`PROFILE_PAGE_UNLOADED_PENDING`] });
        try {
            dispatch({
                type: actionTypes[`PROFILE_PAGE_UNLOADED_SUCCESS`]
            });
        } catch (error) {
            throw error;
        }
    }
});
*/

class Profile extends Component {

    state = {
        userId: null
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this._fetchData(this.props);
    }

    /*
    componentWillUnmount() {
        this.props.onUnload();
    }
    */

    componentDidUpdate(prevProps) {
        const {dispatch, params: {username}, users} = this.props;
        if (prevProps.params.username !== username) {
            this._fetchData(this.props);
        }

        if (prevProps.users !== users) {
            const id = Object.values(users).find(u => u.username == username);
            if (id != undefined) {
                this.setState({userId: id.id});
            }
        }
    }

    onDrop = files => {
        const {activeUser, dispatch} = this.props;
        dispatch(editProfile({
            ...activeUser.profile,
            image: files[0],
        }))
    };

    follow = () => {
        const {user, dispatch} = this.props;
        dispatch(followUser({
            id: user.id
        }));
    };

    unfollow = () => {
        const {activeUser, user, dispatch} = this.props;
        dispatch(unfollowUser({
            userId: user.id,
            followerId: activeUser.id,
        }));
    };

    _fetchData(props) {
        const {dispatch, params: {username}} = props;
        dispatch(getUser(username));
        dispatch(getAwardedTrophiesList({username}));
    }

    followButtonHelper() {
        const {activeUser, followers, user} = this.props;
        if (activeUser.id === user.id) return null;
        let isFollowing = [];
        if (followers) isFollowing = Object.values(followers).filter(follower => follower.id === activeUser.id);
        return (isFollowing.length === 0 ? this.renderFollowButton() : this.renderUnfollowButton());
    }

    renderFollowButton() {
        const {activeUser} = this.props;
        if (!activeUser) return null;

        return (
            <div onClick={this.follow} className="action-buttons">
                <Link className="button is-blue is-small">
                    <span className="icon">
                        <i className="fas fa-user-plus is-small"/>
                    </span>
                    <div className="follow-btn">
                        Follow
                    </div>
                </Link>
            </div>
        );
    }

    renderUnfollowButton() {
        const {activeUser} = this.props;
        if (!activeUser) return null;

        return (
            <div onClick={this.unfollow} className="action-buttons">
                <Link className="button is-blue is-small">
                    <span className="icon">
                        <i className="fas fa-user-check is-small"/>
                    </span>
                    <div className="follow-btn">
                        Following
                    </div>
                </Link>
            </div>
        );
    }

    renderEditProfileImage(user) {
        const {activeUser} = this.props;
        if(!activeUser || activeUser.id !== user.id) return null;
        return (
          <Dropzone onDrop={this.onDrop}>
            {({getRootProps, getInputProps, isDragActive}) => {
              return (
                <div
                  {...getRootProps()}
                  className={classNames('dropzone edit-profile-image', {'dropzone--isActive': isDragActive})}
                >
                  <input {...getInputProps()} />
                  {
                    isDragActive ?
                      <p>Drop files here...</p> :
                      <p>Edit profile image</p>
                  }
                </div>
              )
            }}
          </Dropzone>
        );

    }

    renderCredentials(user) {
        return (
            <div className="box-bottom">
                <ul className="cred-list body-text">
                    <li>
                        <span className="icon has-text-grey-light">
                            <i className="fas fa-map-marker-alt"/>
                        </span>
                        {(user.location != "" && user.location != null) && <span>Lives in {user.location}</span>}
                    </li>
                    <li>
                        <span className="icon has-text-grey-light">
                            <i className="fas fa-briefcase"/>
                        </span>
                        {user.work}
                    </li>
                    <li>
                        <span className="icon has-text-grey-light">
                            <i className="fas fa-user-graduate"/>
                        </span>
                        {user.education}
                    </li>
                </ul>
            </div>
        );
    }

    renderPoints(user) {
        const {trophies, awardedTrophies} = this.props;
        let userTrophyImages = {};
        const userTrophies = Object.values(awardedTrophies)
            .filter(at => at.user === user.id)
            .reduce((acc, at) => {
                const trophyTitle = trophies[at.trophy].title;
                userTrophyImages[trophyTitle] = trophies[at.trophy].image;
                acc[trophyTitle] = trophyTitle in acc ? acc[trophyTitle] + 1 : 1;
                return acc;
            }, {});
        return (
            <ul className="cred-list">
                <li>
                <nav className="level is-mobile">
                    <div className="level-item has-text-centered">
                        <div className="title is-5">{user.influence}</div>
                    </div>
                    <div className="level-item has-text-centered">
                            <div>Influence</div>
                    </div>
                </nav>
                </li>

                {Object.keys(userTrophies).map(title =>
                    <li key={title}>
                        <nav className="level is-mobile">
                            <img className="level-item has-text-centered" src={userTrophyImages[title]} height="38" width="38" />
                            <div className="level-item has-text-centered">
                                {`${title}${userTrophies[title] > 1 ? ' x ' + userTrophies[title] : ''}`}
                            </div>
                        </nav>
                    </li>
                )}
            </ul>
        );
    }

    renderFollowers() {
        const {followers} = this.props;
        let follows = {};
        if (followers)
            follows = Object.values(followers);

        return (
            <div className="level-item has-text-centered">
                <div>
                    <div className="title is-6">Followers</div>
                    <div className="has-text-grey">{follows.length}</div>
                </div>
            </div>
        );
    }


    render() {
        const {activeUser, users, posts, params: {username}, trophies, awardedTrophies} = this.props;
        const {userId} = this.state;
        let user = "";
        if (userId)
            user = users[userId];

        return (
            <div className="Profile">
                <Navigation />
                <section className="section">
                    <div className="container">
                        <div className="profile-box box-style">
                            <div className="columns is-reverse-mobile">
                                <div className="column is-3">
                                    <img src={getProfileImage(user.id, users)} className="profile-img" height="200" width="200"/>
                                    {this.renderEditProfileImage(user)}
                                </div>
                                <div className="column is-9">
                                    <h4 className="title is-4">
                                        {user.username}
                                    </h4>
                                    <h5 className="subtitle is-5">
                                        {user.description}
                                    </h5>
                                    <p>
                                        {user.about_text}
                                    </p>

                                    {activeUser && activeUser.id === user.id && <div className="action-buttons"><Link className="button is-small edit-button" to={`/account/basic-information`}>Edit Profile</Link></div>}
                                </div>
                            </div>
                        </div>
                        <div className="columns">
                            <div className="column is-3">
                                <div className="content-box box-style">
                                    <div className="box-header">
                                        <div>
                                        <h6 className="title is-6">Trophy Case</h6>
                                        </div>
                                    </div>
                                    <div className="box-bottom">
                                        {this.renderPoints(user)}
                                    </div>
                                </div>

                                <div className="content-box box-style">
                                    <div className="box-header">
                                        <div>
                                        <h6 className="title is-6">Background & Credentials</h6>
                                        </div>
                                    </div>
                                    {this.renderCredentials(user)}
                                </div>
                            </div>
                            <div className="column is-9">
                                <div className="content-box">

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        activeUser: state.activeUser,
        followers: state.followers.data,
        posts: state.posts.list,
        trophies: state.trophies.data,
        awardedTrophies: state.awardedTrophies.data,
        users: state.users.data
    }
};

export default connect(mapStateToProps)(Profile);
