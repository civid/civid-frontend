import React from 'react';
import {Route} from 'react-router';

import Authenticate from '../HOC/Authenticate';
import Profile from "./";


export default (
    <Route path="/profile/:username" component={Authenticate(Profile)}/>
);
