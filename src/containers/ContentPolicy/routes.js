import React from 'react';
import {Route} from 'react-router';
import Authenticate from '../HOC/Authenticate';
import AuthenticationRequired from '../HOC/AuthenticationRequired';
import Content from "./";



export default (
    <Route path="/content-policy" component={Authenticate(Content)}/>
);
