import React, {Component} from 'react';
import logo from '../../assets/images/iconlogo.png';
import Navigation from '../../components/Navigation';
import './contentpolicy.scss';


class Content extends Component {

  render() {

    return (
      <div className="ContentPolicy">
        <div className="app-container">
          <Navigation />


          <section className="section">
            <div className="container">
              <h4 className="title is-4">Civid Content Policy</h4>
                <div className="content">
                  <h6 className="subtitle is-6">
                  Core Principles
                  </h6>
                  <p>
                    <i>Observe, reflect, respect </i><br/>Civid is a place for people to express their opinions on worldwide issues happening right now. Observe and engage with different views, learn from the experience of others, and respect every member of the Civid community.
                  </p>
                  <p>
                    <i>Relevant</i><br/>Civid’s goal is to be the ultimate source for people to stay informed, and get and keep up to date about timely events. Discuss relevant issues, that can help shed light and truth about real world affairs.
                  </p>
                  <p>
                    <i>Helpful</i><br/>Your posts should be helpful for other readers to better understand an issue. Don’t think about it as making a point, but as rather sharing your perspective and knowledge to help others better understand the latest news and world stories.
                  </p>
                </div>
                <div className="content">

                  <h6 className="subtitle is-6">Real name policy</h6>
                  <p>
                    On Civid, all users are required to use their real, full name once they create an account. If Civid staff believes that a user might be using a name that is not theirs, the user will be asked to present supporting evidence of his full name.
                  </p>
                  <p>
                    <i>Full names must appear in English</i><br/>All characters of a profile name must be constructed of letters from the Latin Alphabet. Any user who wishes to write his name in a different language, can do that in parenthesis following his name.
                      i.e. Ben Kolber (בן קולבר), John Doe (约翰).
                  </p>
                  <p>
                    <i>Titles</i><br/>In your profile full name, titles such as “Mr.”, “Dr.” and “sir” may not be used. Information about education, degrees, or anything else, can be added to your Credentials on the profile page.
                  </p>
                  <p>
                    <i>Capitalization</i><br/>Names on Civid may not be fully capitalized i.e. DANIELLA HART. Names should be capitalized appropriately i.e. Daniella Hart.
                  </p>
                </div>

                <div className="content">

                  <h6 className="subtitle is-6">Policy on post removal</h6>
                  <p>
                    A post that is out of context, unrelated to the discussed issue and does not help a visitor to better understand, will be collapsed.
                  The following policies also dictate what type of post will be collapsed by Civid moderation.
                  </p>

                  <p>
                    <i>Doesn’t relate to the content</i><br/>A post needs to relate to the topic being discussed, regardless of debate, discussion or ‘Change My Mind’. If the post is unrelated, responds to a different discussion, and in any way, does not relate directly to the issue being discussed, it is violating Civid policy. It is okay to relate your post to another post in the same discussion, as long as the post helps by adding meaningful content to a discussion, and helps other people who want to learn about the subject, better understand it.
                  </p>
                  <p>
                  <i>Jokes</i><br/>Jokes do not contribute much to the better understanding of an issue, although, we do not discourage humor in writing. As long as a humorous post helps better understand the subject, or sheds a new perspective on the issue, they are okay to post. Anything else, is not.
                  </p>
                  <p>
                    <i>Violates the image policy</i><br/>Please refer to the image policy of Civid.
                  </p>
                  <p>
                    <i>Needs to be in English</i><br/>Civid is currently an English only platform. All posted content must be in the English language.
                  </p>
                  <p>
                  <i>Bad formatting</i><br/>Answers who are formatted poorly, diminish from the content and quality of a post, and will be collapsed by Civid moderation. The most likely reason for a post to be removed due to poor formatting usually is poor grammar, poor use of spacing or use of ‘text-speak’ style writing. Refer to the ‘what are good formatting standards for a Civid post’.
                  </p>
                </div>
                <div className="content">
                  <h6 className="subtitle is-6">What does initiating a relevant discussion mean?</h6>
                  <p>
                    The point of a discussion is to be the place where people come to learn about issues happening around the world.
                  </p>
                  <p>
                    <i>Relevance</i><br/>A good discussion is one that is relevant to something happening this week in the world. Civid wants to help readers better understand issues happening today, so posting a discussion “is meat bad” and “Should prostitution be legal” are not relevant discussions.
                  </p>
                  <p>
                    <i>Clarity</i><br/>A discussion issue should be clearly written, grammatically correct and to the point. This means that the discussion issue should contain enough information so that meaningful opinions can be posted.
                  </p>
                  <p>
                    <i>Help readers obtain information</i><br/>By creating a discussion, you are seeking to obtain information, not to make a claim or a statement. Discussions should encourage different perspectives, different opinions, and different explanations. Discussions need to contain open ended questions, that does not narrow itself to a yes or no type poll.
                  </p>
                  <p>
                    <i>Comply with policy</i><br/>All discussions should comply with Civid’s posting policy.
                  </p>
                  <p>
                    <i>Topics</i><br/>Topics are used to tag discussions so that readers can more easily find the discussions they care about. They should identify the subject of the discussion, and help localize the question, i.e. “Should an A.I. be built” will not be related to world stories, but rather computer science.
                  </p>
                </div>

                <div className="content">
                  <h6 className="subtitle is-6">What does a good post on Civid look like?</h6>
                  <p>
                  Civid’s goal is to help the people better understand what is happening in the world right now.
                  To help readers delve into the discussion, and better understand an issue being discussed, we mandate writing posts which are helpful to anyone who wants to learn about an issue. A post is helpful when it is clearly written, credible, directly relates to the issue being discussed and brings a new perspective to the discussion.
                  </p>
                  <p>
                  <i>Relating to the issue</i><br/>A good post will relate to the issue being discussed, as well as written in a way that anyone can easily understand, and enter the discussion. You should always assume that people reading your posts, have no background information about the issue, and are coming to you for an explanation.
                  </p>
                  <p>
                    <i>Provides knowledge to anyone interested in the issue</i><br/>A good post is not one that is purely an opinion. If it is purely opinion, convince your readers your opinion is correct using facts. A good post has facts and insights that are useful for proper understanding of an issue.
                  </p>
                  <p>
                    <i>Supported with logic</i><br/>Always include logic and rationale in your posts. This will help demonstrate your posts accuracy, and will allow your reader to understand why your opinion is the correct one.
                  </p>
                  <p>
                    <i>Credible and factual</i><br/>A good post will persuade readers that it is trustworthy, using facts, personal experience, logic and good explanations.
                  </p>
                  <p>
                    <i>Clear and easy to read</i><br/>A good post is one that is easy to read and understand by anyone who wishes to learn more about an issue. Anything you add should clarify you point, your post should be enjoyable to read, and avoids sarcasm and inside jokes.
                  </p>
                </div>
                <div className="content">
                  <h6 className="subtitle is-6">How Civid distributes quality discussions</h6>
                  <p>
                    <i>Relevance</i><br/>if the discussion is relevant to the events of the week, it will reach more people, and continue to as long as it remains time relevant.
                  </p>
                  <p>
                    <i>Content</i><br/>If a discussion is lively, and people are inputting useful and informative posts, the discussion will reach more readers.
                  </p>
                  <p>
                    <i>Influence</i><br/>A writer who has obtained more influence, will receive more views to discussions that he creates, as long as they are time relevant and uphold Civid policy.
                  </p>
                </div>
                <div className="content">
                  <h6 className="subtitle is-6">What are some good formatting standards, in addition to the Civid policies?</h6>
                  <p>
                    <i>Discussion issues</i>
                  </p>
                    <ul>
                      <li>Don’t make claims or statements in the discussion issue. Keep it open ended, and encourage different thoughts and opinions.</li>
                      <li>When using quotes, use double quotes, not single.</li>
                      <li>A single question mark is sufficient in all cases.</li>
                      <li>Issues should be short and clear.</li>
                      </ul>
                  <p>
                    <i>Posts</i>
                  </p>
                    <ul>
                      <li>
                      Use single spacing and not double spacing.</li>
                      <li>Make sure your sentences are not fragmented.</li>
                      <li>Organize your text using formatting tools Civid provides.</li>
                      <li>For lists, use numbers and bullet points.</li>
                      </ul>
                </div>
                <div className="content">
                  <h6 className="subtitle is-6">Adding images and links to answers.</h6>
                  <p>
                    <i>The following images are banned from use in posts on Civid:</i>
                  </p>
                    <ul>
                      <li>Meme pictures.</li>
                      <li>Humorous, sex related, or violent content that does not relate to the issue being discussed, and does not help someone who wishes to learn more about the issue.</li>
                      <li>Graphic violence images - graphic violent images are only allowed to be used in two situations:
                        <ul>
                          <li>The image helps to better understand the discussion.</li>
                          <li>The image does not glorify violence.</li>
                        </ul>
                      </li>
                    </ul>
                </div>
            </div>
          </section>


    <section className="footer">
      <div className="container">
        <div className="content">
          <p>
            © 2018 Civid, inc.
          </p>
        </div>
      </div>
    </section>
  </div>
  </div>
        );
    }

}

export default Content;
