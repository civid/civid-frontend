import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import Truncate from 'react-truncate';
import TextTruncate from 'react-text-truncate';
import TimeAgo from 'react-timeago';
import Navigation from '../../components/Navigation';
import {getNotificationList} from '../../actions/notify/notification/list';
import {markNotificationsAsRead} from '../../actions/notify/notification/markAsRead';
import {getProfileImage, getUsername} from "../../utils/user";
import {Link, withRouter} from 'react-router';
import {NOTIFICATION_TYPES} from '../../utils/normalize';
import './Notifications.scss';


class NotificationItem extends Component {

    onMarkAsRead = () => this.props.onMarkAsRead(this.props.notification.id);

    onFollowTheLink = () => this.props.onFollowTheLink(this.props.notification.id, this._getLinkUrl());

    renderNotificationText() {
        const {notification: n, postReplies, trophies} = this.props;
        const postReply = postReplies[n.action_object];
        const trophy = trophies[n.action_object];

        switch (n.verb) {
            case NOTIFICATION_TYPES.REPLY_TO_COMMENT:
                return `replied to your comment with "${postReply ? postReply.claim_0 : ''}".`;
            case NOTIFICATION_TYPES.RESPONSE_TO_OPINION:
                return `responded to your opinion with "${postReply ? postReply.title : ''}".`;
            case NOTIFICATION_TYPES.NEW_FOLLOWER:
                return 'followed you.';
            case NOTIFICATION_TYPES.RECEIVED_TROPHY:
                return `awarded you a trophy - "${trophy ? trophy.title : ''}"!`;
            case NOTIFICATION_TYPES.TROPHY_REMINDER:
                return 'reminder: you have a trophy to award in the discussion you started.'
        }
    }

    renderLinkText() {
        switch (this.props.notification.verb) {
            case NOTIFICATION_TYPES.REPLY_TO_COMMENT:
            case NOTIFICATION_TYPES.RESPONSE_TO_OPINION:
                return 'Show your opinion';
            case NOTIFICATION_TYPES.NEW_FOLLOWER:
                return null;
            case NOTIFICATION_TYPES.RECEIVED_TROPHY:
                return 'Show my profile';
            case NOTIFICATION_TYPES.TROPHY_REMINDER:
                return 'Show your opinion';
        }
    }

    renderIcon() {
        switch (this.props.notification.verb) {
            case NOTIFICATION_TYPES.REPLY_TO_COMMENT:
                return (
                    <span className="icon is-small has-text-success">
                        <i className="fas fa-comments"/>
                </span>);
            case NOTIFICATION_TYPES.RESPONSE_TO_OPINION:
                return (
                    <span className="icon is-small has-text-primary">
                        <i className="fas fa-comment"/>
                    </span>);
            case NOTIFICATION_TYPES.NEW_FOLLOWER:
                return (
                    <span className="icon is-small has-text-info">
                        <i className="fas fa-user-plus"/>
                    </span>);
            case NOTIFICATION_TYPES.RECEIVED_TROPHY:
                return (
                    <span className="icon is-small has-text-success">
                        <i className="fas fa-trophy"/>
                    </span>);
            case NOTIFICATION_TYPES.TROPHY_REMINDER:
                return (
                    <span className="icon is-small has-text-warning">
                        <i className="fas fa-trophy"/>
                    </span>);
        }
    }

    _getLinkUrl() {
        const {notification, users} = this.props;
        switch (notification.verb) {
            case NOTIFICATION_TYPES.REPLY_TO_COMMENT:
            case NOTIFICATION_TYPES.RESPONSE_TO_OPINION: {
                const {posts, postReplies} = this.props;
                const postReply = postReplies[notification.action_object];
                return `/posts/${posts[postReply.post].slug}`;
            }
            case NOTIFICATION_TYPES.NEW_FOLLOWER:
                return `/profile/${getUsername(notification.actor, users)}`;
            case NOTIFICATION_TYPES.RECEIVED_TROPHY:
                return `/profile/${getUsername(notification.recipient, users)}`;
            case NOTIFICATION_TYPES.TROPHY_REMINDER: {
                const {posts} = this.props;
                const post = posts[notification.target];
                return `/posts/${post.slug}`;
            }
        }
    }

    renderTimeAgo() {
        const {notification} = this.props;
        return (
            <span className="timeago-text">
                <TimeAgo date={notification.timestamp} minPeriod="60"/>
            </span>
        );
    }

    render() {
        const {notification: n, users} = this.props;
        const newFollowerNotification = n.verb === NOTIFICATION_TYPES.NEW_FOLLOWER;
        const actorLinkProps = {};
        actorLinkProps[newFollowerNotification ? 'onClick' : 'to'] =
            newFollowerNotification ? this.onFollowTheLink : `/profile/${n.actor}`;

        return (
            <div className="Notification">
                <div className="notification is-white">
                <button className="delete" onClick={this.onMarkAsRead} />
                    <article className="media">
                        <figure className="media-left">
                            <Link className="user-name" to={`/profile/${getUsername(n.actor, users)}`}>
                                <img src={getProfileImage(n.actor, users)} className="user-img size-30" />
                            </Link>
                        </figure>
                        <div className="media-content">
                            <span>
                                <Link className="user-name" {...actorLinkProps}>
                                    {getUsername(n.actor, users)}
                                </Link>
                            </span>
                            &nbsp;
                                <span className="post-short body-text">
                                    <TextTruncate lines={2} truncateText='..."' text={this.renderNotificationText()}
                                    />
                                </span>
                            {!newFollowerNotification && (<div>
                                <Link className="verb-link body-text" onClick={this.onFollowTheLink}>
                                    {this.renderLinkText()}
                                </Link>
                            </div>)}

                            <div className="notif-details">
                                {this.renderIcon()}
                                {this.renderTimeAgo()}
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        );
    }
}

NotificationItem.propTypes = {
    notification: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired,
    postReplies: PropTypes.object.isRequired,
    trophies: PropTypes.object.isRequired,
    posts: PropTypes.object.isRequired,
    onMarkAsRead: PropTypes.func.isRequired,
    onFollowTheLink: PropTypes.func.isRequired,
};

class Notifications extends Component {

    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(getNotificationList());
    }

    onMarkAsRead = id => this.props.dispatch(markNotificationsAsRead({ ids: [id] }));

    onMarkAllAsRead = () => {
        const ids = Object.values(this.props.notifications).map(n => n.id);
        this.props.dispatch(markNotificationsAsRead({ ids }));
    };

    onFollowTheLink = (id, url) => {
        this.props.dispatch(markNotificationsAsRead({ ids: [id] }))
            .then(() => this.props.router.push(url));
    };

    render() {
        const {notifications, users, postReplies, trophies, posts} = this.props;
        const sortedNotifications = Object.values(notifications).sort((a, b) => {
            if (a.timestamp === b.timestamp) return 0;
            // Sort order reversed intentionally
            return a.timestamp < b.timestamp ? 1 : -1;
        });

        return (<div className="app-container">
            <Navigation />
            <section className="section">
                <div className="container">
                    <div className="columns">
                        <div className="column is-8 is-offset-2">
                            <div className="content-box notif-box box-style">
                                <div className="post-content">
                                    {sortedNotifications.length > 0 && <h6 className="subtitle is-6 has-text-centered">
                                        <Link onClick={this.onMarkAllAsRead}>Mark All As Read</Link>
                                    </h6>}
                                    {sortedNotifications.length > 0 && sortedNotifications.map(n =>
                                        <NotificationItem
                                            key={n.id}
                                            notification={n}
                                            users={users}
                                            postReplies={postReplies}
                                            trophies={trophies}
                                    posts={posts}
                                    onMarkAsRead={this.onMarkAsRead}
                                    onFollowTheLink={this.onFollowTheLink}
                                />
                            )}
                            {sortedNotifications.length === 0 && (
                                        <div className="empty-section">
                                            <p className="title is-6 has-text-centered">
                                        No new notifications.
                                            </p>
                                        </div>)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>);
    }
}

export default withRouter(connect(state => ({
    notifications: state.notifications.data,
    users: state.users.data,
    postReplies: state.postReplies.data,
    trophies: state.trophies.data,
    posts: state.posts.data,
}))(Notifications));
