import React from 'react';
import {Route} from 'react-router';

import Authenticate from '../HOC/Authenticate';
import Notifications from "./";


export default (
    <Route path="/notifications" component={Authenticate(Notifications)} />
);
