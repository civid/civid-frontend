import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import logo from '../../assets/images/iconlogo.png';
import LandingNavigation from '../../components/LandingNavigation';
import Footer from '../../components/Footer';


class About extends Component {

  render() {
    const { users } = this.props;

    return (
      <div className="Landing">
        <div className="app-container">
          <LandingNavigation {...users} />

          <section className="section is-alternative">
            <div className="container">
              <div className="content">
                <h1 className="title is-4 is-spaced">
                  What is Civid?
                </h1>
                <p>
                  Civid is a social community where individuals engage in meaningful discussion about what’s happening in the world. Challenge your stance on current events and global issues, and present new perspectives to those who are open to discuss their opinion. Become a true influencer, make a real impact, and help our community better understand events shaping our world.
                </p>
                <br />
              </div>
              <div className="content">
                <h1 className="title is-4 is-spaced">
                  Why does Civid exist?
                </h1>
                <p>
                  Unlike online forums or social media platforms, Civid exists to generate respectul and purposeful discussions. It is a community for opinionated influencers who realize there are infinite ways to look at any story, not preach a one sided opinion. Periodically, our featured host will present an opinion to be challenged, opening the discussion to those who support the claim or seek to oppose it. As a community, we help the discussion leader broaden their (and our) understanding of the event by providing different perspectives, shedding light on new information and facts, and generating meaningful discussions. The host will then choose an influential user who formed the most compelling argument and award the noble “Sapere Aude” title, for helping gain a new perspective.
                </p>
                <br />
              </div>
              <div className="content">
                <h1 className="title is-4 is-spaced">
                  What's the point?
            </h1>
                <p>
                  At Civid, we believe that rational and thoughtful discussion is a cornerstone of intellectual progress. Significant events nowadays are misunderstood, misused for manipulation, or wrongfully interpreted to fit personal agendas. Civid is a space to escape the online echo chambers, internet trolls, and hateful comments, and provide a platform where your opinions are heard, appreciated, respected, and acknowledged.
            </p>
                <br />
              </div>
              <div className="content">
                <h1 className="title is-4 is-spaced">
                  Our goal at Civid
          </h1>
                <p>
                  Civid’s goal is to create a collaborative and influential community, which is committed to gaining a greater understanding of the issues happening around us. By both challenging and justifying your own stance, members of the Civid community contribute new knowledge, while influencing their fellow users.
                    Lastly, the Civid community does not preach or insult differing opinions - it embraces diversity of thought. Remember: Your opinion does not define you. Be open to challenge it. You have everything to gain, and nothing to lose.
               </p>
              </div>
            </div>
          </section>

          <Footer />


        </div>
      </div>
    );
  }

}

export default connect(state => ({
  activeUser: state.activeUser,
  users: state.users.data
}))(About);
