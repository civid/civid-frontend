import React from 'react';
import { Route } from 'react-router';
import Authenticate from '../HOC/Authenticate';
import About from "./";

export default (
    <Route path="/about" component={Authenticate(About)} />
);
