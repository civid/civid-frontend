import React, {Component} from 'react';
import {Link} from 'react-router';
import AcceptInvitationForm from '../../../forms/AcceptInvitationForm';
import './AcceptInvitation.scss'


class AcceptInvitation extends Component {

    render() {
        const {params} = this.props;
        return (
                <section className="section">
                    <div className="bg-container">
                        <div className="bg-image">
                        </div>
                    </div>
                    <div className="container">
                        <div className="column is-6 is-offset-3">
                        <div className="content-box box-style">
                            <div className="signup-content">
                                <div className="title is-4">Join our community</div>
                                <div className="subtitle is-6 has-text-grey">Take part in a global conversation dedicated to learning more about each other.</div>
                                <AcceptInvitationForm params={params}/>
                                <p className="privacy-agree">By signing up, you agree to our <Link to={`/terms`}>Terms</Link> and that you have read our <Link to={`/privacy`}>Privacy Policy</Link> and <Link to={`/content-policy`}>Content Policy</Link>.
                                </p>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
        );
    }

}

export default AcceptInvitation;
