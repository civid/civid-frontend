import React from 'react';
import { Route } from 'react-router';
import Authentication from "../Authentication";
import Unauthenticated from '../../containers/HOC/Unauthenticated';
import AuthenticationRequired from '../HOC/AuthenticationRequired';
import Login from "./Login";
import SignUp from "./SignUp"
import VerifyEmailClickLink from './Verify-email-click-link'
import RemoveUserFromMailingList from './remove-from-mailing-list'
import AcceptInvitation from "./AcceptInvitation";
import ForgotPassword from './Forgot-password'

export default (
    <Route path="/" component={Unauthenticated(Authentication)}>
        <Route path="login" component={Login} />
        <Route path="sign-up" component={SignUp} />
        <Route path="forgot-password" component={ForgotPassword} />
        <Route path="verify-email/:id" component={VerifyEmailClickLink} />
        <Route path="remove-from-mailing/:id" component={AuthenticationRequired(RemoveUserFromMailingList)} />
        <Route path="accept-invitation" component={AcceptInvitation} />
        <Route path="accept-invitation/:code" component={AcceptInvitation} />
    </Route>
);
