import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router';
import { emailVerify } from '../../../actions/accounts/user/email-verify'


class VerifyEmailClickLink extends Component {

    state = {
        message: 'Verifying your email ...'
    }

    componentDidMount() {
        const { dispatch } = this.props;
        const pathParts = window.location.pathname.split('/')
        const data = pathParts[pathParts.length - 1]

        dispatch(emailVerify(data))
            .then(() => {
                this.setState({ 'message': 'Email verified! Please login.' })
            })
            .catch(error => {
                this.setState({ message: error.response.data });
            });
    }

    render() {

        const { message } = this.state

        return (
            <section className="section">
                <center><div className="title is-5" style={{marginBottom: "2rem"}}>{message}</div></center>
                <center><Link to="/">Return to Home</Link></center>
            </section>
        )
    }
}



export default connect(state => ({
    activeUser: state.activeUser,
    users: state.users.data
}))(VerifyEmailClickLink);
