import React, { Component } from 'react';
import LandingNavigation from '../../components/LandingNavigation';
import Navigation from '../../components/Navigation';
import Footer from '../../components/Footer'

class Authentication extends Component {

    render() {        return (
            <div className="app-container">
                <div className="Login">
                   <Navigation />
                    {this.props.children}
                </div>
                <Footer />
            </div>
        );
    }

}

export default Authentication;
