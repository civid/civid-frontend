import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RemoveUserFromMailingList } from '../../../actions/accounts/user/remove-from-mailling'


class RemoveUserFromMailing extends Component {

    state = {
        message: 'Removing your email from our mailing list ...'
    }

    componentDidMount() {
        const { dispatch } = this.props;
        const pathParts = window.location.pathname.split('/')
        const data = pathParts[pathParts.length - 1]

        dispatch(RemoveUserFromMailingList(data))
            .then(() => {
                this.setState({ 'message': 'Your email has been remmoved' })
            })
            .catch(error => {
                this.setState({ message: error.response.data });
            });
    }

    render() {

        const { message } = this.state

        return (
            <div className="columns">
                <div className="column">
                    <p>{message}</p>
                </div>
            </div>
        )
    }
}



export default connect(state => ({
    activeUser: state.activeUser,
    users: state.users.data
}))(RemoveUserFromMailing);
