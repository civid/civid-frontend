import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import {Link} from 'react-router';
import LoginForm from '../../../forms/LoginForm';
import Navigation from '../../../components/Navigation';
import './Login.scss'
import SocialLogin from '../../../components/social-auth'


class Login extends Component {

    render() {

        return (
            <section className="signup-section">
                <div className="container">
                    <div className="column is-6 is-offset-3">
                        <div className="content-box box-style">

                            <div className="post-content">
                                <div className="title is-6">Login</div>
                                <SocialLogin />
                                <div className="has-text-centered" style={{fontWeight:"500", fontSize:"1rem", marginBottom:"1rem"}}> Or </div>
                                <LoginForm />
                                <p style={{paddingTop:"1rem"}}>Don't have an account? <Link to={`/sign-up`}>Sign Up</Link></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

}

Login = reduxForm({
    form: 'Login'
})(Login);

export default connect(state => ({
    activeUser: state.activeUser
}))(Login);
