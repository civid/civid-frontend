import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import ForgotPasswordForm from '../../../forms/ForgotPasswordForm';


class ForgotPassword extends Component {

    render() {
        return (
            <section className="section is-alternativ">
                <div className="container">
                    <div className="column is-6 is-offset-3">
                        <div className="content-box box-style">
                            <div className="post-content">
                                <div className="title is-6">Forgot password</div>
                                <ForgotPasswordForm />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

}

ForgotPassword = reduxForm({
    form: 'ForgotPassword'
})(ForgotPassword);

export default connect(state => ({
    activeUser: state.activeUser
}))(ForgotPassword);
