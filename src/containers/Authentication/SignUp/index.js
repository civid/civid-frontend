import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import {Link} from 'react-router';
import SignUpForm from '../../../forms/SignUpForm';
import Navigation from '../../../components/Navigation';
import './signup.scss'
import SocialLogin from '../../../components/social-auth'

class SignUp extends Component {


    render() {
        const { handleSubmit } = this.props;
        return (
        <section className="signup-section">
            <div className="bg-container">
                <div className="bg-image">
                </div>
            </div>
            <div className="container">
                <div className="column is-6 is-offset-3">
                <div className="content-box box-style">
                    <div className="signup-content">
                        <div className="title is-4">Join our community</div>
                        <div className="subtitle is-6 has-text-grey">Take part in a global conversation dedicated to learning more about each other.</div>
                        <SocialLogin />
                        <div className="has-text-centered" style={{fontWeight:"500", fontSize:"1rem", marginBottom:"1rem"}}> Or </div>
                        <SignUpForm />
                        <p style={{paddingTop:"1rem"}}>Already have an account? <Link to={`/login`}>Sign In</Link></p>
                        <p className="privacy-agree">By signing up, you agree to our <Link to={`/terms`}>Terms</Link> and that you have read our <Link to={`/privacy`}>Privacy Policy</Link> and <Link to={`/content-policy`}>Content Policy</Link>.
                        </p>
                    </div>
                </div>
            </div>
            </div>
        </section>
        );
    }

}

SignUp = reduxForm({
    form: 'SignUp'
})(SignUp);

export default connect(state => ({
    activeUser: state.activeUser
}))(SignUp);
