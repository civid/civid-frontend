import React, {Component} from 'react';
import logo from '../../assets/images/iconlogo.png';
import Navigation from '../../components/Navigation';
import './terms.scss';


class Terms extends Component {

  render() {

    return (
      <div className="Terms">
        <div className="app-container">
          <Navigation />


          <section className="section">
            <div className="container">
              <h4 className="title is-4">Terms of Service</h4>
              <div className="content">
                <p>
                  Welcome to Civid.com.
                </p>
                <p>
                  Kindly read the terms and conditions meticulously before utilizing our website, posting anything on our website or social media pages, or contacting us. By accessing the website or services, you agree to be bound by these stipulations. This agreement governs your access to the website, and we might refer Civid as the "Company", "we", "our", "website", "platform" or other relevant terms.
                </p>
                <p>
                  We do not allow any kind of fraudulent, unauthorized or unlawful activity.
                </p>
              </div>
              <div className="content">
                <h6 className="subtitle is-6">Website Content</h6>
                <p>
                  We may post some information, tricks and advice in our blog section and a user understands that all the opinions, information, content and other materials posted on the website are for informational purposes only, and we do not endorse any kind of products on our website. Civid is a social community for people who want to challenge their understanding by accepting the fact that they might not understand the whole story. By challenging their own stance, members of the Civid community acquire and contribute new knowledge. We do not sell personal advice, and all the remarks and opinions do not indicate the outcomes of any kind.
                </p>
                <p>
                  In the event you upload or publish information on our website, social media pages, blog or other platform owned by us, we reserve the rights to remove, edit, manage, publish, or transmit the content in any form, at our own discretion. We will not be liable for any problem arising out of the materials you post and you accept that you own the material you post, and will be accountable in case any legal activity is performed basis on the content you publish on our website.
                </p>
              </div>
              <div className="content">
                <h6 className="subtitle is-6">Liability</h6>
                  <p>
                  Civid does not assure that the information or content that is posted on the website or our social media pages will be 100% accurate or precise. We suggest our users to publish error-free and accurate posts; however we will not be liable in case an error or mistake is noticed. We do not warrant that the website will be free of malware, viruses or other harmful elements that could hinder your use of the website. Regarding the emails we send, we do not guarantee that the graphics or other HTML components will be displayed properly, and we will not be liable for any activity that happens while accessing the website.
                  </p>
                </div>
                <div className="content">
                  <h6 className="subtitle is-6">Your Obligations:</h6>
                  <p>
                    You agree that you will not:
                    </p>
                    <ul>
                      <li>Use our information or services for any illegal purpose;</li>
                      <li>Use malware, viruses or other damaging technologies to harm our material;</li>
                      <li>Try to compromise our data or collect any other user's information without consent;</li>
                      <li>Upload any defamatory, libelous, inappropriate, obscene, hazardous or pornographic information, details, pictures or applications;</li>
                      <li>Submit or display any information, details, photos or systems that could breach any property rights of other individuals utilizing the Services;</li>
                      <li>Reproduce or resell our products for profits;</li>
                      <li>Use our services for any illegal activity;</li>
                      <li>Utilize our services or website if you are under 18 years of age;</li>
                      <li>Utilize automated programs to send spam emails to us or our users, or try to scrape or crawl any page of our website</li>
                    </ul>
                  <p>
                  You agree that you will be utilizing our services, and the information posted on our website only for legitimate purposes, and will not violate local, national or international regulations in any form.
                  </p>
                </div>
                <div className="content">
                  <h6 className="subtitle is-6">Intellectual Property</h6>
                  <p>
                    You understand that all the content posted on our website is owned by the users who post, and we are not responsible for anything that is posted on the website. We may evaluate the posts occasionally however we do not guarantee the accuracy of the posts that are published on our website. You are not allowed to modify or use intellectual property for any reason. We will grant a non-transferable and non-exclusive license to access the website.
                  </p>
                </div>
                <div className="content">
                  <h6 className="subtitle is-6">Notices:</h6>
                  <p>
                    We may notify our users by posting a notification on our website. The notices will become effective as soon as we post them on our platform. As a user, you acknowledge that you agree to the clause, and it is your responsibility to read the notices. If there is any question, or you want to send us a notice, please email us at <a href="mailto:legal@civid.com">legal@civid.com</a>. The communication must be made in English.
                  </p>
                </div>
                <div className="content">
                  <h6 className="subtitle is-6">Miscellaneous:</h6>
                  <p>
                    The user understands that the materials such as business plan do not form part of this agreement. We attempt to provide users with reliable services, however we will not be liable in case a user experiences any delay due to the factors beyond our reasonable control, such as software or hardware interruption, riots, earthquake, telecommunication errors, acts of terrorism, or any other cause that is outside the control of our team.
                  </p>
                  <p>
                    Users are not allowed to assign the agreement and the stipulations contained in it to any third person without our consent. In the event any section of this agreement becomes invalid, the remaining sections will work in full force and impact. The users also understand that in case any dispute arises out of these terms, it must be settled by the negotiation between parties, and in case it is unable to resolve the disputes mutually, they will be solved in the Arbitration Court. Before filing any claim, you must contact us via email to resolve the dispute. The claim must not be made 30 days after sending the e-mail.
                  </p>
                  <p>
                    In the event you disagree with any of the stipulations mentioned in this agreement, you must stop using the website or services we offer.
                  </p>
                </div>
            </div>
          </section>


    <section className="footer">
      <div className="container">
        <div className="content">
          <p>
            © 2018 Civid, inc.
          </p>
        </div>
      </div>
    </section>
  </div>
  </div>
        );
    }

}

export default Terms;
