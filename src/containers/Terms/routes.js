import React from 'react';
import {Route} from 'react-router';
import Authenticate from '../HOC/Authenticate';
import AuthenticationRequired from '../HOC/AuthenticationRequired';
import Terms from "./";



export default (
    <Route path="/terms" component={Authenticate(Terms)}/>
);
