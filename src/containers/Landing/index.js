import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import logo from '../../assets/images/iconlogo.png';
import {getUserList} from '../../actions/accounts/user/list';
import LandingNavigation from '../../components/LandingNavigation';
import SignUpForm from '../../forms/SignUpForm';
import SocialLogin from '../../components/social-auth';


import './Landing.scss';


class Landing extends Component {
    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(getUserList());
    }


    render() {
      const {users} = this.props;

      return (
        <div className="Landing">
          <div className="hero is-fullheight" style={{backgroundColor:"#f4f6f9"}}>
          <div className="hero-head">
            <LandingNavigation {...users}/>
          </div>

    <section className="hero-body">
      <div className="container">
        <div className="columns is-desktop is-vcentered is-multiline">
          <div className="column">
            <header className="section-header">
              <h1 className="title is-2 is-spaced">
                Debates that actually matter
              </h1>
              <h2 className="subtitle is-5 is-header">
                Civid hosts users with controversial opinions who are open to engage with different perspectives.
              </h2>
              <img src="https://s3.amazonaws.com/civid-media/static/banner.png"/>
            </header>

        </div>

        <div className="column is-6-desktop is-offset-1-desktop is-12-tablet">
          <div className="content-box box-style">
           <div className="card-content">
          <SocialLogin />
          <div className="has-text-centered" style={{fontWeight:"500", fontSize:"1rem", marginBottom:"1rem"}}> Or </div>
          <SignUpForm />
          <p style={{paddingTop:"1rem"}}>Already have an account? <Link to={`/login`}>Sign In</Link></p>
          <p className="privacy-agree">By signing up, you agree to our <Link to={`/terms`}>Terms</Link> and that you have read our <Link to={`/privacy`}>Privacy Policy</Link> and <Link to={`/content-policy`}>Content Policy</Link>.
          </p>
          </div>
          </div>
        </div>
      </div>

  </div>
</section>


    <section className="footer">
      <div className="container">
        <div className="content">
          <p>
            © 2019 Civid, inc.
          </p>
        </div>
      </div>
    </section>
  </div>
  </div>
        );
    }

}

export default connect(state => ({
    activeUser: state.activeUser,
    users: state.users.data
})) (Landing);
