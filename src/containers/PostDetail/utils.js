import {AWARDED_TROPHY_CONTEXT_TYPES} from "../../utils/normalize";


export function findAwardedTrophiesForPost(awardedTrophies, post) {
    return Object.values(awardedTrophies).filter(
        at => at.context && at.context.schema === AWARDED_TROPHY_CONTEXT_TYPES.POST && at.context.id === post.id);
}
