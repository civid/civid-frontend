import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import {getUsername, getProfileImage} from "../../utils/user";


const PostReplyPreview = ({postReply, onExpand, nestingLevel, numReplies, users}) => {
    let rep = "comments";
    if (numReplies == 1) rep = "comment";
    return (
    <div className="preview-content post-preview" id={postReply.id} onClick={onExpand}>

        <div className={classnames({
          'innerpreview-box': nestingLevel > 0,
          'preview-box': nestingLevel == 0
        })}>
        <article className="media centered-content" key={postReply.id}>
              <div className="user media-left">
                <img className="user-img size-30" src={getProfileImage(postReply.user, users)}/>
              </div>
              <div className="media-content" style={{overflow:'hidden'}}>
              <div className="post-name">{getUsername(postReply.user, users)}
              </div>
                <div className="reply-text-preview">{postReply.claim_0}</div>
              </div>
        </article>
        {nestingLevel == 0 && numReplies > 0 && <div style={{fontWeight:"600", fontSize:"1rem", marginTop:".5rem"}}>...and {numReplies} more {rep}</div>}
        </div>
    </div>);
  }

PostReplyPreview.propTypes = {
    postReply: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired,
    onExpand: PropTypes.func.isRequired,
};

export default PostReplyPreview;
