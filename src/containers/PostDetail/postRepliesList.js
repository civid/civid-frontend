import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getUsername, getProfileImage } from "../../utils/user";
import { Link, browserHistory } from 'react-router';
import TimeAgo from 'react-timeago';
import striptags from "striptags";
import {
  FacebookShareButton,
  TwitterShareButton,
  FacebookIcon,
  TwitterIcon
} from 'react-share';
import classnames from 'classnames';
import { createReplyVote } from '../../actions/votes/reply-vote/create';
import { deleteReplyVote } from '../../actions/votes/reply-vote/delete';
import { editReplyVote } from '../../actions/votes/reply-vote/edit';
import PostNestedReplyForm from '../../forms/PostNestedReplyForm';
import EllipsisMenu from '../../components/EllipsisMenu';
import PostReplyPreview from './postReplyPreview';
import Accordion from '../../components/Accordion/accordion';
import Influence from '../../components/Influence';
import Support from '../../components/Support';
import SupportButton from '../../components/Support-button';
import ShareButton from '../../components/Share-button';


const MAX_NESTING_LEVEL = 12;

class PostReply extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isExpanded: false,
            tooNested: false,
            selected: 'active',
            isSupportVisible: false,
            support: 0,
        };
    }

    supportOnClick = (e) => {
        const {postReply, activeUser} = this.props;
        if (!activeUser) this.props.SignUpModalOpenClose();
        else if (postReply.user != activeUser.id)
            this.setState({ 'isSupportVisible': !this.state.isSupportVisible })
    }

    setSupport = (num) => {
        this.setState({support: num})
    }

    onReplyClick = () => {
        const {activeUser} = this.props;
        if (!activeUser) this.props.SignUpModalOpenClose();
        else {
            const noFurtherNesting = this.props.nestingLevel >= MAX_NESTING_LEVEL;
            if (noFurtherNesting) {
                this.setState({ tooNested: true });
            } else {
                this.setState(
                    { isExpanded: true },
                    () => this.props.onReplyToPostClick(this.props.postReply.id)
                );
            }
        }
    };
    onChange = (selected) => {
        this.setState({ selected });
    }

    setDelete = () => this.props.setDelete(this.props.postReply);

    onExpand = () => this.setState({ isExpanded: true });

    renderClaim = (claim, index) => {
        const numberedClaim = claim.slice(0,3) + (index+1) + ". " + claim.slice(3);
        let strippedTags = (index+1) + ". " + striptags(claim).replace(/&nbsp;/gi,'');
        return (
            <div className="claim-box" key={index} label={strippedTags}>
                <div key={index} className="claim-contentopen"dangerouslySetInnerHTML={{ __html: numberedClaim }} />
            </div>
        )
    }

    renderClaims = (post) => {
        const claim_id = ['claim_0', 'claim_1', 'claim_2', 'claim_3'];
        let claims = [];
        claim_id.map(claim => {
            if (post[claim] != "") {
                claims.push(post[claim]);
            }
        });

        return (
            <Accordion allowMultipleOpen>
                {claims.map((claim, index) => (this.renderClaim(claim, index)))}
            </Accordion>
        )
    }

    countReplies = (postReply) => {
        const {postReplies, topLevelId} = this.props;
        const count = Object.values(postReplies).filter(p => p.claimparent == topLevelId && p.parent).length;
        return count;
    }

    onAwardTrophy = () => this.props.onAwardTrophy(this.props.postReply.user);

    render() {
        const {
            postReply,
            dispatch,
            postReplies,
            postToDisplayFormFor,
            onReplyToPostClick,
            onSuccessfulReplySubmit,
            activeUser,
            renderPreviewsOnly,
            nestingLevel,
            setDelete,
            canAwardTrophy,
            userReceivedAward,
            users,
            canReply,
            replyVotes,
            trophies,
            topLevelId,
            claimUser
        } = this.props;
        const { isExpanded, tooNested, isSupportVisible, support} = this.state;
        const displayReplyForm = postToDisplayFormFor === postReply.id;
        const canDeleteReply = activeUser && activeUser.id === postReply.user;
        const replyCount = this.countReplies(postReply);
        const shareUrl = 'https://civid.com';
        const title = 'Civid';
        const iconStyle = {fill:"#ffffff"};

        return (
            <div className={classnames({
                'content-box box-style': nestingLevel == 0
            })}>
            <div className={classnames({
                'post-content': nestingLevel == 0,
            })}>
            <div className={classnames({
                'inner-content': nestingLevel != 1,
                'innerreplies-content': nestingLevel == 1
                })} id={postReply.id}>
                <article className="media" key={postReply.id} style={{display: "block"}}>
                    <div className="media-content">
                        {postReply.title && <h6 className="reply-title">
                            {userReceivedAward && userReceivedAward === postReply.user && <span className="user-trophy has-text-centered"><img src={trophies[1].image} width="20" height="20" /></span>}
                            {postReply.title}</h6>}
                        <nav className="level is-mobile is-marginless">
                            <div className="level-left details">
                                <Link className="user level-item" to={`/profile/${getUsername(postReply.user, users)}`}>
                                    {nestingLevel === 0 ? <img className="user-img size-30" src={getProfileImage(postReply.user, users)} /> :
                                        <img className="user-img size-30" src={getProfileImage(postReply.user, users)} />}
                                </Link>
                                <div className="level-item">
                                    <Link className={classnames(
                                        'post-name', {
                                        'op-user': nestingLevel != 0 && postReply.user == claimUser,
                                    })} to={`/profile/${getUsername(postReply.user,users)}`}>
                                        {getUsername(postReply.user, users)}
                                        { nestingLevel != 0 && postReply.user == claimUser && <span className="icon">
                                            <i className="fas fa-comment is-small"></i>
                                        </span>}
                                    </Link>
                                </div>
                                {canAwardTrophy && <Link className="post-reply level-item is-link" onClick={this.onAwardTrophy}>
                                    Award Trophy
                                </Link>}
                                {canReply && nestingLevel != 0 && !tooNested && activeUser && <Link className="post-reply level-item is-link" onClick={this.onReplyClick}>
                                    Reply
                                </Link>}

                                {activeUser && <div className="level-item">
                                    {' · '}
                                </div>}
                                <div className="level-item time-ago post-reply">
                                    <TimeAgo date={postReply.created_date} minPeriod="60" />
                                </div>
                            </div>
                        </nav>



                        {nestingLevel == 0 ? <div className="body-text claim-box">{this.renderClaims(postReply)}</div> : <div className="body-text">{postReply.claim_0}</div>}

                        {nestingLevel == 0 && <div className="claim-stats">
                        <nav className="level is-mobile">
                            <Influence post={postReply} support={support} users={users}/>
                        </nav>
                    </div>}

                    {nestingLevel == 0 &&
                        <div className="action-group">
                        <nav className={classnames(
                            'level', {
                            'is-mobile': !isSupportVisible
                        })}>
                            <div className="level-item">
                                {!isSupportVisible && <SupportButton click={this.supportOnClick} />}
                                {isSupportVisible && <Support click={this.supportOnClick} visible={isSupportVisible} post={postReply} setSupport={this.setSupport} />}
                            </div>
                            <div className="level-item">
                            {canReply && <Link className="button is-white action-button level-item" onClick={this.onReplyClick}>
                                <span className="icon">
                                    <i className="fas fa-comment" style={{fontSize: "1rem", marginTop: "-.3rem", color: "#3081f9" }}></i>
                                </span>
                                <span className="action-text">Respond</span>
                                </Link>
                            }
                            </div>

                        <div className="level-item">
                        <div className="share-button">
                            <FacebookShareButton
                              url={shareUrl}
                              quote={title}>
                              <FacebookIcon
                                size={35}
                                iconBgStyle={iconStyle}
                                logoFillColor="#95989A"
                                />
                            </FacebookShareButton>
                          </div>
                          <div className="share-button">
                            <TwitterShareButton
                              url={shareUrl}
                              quote={title}>
                              <TwitterIcon
                                size={35}
                                iconBgStyle={iconStyle}
                                logoFillColor="#95989A"
                                />
                            </TwitterShareButton>
                          </div>

                            <EllipsisMenu post={postReply} SignUpModalOpenClose={this.props.SignUpModalOpenClose} canDelete={canDeleteReply} willDelete={this.setDelete} activeUser={activeUser}/>
                        </div>
                        </nav>
                        </div>
                    }


                        {tooNested && <div className="too-nested-warning">
                            {`Sorry, this thread has reached its limit (${MAX_NESTING_LEVEL} levels).`}
                        </div>}
                        {displayReplyForm && <article>
                            <PostNestedReplyForm
                                claimId={topLevelId}
                                postId={postReply.post}
                                parentReplyId={postReply.id}
                                onSuccessfulSubmit={onSuccessfulReplySubmit}
                            />
                        </article>}
                    </div>
                </article>
                {postReply.replies && postReply.replies.length > 0 && <article className={classnames({
                    'innerpreview-style': nestingLevel > 0,
                    'preview-style': nestingLevel == 0
                })}>
                    {renderPreviewsOnly && !isExpanded && <PostReplyPreview
                        postReply={postReplies[postReply.replies[postReply.replies.length-1]]}
                        onExpand={this.onExpand}
                        users={users}
                        numReplies={replyCount-1}
                        nestingLevel={nestingLevel}
                    />}
                    {(!renderPreviewsOnly || isExpanded) && <PostRepliesList
                        topLevelId={topLevelId}
                        postsToDisplay={postReply.replies.map(id => postReplies[id])}
                        postReplies={postReplies}
                        users={users}
                        claimUser={claimUser}
                        postToDisplayFormFor={postToDisplayFormFor}
                        onReplyToPostClick={onReplyToPostClick}
                        onSuccessfulReplySubmit={onSuccessfulReplySubmit}
                        activeUser={activeUser}
                        nestingLevel={nestingLevel + 1}
                        renderPreviewsOnly={true}
                        setDelete={setDelete}
                        canReply={canReply}
                        replyVotes={replyVotes}
                        dispatch={dispatch}
                    />}
                </article>}
            </div>
            </div>
            </div>);
    }
}

PostReply.defaultProps = {
    postToDisplayFormFor: null,
    activeUser: null,
    renderPreviewsOnly: false,
    onAwardTrophy: null,
    canAwardTrophy: false,
    canReply: false,
};

PostReply.propTypes = {
    topLevelId: PropTypes.number,
    postReply: PropTypes.object.isRequired,
    postReplies: PropTypes.object.isRequired,
    postToDisplayFormFor: PropTypes.number,
    onReplyToPostClick: PropTypes.func.isRequired,
    onSuccessfulReplySubmit: PropTypes.func.isRequired,
    activeUser: PropTypes.object,
    renderPreviewsOnly: PropTypes.bool,
    nestingLevel: PropTypes.number.isRequired,
    setDelete: PropTypes.func.isRequired,
    onAwardTrophy: PropTypes.func,
    canAwardTrophy: PropTypes.bool,
    userReceivedAward: PropTypes.number,
    canReply: PropTypes.bool,
    users: PropTypes.object.isRequired,
    claimUser: PropTypes.number
};

const PostRepliesList = ({ postsToDisplay, topLevelId, nestingLevel, ...restOfProps }) => {
    if (postsToDisplay.length === 0) return null;
    const sorted = postsToDisplay.sort((a, b) => a.created_date > b.created_date).reverse();

    return (<div>
        {sorted.map(r => {
            let t = topLevelId;
            if (nestingLevel == 0 ) t = r.id;
            return (
                <PostReply
                    key={r.id}
                    postReply={r}
                    topLevelId={t}
                    nestingLevel={nestingLevel}
                    {...restOfProps}
                />
            )
        })}
    </div>);
};

PostRepliesList.defaultProps = {
    postToDisplayFormFor: null,
    activeUser: null,
    renderPreviewsOnly: false,
    onAwardTrophy: null,
    canAwardTrophy: false,
    canReply: false,
};

PostRepliesList.propTypes = {
    topLevelId: PropTypes.number,
    postsToDisplay: PropTypes.array.isRequired,
    postReplies: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired,
    replyVotes: PropTypes.object.isRequired,
    postToDisplayFormFor: PropTypes.number,
    onReplyToPostClick: PropTypes.func.isRequired,
    onSuccessfulReplySubmit: PropTypes.func.isRequired,
    activeUser: PropTypes.object,
    renderPreviewsOnly: PropTypes.bool,
    nestingLevel: PropTypes.number.isRequired,
    setDelete: PropTypes.func.isRequired,
    onAwardTrophy: PropTypes.func,
    canAwardTrophy: PropTypes.bool,
    userReceivedAward: PropTypes.number,
    canReply: PropTypes.bool,
};

export default connect((state, props) => ({
}))(PostRepliesList);
