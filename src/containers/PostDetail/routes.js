import React from 'react';
import {Route} from 'react-router';

import Authenticate from '../HOC/Authenticate';
import PostDetail from "./";


export default (
    <Route path="/posts/:postId" component={Authenticate(PostDetail)}/>
);
