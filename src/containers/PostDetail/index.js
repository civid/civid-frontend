import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router';
import { getPostFromQueue } from '../../actions/posts/post/get-post-from-queue'
import Navigation from '../../components/Navigation';
import OpinionCard from '../../components/Opinion-card';
import ModalPostForm from '../../components/modal/modalPost';
import Post from '../../components/Post/index'
import {getPost} from '../../actions/posts/post/get';
import Anonymous from '../../components/Anonymous';
import PostPreview from '../../components/PostPreview';
import { getProfileImage } from '../../utils/user';
import {getTrophiesList} from '../../actions/trophies/trophy/list';
import {getAwardedTrophiesList} from '../../actions/trophies/awarded-trophy/list';
import {AWARDED_TROPHY_CONTEXT_TYPES} from '../../utils/normalize';
import './PostDetail.scss';


class PostDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isClockVisible: false,
            isOpinionModalVisible: false,
        };
    }

    componentDidMount() {
        const { dispatch, params: {postId}} = this.props;
        dispatch(getPost(postId));
        dispatch(getTrophiesList());
        dispatch(getAwardedTrophiesList());
    }


    opinionModalOpenClose = (e) => {
        this.setState({ 'isOpinionModalVisible': !this.state.isOpinionModalVisible })
    }

    getPostData = (data) => {
        if (data !== undefined && data.hasOwnProperty('data')) {
            const key = Object.keys(data.data)
            return data.data[key]
        }
        return
    }

    renderPreviousWinners() {
        const {awardedTrophies, posts} = this.props;
        const trophiesList = Object.values(awardedTrophies);
        const titles = Object.values(posts).reverse();
        const winners = trophiesList.filter(
        at => at.context && at.context.schema === AWARDED_TROPHY_CONTEXT_TYPES.POST).slice(trophiesList.length-2, trophiesList.length).reverse();

        return (
           <div className="content-box box-style is-hidden-mobile">
                <div className="box-header">
                    <div>
                    <h6 className="title is-6">Previous Winners
                    </h6>
                    </div>
                </div>
                <div className="winners-bottom">
                    {titles.slice(1,3).map(post => <div style={{marginBottom: "1rem"}}>
                        <div className="title is-6" style={{marginBottom: "1rem"}}>{post.title}</div>
                        <Link className="user-name">
                            <img src="https://i.imgur.com/uuykYlB.png" className="user-img size-30" width="30" height="20" style={{ margin: "0rem 1rem" }}/>
                        </Link>
                        <span className="post-name">testuser</span>
                        </div>
                    )}
                </div>
            </div>
        );
    }

    renderMostPopular() {
        const {awardedTrophies, posts} = this.props;
        const trophiesList = Object.values(awardedTrophies);
        const titles = Object.values(posts).reverse();
        const winners = trophiesList.filter(
        at => at.context && at.context.schema === AWARDED_TROPHY_CONTEXT_TYPES.POST).slice(trophiesList.length-2, trophiesList.length).reverse();

        return (
           <div className="content-box box-style is-hidden-mobile">
                <div className="box-header">
                    <div>
                    <h6 className="title is-6">Most Popular
                    </h6>
                    </div>
                </div>
                <div className="winners-bottom">
                    {titles.slice(1,3).map(post => <div style={{marginBottom: "1rem"}}>
                        <div className="title is-6" style={{marginBottom: "1rem"}}>{post.title}</div>
                        <Link className="user-name">
                            <img src="https://i.imgur.com/uuykYlB.png" className="user-img size-30" width="30" height="20" style={{ margin: "0rem 1rem" }}/>
                        </Link>
                        <span className="post-name">testuser</span>
                        </div>
                    )}
                </div>
            </div>
        );
    }

    renderRules() {
        return (
            <div className="content-box box-style is-hidden-mobile">
                <div className="box-header">
                    <div>
                    <h6 className="title is-6">Rules of Discussion
                    <span className="icon faq-icon">
                    <Link className="is-grey" to={`/faq`}><i className="fas fa-question-circle"/></Link></span>
                    </h6>
                    </div>
                </div>
                    <div className="box-bottom">
                        <ul className="rules-list">
                            <li>1. Do not use hostile or offensive language.</li>
                            <li>2. Direct responses must challenge the initiator's argument in some way.</li>
                            <li>3. Responses must contribute meaningfully to the discussion. </li>
                            <li>4. Do not directly accuse the initiator. If you have suspicions that they are not playing fair, please message us. </li>
                            <li>5. A single trophy is awarded for the reply that best changes your mind, if any. Do not award sarcastic, joking, or otherwise non-serious trophies. </li>
                        </ul>
                    </div>
            </div>
        );
    }

    renderPreviousDiscussions() {
        const {posts} = this.props;
        const postList = Object.values(posts);
        if (!postList) return null;
        return (
            <div className="columns">
                {postList.slice(0,3).reverse().map(post => <PostPreview
                    key={post.id}
                    post={post}
                />)}
            </div>
        );
    }

    render() {
        const { activeUser, users, post } = this.props;
        const postData = this.getPostData(post);
        if (!postData) return null;
        return (
            <div className="home">
                <div className="app-container">

                    <Navigation />

                    <section className="feed-banner has-background-white">
                        <h3 className="title has-text-centered control">Think you can change their mind?</h3>
                        <h5 className="subtitle is-5 has-text-centered">
                        <nav className="level">
                            <p className="level-item centered-content user-name">
                                Today's Host:
                                <Link className="user-name" to={`/profile/${postData.user}`}>
                                    <img src={getProfileImage(postData.user, users)} className="user-img size-40" style={{ margin: "0 1rem" }}/>
                                </Link>

                                <Link to={`/profile/${postData.user}`} style={{ color: "#4A4A4A" }}>
                                    {postData.username}
                                </Link>
                            </p>
                        </nav>
                        </h5>
                    </section>

                    <div className="container">
                        <div className="columns main-content margin-top-2 is-reverse-mobile">
                            <div className="column is-9">

                                <Post />

                                <div className="is-size-5" style={{ fontWeight:"600", paddingTop: "2rem", paddingBottom: "1rem" }}><span style={{borderBottom: "5px solid #3081f9", paddingBottom: ".4rem" }}>Previous Discussions<hr style={{ margin: ".5rem 0", backgroundColor: "#95989a" }}/></span>
                                </div>
                                {this.renderPreviousDiscussions()}
                            </div>
                            <div className="column is-3">
                                <OpinionCard click={this.opinionModalOpenClose} activeUser={activeUser} />


                                {this.renderPreviousWinners()}
                                {this.renderMostPopular()}
                                {this.renderRules()}

                            </div>

                            <ModalPostForm visible={this.state.isOpinionModalVisible} modalClose={this.opinionModalOpenClose} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default connect(state => ({
    post: state.post,
    posts: state.posts.data,
    activeUser: state.activeUser,
    users: state.users.data,
    trophies: state.trophies.data,
    awardedTrophies: state.awardedTrophies.data,
}))(PostDetail);
