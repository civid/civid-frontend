import React, {Component} from 'react';
import {connect} from 'react-redux';
import UserForm from '../../../forms/UserForm';


class BasicInformation extends Component {

    render() {
        return (
            <div className="content-box box-style">
                <div className="post-content">
                    <div className="title is-6">
                        Edit Profile Info
                    </div>
                    <UserForm/>
                </div>
            </div>
        );
    }

}

export default connect(state => ({}))(BasicInformation);
