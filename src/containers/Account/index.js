import React, {Component} from 'react';
import Navigation from '../../components/Navigation';
import LeftMenu from './LeftMenu';
import './Account.scss'


class Account extends Component {

    render() {
        return (
            <div className="app-container">
                <Navigation />
                <section className="section">
                <div className="container">
                    <div className="columns">
                        <div className="column is-3">
                        <LeftMenu/>
                        </div>
                        <div className="column is-7">
                            {this.props.children}
                        </div>
                    </div>
                </div>
                </section>
            </div>
        );
    }

}

export default Account;
