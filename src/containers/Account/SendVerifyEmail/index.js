import React, { Component } from 'react';
import { connect } from 'react-redux';
import { sendVerifyMail } from '../../../actions/accounts/user/send-email'

const style = {
    marginTop: "20px",
}

class SendVerifyEmailPage extends Component {
    state = {
        sent: this.props.email_verified,
        message: '',
    }

    handleClick = (e) => {
        const { dispatch, activeUser } = this.props;
        // avoid send ing the email twice
        this.setState({ 'sent': true })

        dispatch(sendVerifyMail(activeUser.id))
            .then(() => {
                this.setState({
                    'message': 'Email sent, please check your mail box.'
                })
            })
            .catch(error => {
                this.setState({
                    message: error.message
                });
            });

    }
    render() {

        return (
            <div className="content-box box-style">
                <div className="post-content">
                    <div className="title is-6" style={style}>
                        <button disabled={this.state.sent}
                            className="button is-blue is-medium is-fullwidth"
                            onClick={this.handleClick}
                        >
                            Email is unverified. Click to resend
                        </button>
                        <div className="center is-warning" style={style}>
                            {this.state.message}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default connect((state) => ({
    activeUser: state.activeUser
}))(SendVerifyEmailPage)
