import React, {Component} from 'react';
import {connect} from 'react-redux';
import UpdatePasswordForm from '../../../forms/UpdatePasswordForm';


class UpdatePassword extends Component {

    render() {
        return (
            <div className="content-box box-style">
                <div className="post-content">
                    <div className="title is-6">
                        Update Password
                    </div>
                    <UpdatePasswordForm/>
                </div>
            </div>
        );
    }

}

export default connect(state => ({}))(UpdatePassword);
