import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'


class LeftMenu extends Component {

    emailNotVerified = () => {
        const { activeUser } = this.props;
        if (activeUser.email_verified) {
            return (null)
        } else {
            return (
                <li className="nav-item">
                    <Link activeClassName="active" className="nav-link" to="/account/send-verify-email">Email
                    not verified
                    </Link>
                </li>
            )
        }
    }

    render() {
        return (
            <nav className="content-box box-style">
                <div className="post-content">
                    <ul className="feed-list">
                        <li className="nav-item">
                            <Link activeClassName="active" className="nav-link" to="/account/basic-information">Basic
                            Information</Link>
                        </li>
                        <li className="nav-item">
                            <Link activeClassName="active" className="nav-link" to="/account/update-password">Update
                            Password</Link>
                        </li>

                        {this.emailNotVerified()}

                    </ul>
                </div>
            </nav>
        );
    }

}

export default connect((state) => ({
    activeUser: state.activeUser,
}))(LeftMenu);
