var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const path = require('path');
const webpack = require('webpack');

let API_HOST = 'http://localhost:8000';
let PATH_TO_BACKEND = path.join(__dirname, '../civid-backend');

if (process.env.PATH_TO_BACKEND)
    PATH_TO_BACKEND = process.env.PATH_TO_BACKEND;

module.exports = {
    devServer: {
        inline: true,
        contentBase: [path.join(__dirname, 'public'), PATH_TO_BACKEND],
        historyApiFallback: true,
        port: 3000
    },
    devtool: 'cheap-module-eval-source-map',
    entry: './src/index.js',
    module: {
        rules: [
            {
                test: /\.js$/,
                rules: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.scss/,
                rules: [
                    'style-loader', 'css-loader', 'sass-loader'
                ]
            },
            {
                test: /\.(pdf|jpg|png|gif|svg|ico)$/,
                use: [
                    {
                        loader: 'url-loader'
                    },
                ]
            }
        ]
    },
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'js/bundle.min.js',
        publicPath: '/'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'API_ROOT': JSON.stringify(`${API_HOST}`),

            }
        }),
        /*new BundleAnalyzerPlugin() */
    ]

};
