const path = require('path');
const webpack = require('webpack');

let API_HOST = '';
let PATH_TO_BACKEND = path.join(__dirname, '../civid-backend');

const CompressionPlugin = require('compression-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

if (process.env.PATH_TO_BACKEND)
    PATH_TO_BACKEND = process.env.PATH_TO_BACKEND;

module.exports = (env) => {
  const mode = env.TARGET_ENV === 'development' ? 'development' : 'production';
  let API_HOST = 'https://api.civid.com';
   if (env.TARGET_ENV === 'development') API_HOST = 'http://localhost:8000';
   else if (env.TARGET_ENV === 'staging') API_HOST = 'http://testapi.civid.com';
    return {
        devServer: {
            inline: true,
            contentBase: [path.join(__dirname, 'public'), PATH_TO_BACKEND],
            historyApiFallback: true,
            port: 3000
        },
        mode: mode,
        plugins: [
          new webpack.DefinePlugin({
            'process.env': {
              'TARGET_ENV': JSON.stringify(env.TARGET_ENV),
              'API_ROOT': JSON.stringify(API_HOST)
            }
          }),
          new CleanWebpackPlugin('public', {}),

            new MiniCssExtractPlugin({
                filename: 'style.[contenthash].css',
            }),
                new HtmlWebpackPlugin({
                  inject: false,
                  hash: true,
                  template: './src/index.html',
                  filename: 'index.html'
                })
        ],
        entry: { main: './src/index.js' },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.svg$/,
                    loader: 'svg-inline-loader'
                },
                {
                    test: /\.css$/,
                    use: [
                      MiniCssExtractPlugin.loader,
                      { loader: 'css-loader', options: { } },
                      'postcss-loader',
                    ],
                  },
                  {
                    test: /\.sass$|\.scss$/,
                    use: [
                      MiniCssExtractPlugin.loader,
                      {
                        loader: 'css-loader',
                        options: {  },
                      },
                      { loader: 'postcss-loader' },
                      { loader: 'sass-loader' },
                    ],
                  },
                {
                    test: /\.(jpg|png|gif|svg|pdf|ico)$/,
                    loader: "file-loader?name=/assets/images/[name]-[hash:8].[ext]"
                }
            ]
        },
        output: {
            path: path.join(__dirname, 'public'),
            filename: 'js/[name].[chunkhash].js'
        },
    };
};



